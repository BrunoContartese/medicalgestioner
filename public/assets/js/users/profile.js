function updateProfile()
{
    var name = $('input[name="name"]').val();
    var email = $('input[name="email"]').val();
    var username = $('input[name="username"]').val();
    var password = $('input[name="password"]').val();
    var repeat_password = $('input[name="repeat_password"]').val();

    var data = {
      name: name,
      email: email,
      username: username,
      password: password,
      repeat_password: repeat_password
    };

    axios.post('/profile', data)
        .then(function(result) {
            if(result.status === 200) {
                location.href = '/profile';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
}

function updateProfileImage()
{
    var formData = new FormData();

    var imageFile = document.querySelector('#profileImageFile');

    formData.append("profileImageFile", imageFile.files[0]);

    axios.post('/profile/image/', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
        .then(function(result) {
            if(result.status === 200) {
                location.href = '/profile/image';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
}

function updateSignatureImage()
{
    var formData = new FormData();

    var imageFile = document.querySelector('#signatureImageFile');

    formData.append("signatureImageFile", imageFile.files[0]);

    axios.post('/profile/signature/', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
        .then(function(result) {
            if(result.status === 200) {
                location.href = '/profile/signature';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
}