$(document).ready(function() {
   $('.chosen-select').chosen();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

function confirmSubmit(id, message = "¿Está seguro de eliminar el registro?")
{
    bootbox.confirm({
        message: message,
        class: "bounceIn",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-success'
            }
        },
        callback: function (result) {
            if(result) {
                $("#"+id).submit();
            } else {
                bootbox.hideAll();
            }
        }
    });
}

function addErrors(data, div = "#ajaxErrors") {
    var errors = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><p><strong>Parece que hay un error:</strong></p>';
    errors += '<ul>';
    for (var datos in data) {
        errors += '<li>' + data[datos] + '</li>';
    }
    errors += '</ul></div>';
    $(div).html(errors);
}

function toastrResponseError(errorCode, data)
{
    switch(errorCode) {
        case 500:
            toastr.error("Ha ocurrido un error en el servidor. Por favor contáctese con la oficina de sistemas.");
            break;
        case 404:
            toastr.error("No se ha encontrado la ruta hacia el servidor. Por favor contáctese con la oficina de sistemas.");
            break;
        case 403:
            toastr.error("Usted no tiene permisos para realizar la acción solicitada.");
            break;
        case 422:
            addErrors(data.errors);
            break;
        case 405:
            toastr.error("Método no permitido.");
            break;
    }
}

$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
});