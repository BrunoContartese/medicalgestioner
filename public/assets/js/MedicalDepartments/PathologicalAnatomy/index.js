var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#pathologicalAnatomySamplesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/medicalDepartments/datatable/PathologicalAnatomy',
        columnDefs: [
            {
                targets: 0,
                data: 'id',
                name: 'id',
                searchable: false
            },
            {
                targets: 1,
                data: 'institution.name',
                name: 'institution.name',
                searchable: false
            },
            {
                targets: 2,
                data: 'medical_department.name',
                name: 'medical_department.name',
                searchable: false
            },
            {
                targets: 3,
                data: 'code',
                name: 'code',
                searchable: false
            },
            {
                targets: 4,
                data: 'pathological_anatomy_type.name',
                name: 'pathological_anatomy_type.name',
                searchable: false
            },
            {
                targets: 5,
                data: 'patient_checking_account.patient.name',
                name: 'patientCheckingAccount.patient.name',
            },
            {
                targets: 6,
                data: 'received_at',
                name: 'received_at',
                searchable: false
            },
            {
                targets: 7,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});
