var pathologicalAnatomyBillingCodesTable = null;

$(document).ready(function() {
    pathologicalAnatomyBillingCodesTable =  $('#pathologicalAnatomyBillingCodesTable').dataTable({
        columnDefs: [
            {
                targets: 0,
                data: 'id',
                name: 'id',
                visible: false
            },
            {
                targets: 1,
                data: 'code',
                name: 'code'
            },
            {
                targets: 2,
                data: 'description',
                name: 'description'
            },
            {
                targets: 3,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });


    $('#pathologicalAnatomyBillingCodesTable tbody').on( 'click', '#delete', function () {
        pathologicalAnatomyBillingCodesTable
            .api()
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    } );

});

function addPracticeToTable()
{
    var practiceCode = $('select[name="nomenclator_practice_id"]').val();

    axios.get('/administration/nomenclatorPractices/get/' + practiceCode)
        .then(function(result) {
            if(result.status === 200) {
                var practice = result.data;
                pathologicalAnatomyBillingCodesTable.api().rows.add([
                    {
                        id: practice.id,
                        code: practice.code,
                        description: practice.description,
                        actions: deleteButton
                    }
                ]).draw(true);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        });
}

function deleteButton()
{
    return "<button class='btn btn-danger' id='delete'><i class='fa fa-trash'></i></button>"
}

function closeTab()
{
    window.close();
}

function syncBillingCodes()
{
    var data = {
        practices: pathologicalAnatomyBillingCodesTable.fnGetData()
    };

    var sampleId = $('input[name="sample_id"]').val();

    console.log(sampleId);

    axios.post('/medicalDepartments/pathologicalAnatomy/billingCodesAssignment/' + sampleId ,data)
        .then(function(result) {
            if(result.status === 200) {
                //closeTab();
                toastr.success('Exito al asignar códigos de facturación.');
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        });
}
