var dataTable = null;
var pathologicalAnatomyBillingCodesTable = null;

$(document).ready(function() {

    dataTable =  $('#pathologicalAnatomySamplesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/medicalDepartments/datatable/PathologicalAnatomy/medicalReport',
        columnDefs: [
            {
                targets: 0,
                data: 'created_at',
                name: 'created_at',
                searchable: false,
                width: '10%'
            },
            {
                targets: 1,
                data: 'code',
                name: 'code',
                searchable: false,
                width: '10%'
            },
            {
                targets: 2,
                data: 'pathological_anatomy_type.name',
                name: 'pathological_anatomy_type.name',
                searchable: false,
                width: '15%'
            },
            {
                targets: 3,
                data: 'patient_checking_account.patient.name',
                name: 'patientCheckingAccount.patient.name',
                width: '30%'
            },
            {
                targets: 4,
                data: 'billingCodes',
                name: 'billingCodes',
                searchable: false,
                width: '5%'
            },
            {
                targets: 5,
                data: 'categorizeSample',
                name: 'categorizeSample',
                searchable: false,
                width: '5%'
            },
            {
                targets: 6,
                data: 'writeMedicalReport',
                name: 'writeMedicalReport',
                searchable: false,
                width: '5%'
            },
            {
                targets: 7,
                data: 'actions',
                name: 'actions',
                searchable: false,
                width: '10%'
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});