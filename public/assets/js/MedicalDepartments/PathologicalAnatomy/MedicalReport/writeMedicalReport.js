var editor = null;
$(document).ready(function() {
    editor = textboxio.replace('#editor');
});

function getTitle()
{
    var titleId = $('select[name="medical_report_title_id"]').val();

    axios.get('/medicalDepartments/pathologicalAnatomy/medicalReportTemplates/getTitle/' + titleId)
        .then(function(result) {
            if(result.status === 200) {
                addContentToEditor('<h2>' + result.data + '</h2>');
                cleanMedicalReportTitlesSelect();
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}

function getTemplatesByCategory()
{
    var templateCategoryId = $('select[name="medical_report_template_category_id"]').val();

    axios.get('/medicalDepartments/pathologicalAnatomy/medicalReportTemplates/getTemplatesByCategory/' + templateCategoryId)
        .then(function(result) {
            if(result.status === 200) {
                $('#templates').html(result.data);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $('.chosen-select').chosen({
                width: '100%'
            });
        });
}

function getTemplate()
{
    var templateId = $('select[name="pathological_anatomy_template"]').val();

    axios.get('/medicalDepartments/pathologicalAnatomy/medicalReportTemplates/getTemplate/' + templateId)
        .then(function(result) {
            if(result.status === 200) {
                addContentToEditor(result.data);
                cleanTemplateSelectDiv();
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}

function cleanMedicalReportTitlesSelect()
{
    $('select[name="medical_report_title_id"]').prop('selectedIndex', -1);
    $('select[name="medical_report_title_id"]').trigger("chosen:updated");
}

function cleanTemplateSelectDiv()
{
    $('#templates').html("");
    cleanTemplatesCategoriesSelect();
}

function addContentToEditor(text)
{
    var currentText = editor.content.get();
    var newText = currentText + "<br>" + text + "<br>";
    editor.content.set(newText);
}

function cleanTemplatesCategoriesSelect()
{
    $('select[name="medical_report_template_category_id"]').prop('selectedIndex', -1);
    $('select[name="medical_report_template_category_id"]').trigger("chosen:updated");
}


function signMedicalReport()
{
    axios.get('/profile/getSignature')
        .then(function(result) {
            if(result.status === 200) {
                console.log(result.data);
                var html = "<div style='text-align: right'><img src='" + result.data + "' width='300px' height='300px' /></div>";
                addContentToEditor(html);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        });
}

function getPapanicolaouTemplateModal()
{
    axios.get('/profile/getSignature')
        .then(function(result) {
            if(result.status === 200) {
                var dialog = bootbox.dialog({
                        backdrop  : "static",
                        title: '<h3>Informe de Papanicolaou.</h3>',
                        onEscape: true,
                        message: result,
                        size: 'large',
                        class: "bounceIn",
                        buttons: {
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-success'
                            },
                            noclose: {
                                label: 'Guardar',
                                className: 'btn-warning',
                                callback: function() {
                                    writePapanicolaouReport();
                                }
                            }
                        }
                    }).init( function() {

                        $(".chosen-select").chosen({
                            width: "100%",
                            placeholder_text_single: "Seleccione una opción",
                            no_results_text: "Sin resultados."
                        });
                    });
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        });
}

function writePapanicolaouReport()
{
    var templateId;
    for(var i = 1; i<14 ; i++) {
        templateId = $('select[name="category' + i +'"]').val();
        if(i == 13)
            addContentToEditor("<b><u>Diagnóstico</u</b>");
        if(templateId !== "")
            getTemplateDescription(templateId);
    }
}

async function getTemplateDescription(templateId)
{
     await axios.get("/pathologicalAnatomy/getPapanicolaouTemplateDescription/" + templateId)
        .then(function(result) {
            if(result.status === 200) {
                addContentToEditor(result.data);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        });
}

function storeMedicalReport()
{

}

