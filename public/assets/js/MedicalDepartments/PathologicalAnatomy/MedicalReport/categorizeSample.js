function getLevelTwoCategories()
{
    var levelOneCategoryId = $('select[name="pathological_anatomy_category_level_one_id"]').val();
    axios.get('/medicalDepartments/pathologicalAnatomy/categories/levelOne/childCategories/' + levelOneCategoryId)
        .then(function(result) {
            if(result.status === 200) {
                $('#pathological_anatomy_category_level_two').html(result.data);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $(".chosen-select").chosen({
                width: '100%'
            });
        });
}

function getLevelThreeCategories()
{
    var levelTwoCategoryId = $('select[name="pathological_anatomy_category_level_two_id"]').val();
    axios.get('/medicalDepartments/pathologicalAnatomy/categories/levelTwo/childCategories/' + levelTwoCategoryId)
        .then(function(result) {
            if(result.status === 200) {
                $('#pathological_anatomy_category_level_three').html(result.data);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $(".chosen-select").chosen({
                width: '100%'
            });
        });
}

function getLevelFourCategories()
{
    var levelThreeCategoryId = $('select[name="pathological_anatomy_category_level_three_id"]').val();
    axios.get('/medicalDepartments/pathologicalAnatomy/categories/levelThree/childCategories/' + levelThreeCategoryId)
        .then(function(result) {
            if(result.status === 200) {
                $('#pathological_anatomy_category_level_four').html(result.data);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $(".chosen-select").chosen({
                width: '100%'
            });
        });
}

function syncCategories()
{
    var levelOneCategoryId = $('select[name="pathological_anatomy_category_level_one_id"]').val();
    var levelTwoCategoryId = $('select[name="pathological_anatomy_category_level_two_id"]').val();
    var levelThreeCategoryId = $('select[name="pathological_anatomy_category_level_three_id"]').val();
    var levelFourCategoryId = $('select[name="pathological_anatomy_category_level_four_id"]').val();

    var sampleId = $('input[name="sample_id"]').val();

    var data = {
        levelOneCategoryId: levelOneCategoryId,
        levelTwoCategoryId: levelTwoCategoryId,
        levelThreeCategoryId: levelThreeCategoryId,
        levelFourCategoryId: levelFourCategoryId
    };

    axios.post('/medicalDepartments/pathologicalAnatomy/categorizeSample/' + sampleId, data)
        .then(function(result) {
            if(result.status === 200) {
                toastr.success("Muestra categorizada correctamente.<br>Será redirigido a la pantalla de informes en 5 segundos.");
                window.setTimeout(function() {
                    window.close();
                }, 5000);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $(".chosen-select").chosen({
                width: '100%'
            });
        });
}