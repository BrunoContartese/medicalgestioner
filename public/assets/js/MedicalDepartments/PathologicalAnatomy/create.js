var patientChecked = false;
var openedCheckingAccountsTable = null;

$(document).ready(function() {
    $('.chosen').chosen({
        width: '100%'
    });

    /*Boton de terminar*/
    var $createPathologicalAnatomyStudyWizardFinish = $('#createPathologicalAnatomyStudyWizard').find('ul.pager li.finish');

    /*Validación antes de pasar al siguiente paso*/
    var $createPathologicalAnatomyStudyWizardValidator = $("#createPathologicalAnatomyStudyWizard form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
            $(element).remove();
        },
        errorPlacement: function( error, element ) {
            element.parent().append( error );
        }
    });

    /*Accion que realizará al finalizar*/
    $createPathologicalAnatomyStudyWizardFinish.on('click', function( ev ) {
        ev.preventDefault();
        storeNewPathologicalAnatomyStudy();
    });

    /*Bootstrap Wizard*/
    $('#createPathologicalAnatomyStudyWizard').bootstrapWizard({
        tabClass: 'wizard-steps',
        nextSelector: 'ul.pager li.next',
        previousSelector: 'ul.pager li.previous',
        firstSelector: null,
        lastSelector: null,
        onNext: function( tab, navigation, index, newindex ) {

            /*Valido si ha llenado los campos necesarios*/
            var validated = $('#createPathologicalAnatomyStudyWizard form').valid();

            if( !validated ) {
                $createPathologicalAnatomyStudyWizardValidator.focusInvalid();
                return false;
            }

            /*Si el index está en 1 (Ingreso de dni de paciente)
            y no se ha checkeado que la información del paciente sea correcta
            hago que no pase al siguiente paso hasta que no confirme los datos del mismo
             */
            if(index === 1 && patientChecked === false) {
                checkPatient();
                return false;
            }

            /*Si el index está en 1 (Ingreso de dni de paciente)
              y ya se ha checkeado que la información del paciente sea correcta
              pongo patientChecked en false, cosa de que si vuelve a la primer pestaña el usuario
              se vea obligado a checkear la información del paciente nuevamente.
             */
            if(index === 1 && patientChecked === true) {
                patientChecked = false;
            }

            /*Verifico que haya seleccionado una rendición en el 2do paso*/
            if(index === 2) {
                var selectedCheckingAccount = openedCheckingAccountsTable.api().row('.selected').data();
                if(openedCheckingAccountsTable.$('tr.selected').length < 1) {
                    toastr.error("Debe seleccionar una rendición.");
                    return false;
                }
            }

        },
        onTabClick: function( tab, navigation, index, newindex ) {
            /*Si presiona directamente en otro paso del formulario no le permito avanzar*/
            return false;
        },
        onTabChange: function( tab, navigation, index, newindex ) {
            /*Me fijo si es el último paso del wizard para mostrar botón de siguiente o de finalizar*/
            var $total = navigation.find('li').size() - 1;
            $createPathologicalAnatomyStudyWizardFinish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
            $('#createPathologicalAnatomyStudyWizard').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
        },
        onTabShow: function( tab, navigation, index ) {
            /*Voy moviendo el % de la barra*/
            var $total = navigation.find('li').length - 1;
            var $current = index;
            var $percent = Math.floor(( $current / $total ) * 100);
            $('#createPathologicalAnatomyStudyWizard').find('.progress-indicator').css({ 'width': $percent + '%' });
            tab.prevAll().addClass('completed');
            tab.nextAll().removeClass('completed');
        }
    });
});

/*Traigo un modal con los datos del paciente con el dni ingresado, si es que existe en la base de datos*/
function checkPatient()
{
    $(".se-pre-con").fadeIn("slow");
    var documentType = $('select[name="document_type_id"]').val();
    var documentNumber = $('input[name="document_number"]').val();
    axios.get('/administration/patient/findByDocumentNumber/' + documentType + '/' + documentNumber)
        .then(function(result) {
            $(".se-pre-con").fadeOut("slow");
            confirmPatientData(result.data);
        })
        .catch(function(error) {
            toastr.error("Paciente no encontrado.");
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        })
}

/*Confirmación de los datos del paciente*/
function confirmPatientData(patientData)
{
    var dialog = bootbox.confirm({
        message: patientData,
        className: 'modal80',
        buttons: {
            confirm: {
                label: 'Continuar',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-warning'
            }
        },
        callback: function (result) {
            if(result === true) {
                patientChecked = true;
                getPatientOpenedCheckingAccounts();
                $('ul.pager li.next').trigger('click');
            }
        }
    }).off("shown.bs.modal");
}

/*Muestro las rendiciones abiertas del paciente*/
function getPatientOpenedCheckingAccounts()
{
    var documentType = $('select[name="document_type_id"]').val();
    var documentNumber = $('input[name="document_number"]').val();

    axios.get('/helpers/getOpenedCheckingAccounts/' + documentType + '/' + documentNumber)
        .then(function(result) {
            $(".se-pre-con").fadeOut("slow");
            $('#step2').html(result.data);
        })
        .catch(function(error) {
            $('ul.pager li.previous').trigger('click');
            toastr.error("El paciente no posee rendiciones abiertas. <br> Por favor realice un check in antes de proceder.");
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $('.chosen-select').chosen({
                width: '100%'
            });
            initializeOpenedCheckingAccountsTable();
        })
}

/*creo un nuevo registro en la tabla de estudios de anatomía patológica*/
function storeNewPathologicalAnatomyStudy()
{
    /*Rendición seleccionada*/
    var checkingAccountId = openedCheckingAccountsTable.api().row('.selected').data();
    /*Selecciono solo el campo ID*/
    checkingAccountId = checkingAccountId[0];

    /*Datos del estudio*/
    var institutionId = $('select[name="institution_id"]').val();
    var pathologicalAnatomyStudyType = $('select[name="pathological_anatomy_study_type_id"]').val();
    var medicalSpecialistId = $('select[name="medical_specialist_id"]').val();
    var medicalDepartmentId = $('select[name="medical_department_id"]').val();
    var comments = $('textarea[name="comments"]').val();

    var data = {
        patient_checking_account_id: checkingAccountId,
        institution_id: institutionId,
        pathological_anatomy_type_id: pathologicalAnatomyStudyType,
        medical_specialist_id: medicalSpecialistId,
        medical_department_id: medicalDepartmentId,
        comments: comments,
    };

    axios.post('/medicalDepartments/pathologicalAnatomy/create', data)
        .then(function(result) {
            if(result.status === 200) {
                window.open(result.data, '_blank');
                location.href = '/medicalDepartments/pathologicalAnatomy';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        })
}

function initializeOpenedCheckingAccountsTable()
{
    openedCheckingAccountsTable =  $('#openedCheckingAccountsTable').dataTable({
        responsive: true,
        language: {url: '/assets/css/datatables/spanish.json'}
    });

    $('#openedCheckingAccountsTable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            openedCheckingAccountsTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
}

function onKeyPressEvent(e) {
    if(e.keyCode === 13) {
        $('ul.pager li.next').trigger('click');
    }
}
