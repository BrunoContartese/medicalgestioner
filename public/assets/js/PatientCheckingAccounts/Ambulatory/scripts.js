var patientChecked = false;

$(document).ready(function() {

    /*Boton de terminar*/
    var $patientCheckInWizardFinish = $('#patientCheckInWizard').find('ul.pager li.finish');

    /*Validación antes de pasar al siguiente paso*/
    var $patientCheckInWizardValidator = $("#patientCheckInWizard form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
            $(element).remove();
        },
        errorPlacement: function( error, element ) {
            element.parent().append( error );
        }
    });

    /*Accion que realizará al finalizar*/
    $patientCheckInWizardFinish.on('click', function( ev ) {
        ev.preventDefault();
        storeNewCheckingAccount();
    });

    /*Bootstrap Wizard*/
    $('#patientCheckInWizard').bootstrapWizard({
        tabClass: 'wizard-steps',
        nextSelector: 'ul.pager li.next',
        previousSelector: 'ul.pager li.previous',
        firstSelector: null,
        lastSelector: null,
        onNext: function( tab, navigation, index, newindex ) {

            /*Valido si ha llenado los campos necesarios*/
            var validated = $('#patientCheckInWizard form').valid();

            if( !validated ) {
                $patientCheckInWizardValidator.focusInvalid();
                return false;
            }

            /*Si el index está en 1 (Ingreso de dni de paciente)
            y no se ha checkeado que la información del paciente sea correcta
            hago que no pase al siguiente paso hasta que no confirme los datos del mismo
             */
            if(index === 1 && patientChecked === false) {
                checkPatient();
                return false;
            }

            /*Si el index está en 1 (Ingreso de dni de paciente)
              y ya se ha checkeado que la información del paciente sea correcta
              pongo patientChecked en false, cosa de que si vuelve a la primer pestaña el usuario
              se vea obligado a checkear la información del paciente nuevamente.
             */
            if(index === 1 && patientChecked === true) {
                patientChecked = false;
            }

            /*Verifico que haya seleccionado una obra social en el 2do paso*/
            if(index === 2) {
                var selectedPatientMedicalInsuranceId = $('select[name="medical_insurance_id"]').val();
                if(!parseInt(selectedPatientMedicalInsuranceId)) {
                    toastr.error("Debe seleccionar una obra social.");
                    return false;
                }
                /*Muestro la información seleccionada en los pasos 1 y 2*/
                getFinalStepInformation();
            }

        },
        onTabClick: function( tab, navigation, index, newindex ) {
            /*Si presiona directamente en otro paso del formulario no le permito avanzar*/
            return false;
        },
        onTabChange: function( tab, navigation, index, newindex ) {
            /*Me fijo si es el último paso del wizard para mostrar botón de siguiente o de finalizar*/
            var $total = navigation.find('li').size() - 1;
            $patientCheckInWizardFinish[ newindex != $total ? 'addClass' : 'removeClass' ]( 'hidden' );
            $('#patientCheckInWizard').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]( 'hidden' );
        },
        onTabShow: function( tab, navigation, index ) {
            /*Voy moviendo el % de la barra*/
            var $total = navigation.find('li').length - 1;
            var $current = index;
            var $percent = Math.floor(( $current / $total ) * 100);
            $('#patientCheckInWizard').find('.progress-indicator').css({ 'width': $percent + '%' });
            tab.prevAll().addClass('completed');
            tab.nextAll().removeClass('completed');
        }
    });
});

/*Traigo un modal con los datos del paciente con el dni ingresado, si es que existe en la base de datos*/
function checkPatient()
{
    $(".se-pre-con").fadeIn("slow");
    var documentType = $('select[name="document_type_id"]').val();
    var documentNumber = $('input[name="document_number"]').val();
    axios.get('/administration/patient/findByDocumentNumber/' + documentType + '/' + documentNumber)
        .then(function(result) {
            $(".se-pre-con").fadeOut("slow");
            confirmPatientData(result.data);
        })
        .catch(function(error) {
            toastr.error("Paciente no encontrado.");
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        })
}

/*Confirmación de los datos del paciente*/
function confirmPatientData(patientData)
{
    var dialog = bootbox.confirm({
                    message: patientData,
                    className: 'modal80',
                    buttons: {
                        confirm: {
                            label: 'Continuar',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-warning'
                        }
                    },
                    callback: function (result) {
                        if(result === true) {
                            patientChecked = true;
                            getPatientMedicalInsurances();
                            $('ul.pager li.next').trigger('click');
                        }
                    }
                }).off("shown.bs.modal");
}

/*Muestro las obras sociales del paciente para poder asignarla a la rendición*/
function getPatientMedicalInsurances()
{
    var documentType = $('select[name="document_type_id"]').val();
    var documentNumber = $('input[name="document_number"]').val();

    axios.get('/administration/patient/patientMedicalInsurances/' + documentType + '/' + documentNumber)
        .then(function(result) {
            $(".se-pre-con").fadeOut("slow");
            $('#patientMedicalInsurances').html(result.data);
        })
        .catch(function(error) {
            $('ul.pager li.previous').trigger('click');
            toastr.error("El paciente no posee obras sociales asignadas. <br> Por favor asigne una desde el menú de pacientes.");
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $('.chosen-select').chosen({
                width: '100%'
            });
        })
}

function getFinalStepInformation()
{
    var documentType = $('select[name="document_type_id"]').val();
    var medicalInsuranceId = $('select[name="medical_insurance_id"]').val();
    var documentNumber = $('input[name="document_number"]').val();

    axios.get('/patientCheckingAccounts/ambulatory/finalStepInformation/' + documentType + '/' + documentNumber + '/' + medicalInsuranceId)
        .then(function(result) {
            $(".se-pre-con").fadeOut("slow");
            $('#step3').html(result.data);
        })
        .catch(function(error) {
            $('ul.pager li.previous').trigger('click');
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
            $('.chosen-select').chosen({
                width: '100%'
            });
        })
}

/*Confirmación de la revisión de los datos, creo un nuevo registro en la tabla de rendiciones*/
function storeNewCheckingAccount()
{
    /*Datos del paciente*/
    var document_type_id = $('select[name="document_type_id"]').val();
    var medical_insurance_id = $('select[name="medical_insurance_id"]').val();
    var document_number = $('input[name="document_number"]').val();

    var data = {
        document_type_id: document_type_id,
        medical_insurance_id: medical_insurance_id,
        document_number: document_number
    };

    axios.post('/patientCheckingAccounts/ambulatory/checkIn', data)
        .then(function(result) {
            if(result.status === 200) {
                location.href = '/billingDepartment/patientCheckingAccounts';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        })
}
