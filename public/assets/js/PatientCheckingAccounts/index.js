var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#checkingAccountsTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/billingDepartment/patientCheckingAccounts',
        columnDefs: [
            {
                targets: 0,
                data: 'created_at',
                name: 'created_at',
                searchable: false
            },
            {
                targets: 1,
                data: 'medical_insurance_id',
                name: 'medical_insurance_id',
                searchable: false
            },
            {
                targets: 2,
                data: 'patient_checking_account_type_id',
                name: 'patient_checking_account_type_id',
                searchable: false
            },
            {
                targets: 3,
                data: 'patient_document_type',
                name: 'patient_document_type',
                searchable: false
            },
            {
                targets: 4,
                data: 'patient.document_number',
                name: 'patient.document_number',
            },
            {
                targets: 5,
                data: 'patient.name',
                name: 'patient.name',
            },
            {
                targets: 6,
                data: 'patient_checking_account_status_id',
                name: 'patient_checking_account_status_id',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});
