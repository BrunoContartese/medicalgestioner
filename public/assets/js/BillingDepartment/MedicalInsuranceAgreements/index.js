var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#medicalInsuranceAgreementsTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/billingDepartment/medicalInsuranceAgreements',
        columnDefs: [
            {
                targets: 0,
                data: 'medical_insurance_id',
                name: 'medical_insurance_id'
            },
            {
                targets: 1,
                data: 'nomenclator_id',
                name: 'nomenclator_id'
            },
            {
                targets: 2,
                data: 'description',
                name: 'description'
            },
            {
                targets: 3,
                data: 'valid_from',
                name: 'valid_from'
            },
            {
                targets: 4,
                data: 'valid_until',
                name: 'valid_until'
            },
            {
                targets: 5,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});


function showUploadPracticesAgreementModal(medicalInsuranceAgreementId)
{
    axios.get('/modals/billingDepartment/practicesAgreementModal/' + medicalInsuranceAgreementId)
        .then(function(result) {
            if(result.status === 200) {
                bootbox.dialog({
                    className: 'bounce',
                    message: result.data,
                    size: 'large',
                    buttons: {
                        cancel: {
                            label: "Cancelar",
                            className: 'btn-danger',
                        },
                        ok: {
                            label: "Subir archivo",
                            className: 'btn-info',
                            callback: function(){
                                //$('#uploadPracticesAgreement').submit();
                                uploadAgreementFile(medicalInsuranceAgreementId);
                                return false;
                            }
                        }
                    }
                })
            }
    }).catch(function(error) {
        toastrResponseError(error.response.status, error.response.data);
    });

}

function uploadAgreementFile(medicalInsuranceAgreementId)
{
    var formData = new FormData();
    var excelFile = document.querySelector('#practicesAgreementFile');
    formData.append("practicesAgreementFile", excelFile.files[0]);
    axios.post('/billingDepartment/uploadPracticesAgreement/' + medicalInsuranceAgreementId, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
    .then(function(result) {
        if(result.status === 200) {
            location.href = '/billingDepartment/medicalInsuranceAgreements';
        }
    })
    .catch(function(error) {
        toastrResponseError(error.response.status, error.response.data);
    })
}

function showPracticesAgreementModal(medicalInsuranceAgreementId)
{

}