var dataTable = null;

$(document).ready(function() {

    dataTable = $('#rangesValorizationTable').dataTable({
        processing: true,
        responsive: true,
        searching: false,
        paging: false,
        ordering: false,
        columnDefs: [
            {
                targets: 0,
                name: 'code',
                data: 'code'
            },
            {
                targets: 1,
                name: 'description',
                data: 'description'
            },
            {
                targets: 2,
                name: 'expense_unit_value',
                data: 'expense_unit_value'
            },
            {
                targets: 3,
                name: 'honorary_unit_value',
                data: 'honorary_unit_value'
            },
            {
                targets: 4,
                name: 'actions',
                data: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

    $('#rangesValorizationTable').on('click', '#deleteRow', function () {
        var data = dataTable.api().row($(this).closest('tr')).data();
        dataTable.api().row($(this).closest('tr')).remove().draw();
        enableOption(data.description);
    });

    addRangesValuesAgreementToTable();
});

function addRow()
{
    var code = $('select[name="range_id"]').val();
    var description = $('select[name="range_id"] option:selected').text();
    var expenseUnitValue =  $('input[name="expense_unit_value"]').val();
    var honoraryUnitValue =  $('input[name="honorary_unit_value"]').val();

    if(validateRangesValuesFields()) {
        dataTable.api().rows.add([
            {
                "code": code,
                "description": description,
                "expense_unit_value": expenseUnitValue,
                "honorary_unit_value": honoraryUnitValue,
                "actions": buttonHtml()
            }
        ]).draw(true);

        disableOption(description);
    }
    cleanRangesFields();
}

function validateRangesValuesFields()
{
    var code = $('select[name="range_id"]').val();
    var description = $('select[name="range_id"] option:selected').text();
    var expenseUnitValue =  $('input[name="expense_unit_value"]').val();
    var honoraryUnitValue =  $('input[name="honorary_unit_value"]').val();

    var isValid = true;
    if(! (isFloat(parseFloat(expenseUnitValue)) || isInt(parseInt(expenseUnitValue))) || ! (isFloat(parseFloat(honoraryUnitValue)) || isInt(parseInt(honoraryUnitValue)) )) {
        isValid = false;
        toastr.error('Por favor verifique que los valores de las unidades sean correctos.');
    }

    if(!isInt(parseInt(code))) {
        isValid = false;
        toastr.error('Por favor seleccione un rango');
    }

    return isValid;
}

function isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
}

function isInt(n) {
    return Number(n) === n && n % 1 === 0;
}

function cleanRangesFields()
{
    $('input[name="expense_unit_value"]').val('');
    $('input[name="honorary_unit_value"]').val('');
}

function buttonHtml()
{
    return '<button type="button" id="deleteRow" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>';
}

function storeAgreement()
{
    var description = $('input[name="description"]').val();
    var medical_insurance_id = $('select[name="medical_insurance_id"]').val();
    var nomenclator_id = $('select[name="nomenclator_id"]').val();
    var valid_from = $('input[name="valid_from"]').val();
    var valid_until = $('input[name="valid_until"]').val();
    var without_end_date = $('input[name="without_end_date"]').prop("checked") ? 1 : 0;
    var rangesValues = dataTable.fnGetData();

    var data = {
        description: description,
        medical_insurance_id: medical_insurance_id,
        nomenclator_id: nomenclator_id,
        valid_from: valid_from,
        valid_until: valid_until,
        without_end_date: without_end_date,
        rangesValues: rangesValues,
    };

    var agreementId = $('input[name="agreementId"]').val();

    $.ajax({
        url: '/billingDepartment/medicalInsuranceAgreements/' + agreementId,
        type: 'PATCH',
        data: data,
        success: function(response){
            location.href = '/billingDepartment/medicalInsuranceAgreements';
        },
        statusCode: {
            403: function (data) {
                var exception = data.responseJSON;
                addErrors(["No tiene permisos para editar el convenio."]);
            },
            422: function (data) {
                var exception = data.responseJSON;
                addErrors(exception.errors);
            },
            404: function (data) {
                var exception = data.responseJSON;
                addErrors(["Ha ocurrido un error. La ruta no ha sido encontrada. Contáctese con la oficina de sistemas."]);
            },
            500: function () {
                var exception = data.responseJSON;
                addErrors(["Ha ocurrido un error en el servidor. Contáctese con la oficina de sistemas."]);
            }
        }
    });
}

function disableOption(item)
{
    $('select[name="range_id"] option:contains(' + item + ')').attr("disabled",'disabled');
    $('select[name="range_id"]').val('');
    $('select[name="range_id"]').trigger("chosen:updated");
}

function enableOption(item)
{
    $('select[name="range_id"] option:contains(' + item + ')').removeAttr("disabled");
    $('select[name="range_id"]').val('');
    $('select[name="range_id"]').trigger("chosen:updated");
}