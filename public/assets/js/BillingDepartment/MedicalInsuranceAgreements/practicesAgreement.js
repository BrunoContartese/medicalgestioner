var dataTable = null;
var dialog;
$(document).ready(function() {

    dataTable =  $('#medicalInsurancePracticesAgreementsTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/billingDepartment/medicalInsurancePracticesAgreement/' + $('input[name="medicalInsuranceAgreementId"]').val(),
        columnDefs: [
            {
                targets: 0,
                data: 'code',
                name: 'code'
            },
            {
                targets: 1,
                data: 'description',
                name: 'description'
            },
            {
                targets: 2,
                data: 'fixed_expense_value',
                name: 'fixed_expense_value',
                searchable: false
            },
            {
                targets: 3,
                data: 'fixed_honorary_value',
                name: 'fixed_honorary_value',
                searchable: false
            },
            {
                targets: 4,
                data: 'helpers_quantity',
                name: 'helpers_quantity',
                searchable: false
            },
            {
                targets: 5,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

    $('#medicalInsurancePracticesAgreementsTable').on('click', '#deleteRow', function () {
        var data = dataTable.row($(this).closest('tr')).data();
        deletePractice(data);
        dataTable.row($(this).closest('tr')).remove().draw();
    });

});

function deletePractice(data)
{
    axios.post('/billingDepartment/practicesAgreement/' + $('input[name="medicalInsuranceAgreementId"]').val() + '/delete', data)
        .then(function(result) {
            if(result.status === 200) {
                toastr.success("Práctica quitada del convenio con éxito.");
                dataTable.ajax.reload();
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}

function addPracticeModal()
{
    axios.get('/modals/billingDepartment/addPracticeAgreementModal')
        .then(function(result) {
            if(result.status === 200) {
                 dialog = bootbox.dialog({
                                className: 'bounce',
                                message: result.data,
                                size: 'large',
                                onScape: true,
                                buttons: {
                                    cancel: {
                                        label: "Cancelar",
                                        className: 'btn-danger',
                                    },
                                    ok: {
                                        label: "Añadir práctica",
                                        className: 'btn-info',
                                        callback: function(){
                                            storePracticeAgreement(dialog);
                                            return false;
                                        }
                                    }
                                }
                            }).init(function() {
                               $('.chosen-select').chosen({
                                   width: '100%'
                               });
                            });
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}

function storePracticeAgreement()
{
    var data = {
        nomenclator_practice_id: $('select[name="nomenclator_practice_id"]').val(),
        fixed_expense_value:  $('input[name="fixed_expense_value"]').val(),
        fixed_honorary_value:  $('input[name="fixed_honorary_value"]').val()
    };

    axios.post('/billingDepartment/practicesAgreement/' +  $('input[name="medicalInsuranceAgreementId"]').val(), data)
        .then(function(result) {
            if(result.status === 200) {
                bootbox.hideAll();
                dataTable.ajax.reload();
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}

function getPracticeInfoById()
{
    axios.get('/administration/nomenclatorPractices/get/' + $('select[name="nomenclator_practice_id"]').val())
        .then(function(result) {
            if(result.status === 200) {
                $('textarea[name="description"]').val(result.data.description);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}