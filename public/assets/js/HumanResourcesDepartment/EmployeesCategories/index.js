var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#employeesCategoriesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/humanResourcesDepartment/employeesCategories',
        columnDefs: [
            {
                targets: 0,
                data: 'tango_code',
                name: 'tango_code'
            },
            {
                targets: 1,
                data: 'name',
                name: 'name'
            },
            {
                targets: 2,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});