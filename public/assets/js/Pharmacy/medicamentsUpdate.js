var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#medicamentUpdatesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/pharmacy/medicamentUpdates',
        columnDefs: [
            {
                targets: 0,
                data: 'created_at',
                name: 'created_at'
            },
            {
                targets: 1,
                data: 'user_id',
                name: 'user_id'
            },
            {
                targets: 2,
                data: 'medicament_update_status_id',
                name: 'medicament_update_status_id',
                searchable: false
            },
            {
                targets: 3,
                data: 'time',
                name: 'time',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});

function uploadZipFile()
{
    var formData = new FormData();
    var zipFile = document.querySelector('#file');
    formData.append("file", zipFile.files[0]);
    $(".se-pre-con").fadeIn("slow");
    axios.post('/pharmacy/medicamentsUpdate', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
        .then(function(result) {
            if(result.status === 200) {
                location.href = '/pharmacy/medicaments';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $(".se-pre-con").fadeOut("slow");
        })
}