var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#pharmacyTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/pharmacy/medicaments',
        columnDefs: [
            {
                targets: 0,
                data: 'barcode',
                name: 'barcode'
            },
            {
                targets: 1,
                data: 'name',
                name: 'name'
            },
            {
                targets: 2,
                data: 'presentation',
                name: 'presentation',
                searchable: false
            },
            {
                targets: 3,
                data: 'medicament_drug_code',
                name: 'medicament_drug_code'
            },
            {
                targets: 4,
                data: 'laboratory_code',
                name: 'laboratory_code'
            },
            {
                targets: 5,
                data: 'sell_price',
                name: 'sell_price',
                searchable: false
            },
            {
                targets: 6,
                data: 'on_stock',
                name: 'on_stock',
                searchable: false
            },
            {
                targets: 7,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});
