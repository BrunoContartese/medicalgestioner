var editor;
$(document).ready(function() {
    editor = textboxio.replace('.editor');
});

function changeMedicationDiscountPercentageState()
{
    var status = $('input[name="medication_discount_percentage"]');

    if(status.prop('disabled') === false) {
        status.val("");
        status.prop('disabled', 'disabled');
    } else {
        status.removeAttr('disabled');
    }
}

function storeMedicalInsurance()
{

    var data = {
        _token: $('input[name="_token"]').val(),
        name: $('input[name="name"]').val(),
        cuit: $('input[name="cuit"]').val(),
        billing_address: $('input[name="billing_address"]').val(),
        perceives_gross_income: $('input[name="perceives_gross_income"]').prop('checked') ? 1 : 0,
        apply_discount_on_medications: $('input[name="apply_discount_on_medications"]').prop('checked') ? 1 : 0,
        medication_discount_percentage: $('input[name="medication_discount_percentage"]').val(),
        contact: editor.content.get()
    };

    $.ajax({
        url: "/administration/medicalInsurances",
        type: "POST",
        data: data,
        success: function(result) {
            location.href = "/administration/medicalInsurances";
        },
        error: function(data) {
            var exeption = data.responseJSON;
            addErrors(exeption.errors);
        }
    })
}