var dataTable = null;
$(document).ready(function() {

    dataTable =  $('#medicalInsurancesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/medicalInsurances',
        columnDefs: [
            {
                targets: 0,
                data: 'name',
                name: 'name'
            },
            {
                targets: 1,
                data: 'billing_address',
                name: 'billing_address',
                searchable: false
            },
            {
                targets: 2,
                data: 'deleted_at',
                name: 'deleted_at',
                searchable: false
            },
            {
                targets: 3,
                data: 'updated_by',
                name: 'updated_by',
                searchable: false
            },
            {
                targets: 4,
                data: 'news_board',
                name: 'news_board',
                searchable: false
            },
            {
                targets: 5,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});

function restoreTrashed(medicalInsuranceId)
{
    var data = {
        _token: $('input[name="_token"]').val()
    };

    $.ajax({
        url: '/administration/medicalInsurance/' + medicalInsuranceId + '/restore',
        type: "POST",
        data: data,
        success: function(result) {
            location.href = "/administration/medicalInsurances"
        },
        error: function(data) {
            var exeption = data.responseJSON;
            addErrors(exeption.errors);
        }
    });
}

function confirmTrash(id)
{
    bootbox.confirm({
        message: "¿Está seguro que desea suspender la obra social?",
        class: "bounceIn",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-success'
            }
        },
        callback: function (result) {
            if(result) {
                $("#"+id).submit();
            } else {
                bootbox.hideAll();
            }
        }
    });
}

function confirmRestoreTrashed(id)
{
    bootbox.confirm({
        message: "¿Está seguro que desea habilitar la obra social?",
        class: "bounceIn",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-success'
            }
        },
        callback: function (result) {
            if(result) {
                restoreTrashed(id);
            } else {
                bootbox.hideAll();
            }
        }
    });
}
