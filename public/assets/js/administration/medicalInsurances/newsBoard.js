var editor;
$(document).ready(function() {
    editor = textboxio.replace('#editor');
});
function storeMedicalInsuranceNews(medicalInsuranceId)
{
    var data = {
        _token: $('input[name="_token"]').val(),
        news: editor.content.get()
    };

    $.ajax({
        url: "/administration/medicalInsurance/storeNews/" + medicalInsuranceId,
        type: "POST",
        data: data,
        success: function(result) {
            location.href = "/administration/medicalInsurances";
        },
        error: function(data) {
            var exeption = data.responseJSON;
            addErrors(exeption.errors, "newsErrors");
        }
    });
}