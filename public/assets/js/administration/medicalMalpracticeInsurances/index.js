var dataTable = null;
$(document).ready(function() {

    dataTable =  $('#medicalMalpracticeInsurancesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/medicalMalpracticeInsurances',
        columnDefs: [
            {
                targets: 0,
                data: 'name',
                name: 'name'
            },
            {
                targets: 1,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});