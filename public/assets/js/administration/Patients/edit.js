var dataTable = 0;
$(document).ready(function() {
    dataTable =  $('#patientMedicalInsurancesTable').dataTable({
        responsive: true,
        sort: false,
        columnDefs: [
            {
                targets: 0,
                data: 'medical_insurance_id',
                name: 'medical_insurance_id'
            },
            {
                targets: 1,
                data: 'medical_insurance_name',
                name: 'medical_insurance_name'
            },
            {
                targets: 2,
                data: 'afilliate_number',
                name: 'afilliate_number'
            },
            {
                targets: 3,
                data: 'expiration_date',
                name: 'expiration_date'
            },
            {
                targets: 4,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

    $('#patientMedicalInsurancesTable').on('click', '#deleteRow', function () {
        var data = dataTable.api().row($(this).closest('tr')).data();
        enableOption(data.medical_insurance_name);
        dataTable.api().row($(this).closest('tr')).remove().draw();
    });

    addMedicalInsurancesToTable();
});

function getCities()
{
    var provinceId = $('select[name="province_id"]').val();

    axios.get('/helpers/cities/cityByProvinceId/' + provinceId)
        .then(function(response) {
            if(response.status === 200) {
                $('#citiesDiv').html(response.data);
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        })
        .finally(function() {
            $('.chosen-select').chosen();
        });
}

function addMedicalInsuranceToTable()
{
    var medicalInsuranceId = $('select[name="medical_insurance_id"]').val();
    var medicalInsuranceName = $('select[name="medical_insurance_id"] option:selected').text();
    var afilliate_number = $('input[name="afilliate_number"]').val();
    var expirationDate = $('input[name="expiration_date"]').val();

    dataTable.api().rows.add([
        {
            "medical_insurance_id": medicalInsuranceId,
            "medical_insurance_name": medicalInsuranceName,
            "afilliate_number": afilliate_number,
            "expiration_date": moment(expirationDate).format('L'),
            "actions": buttonHtml()
        }
    ]).draw(true);

    disableOptionAndCleanFields(medicalInsuranceName);
}

function buttonHtml()
{
    return '<button type="button" id="deleteRow" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>';
}

function disableOptionAndCleanFields(item)
{
    $('select[name="medical_insurance_id"] option:contains(' + item + ')').attr("disabled",'disabled');
    $('select[name="medical_insurance_id"]').val('');
    $('select[name="medical_insurance_id"]').trigger("chosen:updated");

    $('input[name="afilliate_number"]').val('');
    $('input[name="expiration_date"]').val('');
}

function enableOption(item)
{
    $('select[name="medical_insurance_id"] option:contains(' + item + ')').removeAttr("disabled");
    $('select[name="medical_insurance_id"]').val('');
    $('select[name="medical_insurance_id"]').trigger("chosen:updated");
}

function updatePatient()
{
    var data = {
        medicalInsurancesTable: dataTable.fnGetData(),
        name: $('input[name="name"]').val(),
        birthday: $('input[name="birthday"]').val(),
        document_type_id: $('select[name="document_type_id"]').val(),
        document_number: $('input[name="document_number"]').val(),
        gender_id: $('select[name="gender_id"]').val(),
        city_id: $('select[name="city_id"]').val(),
        phone_number: $('input[name="phone_number"]').val(),
        address: $('input[name="address"]').val(),
        mobile_phone_area_code: $('input[name="mobile_phone_area_code"]').val(),
        mobile_phone_number: $('input[name="mobile_phone_number"]').val(),
    };

    var patientId = $('input[name="id"]').val();
    var route = '/administration/patients/' + patientId;

    axios.put(route, data)
        .then(function(response) {
            if(response.status === 200) {
                location.href = '/administration/patients';
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}