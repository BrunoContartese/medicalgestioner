var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#patientsTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/patients',
        columnDefs: [
            {
                targets: 0,
                data: 'name',
                name: 'name'
            },
            {
                targets: 1,
                data: 'document_type',
                name: 'document_type'
            },
            {
                targets: 2,
                data: 'document_number',
                name: 'document_number'
            },
            {
                targets: 3,
                data: 'more_info',
                name: 'more_info'
            },
            {
                targets: 4,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});

function showPatientInfo(patientId)
{
    axios.get('/administration/patients/' + patientId)
        .then(function(response) {
            if(response.status === 200) {
                bootbox.alert({
                    message: response.data,
                    className: 'modal80'
                });
            }
        })
        .catch(function(error) {
            toastrResponseError(error.response.status, error.response.data);
        });
}
