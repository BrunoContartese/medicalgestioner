var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#usersTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/users',
        columnDefs: [
            {
                targets: 0,
                data: 'name',
                name: 'name'
            },
            {
                targets: 1,
                data: 'username',
                name: 'username',
                searchable: false
            },
            {
                targets: 2,
                data: 'email',
                name: 'email',
                searchable: false
            },
            {
                targets: 3,
                data: 'last_login_at',
                name: 'last_login_at',
                searchable: false
            },
            {
                targets: 4,
                data: 'last_login_ip',
                name: 'last_login_ip',
                searchable: false
            },
            {
                targets: 5,
                data: 'actions',
                name: 'actions',
                className: 'actions',
                width: '20%',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});