var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#rolesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/roles',
        columnDefs: [
            {
                targets: 0,
                data: 'name',
                name: 'name'
            },
            {
                targets: 1,
                data: 'permissions',
                name: 'permissions',
                searchable: false
            },
            {
                targets: 2,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});