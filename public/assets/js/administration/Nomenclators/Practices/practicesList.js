var dataTable = null;
$(document).ready(function() {
    var nomenclatorId = $('input[name="nomenclatorId"]').val();

    dataTable =  $('#nomenclatorPracticesListTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/nomenclatorPractices/' + nomenclatorId,
        columnDefs: [
            {
                targets: 0,
                data: 'code',
                name: 'code'
            },
            {
                targets: 1,
                data: 'is_module',
                name: 'is_module',
                searchable: false
            },
            {
                targets: 2,
                data: 'description',
                name: 'description',
            },
            {
                targets: 3,
                data: 'honorary_units',
                name: 'honorary_units',
                searchable: false
            },
            {
                targets: 4,
                data: 'helper_honorary_units',
                name: 'helper_honorary_units',
                searchable: false
            },
            {
                targets: 5,
                data: 'expense_units',
                name: 'expense_units',
                searchable: false
            },
            {
                targets: 6,
                data: 'helpers_quantity',
                name: 'helpers_quantity',
                searchable: false
            },
            {
                targets: 7,
                data: 'actions',
                name: 'actions',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});

function confirmChangeHelpersQuantity(practiceId)
{
    bootbox.confirm({
        message: "¿Está seguro de que desea cambiar la cantidad de ayudantes?",
        class: "bounceIn",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-success'
            }
        },
        callback: function (result) {
            if(result) {
                changeHelpersQuantity(practiceId);
            } else {
                bootbox.hideAll();
            }
        }
    });
}


function changeHelpersQuantity(practiceId)
{
    $.ajax({
        url: "/administration/nomenclatorPractices/changeHelpersQuantity/" + practiceId,
        type: "POST",
        data: {_token: $('input[name="_token"]').val()},
        success: function(result) {
            $('#changeHelpersQuantityButton-' + practiceId).html(result);
        },
        statusCode: {
            403: function () {
                bootbox.alert('No tiene el nivel de autorización necesario para realizar el cambio.');
            },
            422: function(data) {
                var exception = data.responseJSON;
                addErrors(exception.errors);
            }
        }
    });
}