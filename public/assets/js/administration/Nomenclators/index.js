var dataTable = null;
$(document).ready(function() {

    dataTable =  $('#nomenclatorsTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/nomenclators',
        columnDefs: [
            {
                targets: 0,
                data: 'name',
                name: 'name'
            },
            {
                targets: 1,
                data: 'status',
                name: 'status'
            },
            {
                targets: 2,
                data: 'practicesListButton',
                name: 'practicesListButton',
                searchable: false
            },
            {
                targets: 3,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});