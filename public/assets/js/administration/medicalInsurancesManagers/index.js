var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#medicalInsurancesManagersTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/medicalInsurancesManagers',
        columnDefs: [
            {
                targets: 0,
                data: 'medical_insurance_id',
                name: 'medical_insurance_id'
            },
            {
                targets: 1,
                data: 'name',
                name: 'name'
            },
            {
                targets: 2,
                data: 'billing_address',
                name: 'billing_address',
                searchable: false
            },
            {
                targets: 3,
                data: 'updated_by',
                name: 'updated_by',
                searchable: false
            },
            {
                targets: 4,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});