var editor;
$(document).ready(function() {
    editor = textboxio.replace('.editor');
});

function storeMedicalInsuranceManager()
{

    var data = {
        _token: $('input[name="_token"]').val(),
        medical_insurance_id: $('select[name="medical_insurance_id"]').val(),
        name: $('input[name="name"]').val(),
        cuit: $('input[name="cuit"]').val(),
        billing_address: $('input[name="billing_address"]').val(),
        perceives_gross_income: $('input[name="perceives_gross_income"]').prop('checked') ? 1 : 0,
        contact: editor.content.get()
    };

    $.ajax({
        url: "/administration/medicalInsurancesManagers",
        type: "POST",
        data: data,
        success: function(result) {
            location.href = "/administration/medicalInsurancesManagers";
        },
        error: function(data) {
            var exeption = data.responseJSON;
            addErrors(exeption.errors);
        }
    })
}