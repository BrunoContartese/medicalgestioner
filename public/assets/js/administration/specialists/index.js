var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#specialistsTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/specialists',
        columnDefs: [
            {
                targets: 0,
                data: 'cuit',
                name: 'cuit',
            },
            {
                targets: 1,
                data: 'licence',
                name: 'licence',
                searchable: false
            },
            {
                targets: 2,
                data: 'name',
                name: 'name',
                searchable: false
            },
            {
                targets: 3,
                data: 'specialties',
                name: 'specialties',
                searchable: false
            },

            {
                targets: 4,
                data: 'actions',
                name: 'actions',
                className: 'actions',
                width: '20%',
                searchable: false
            },
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});
