var dataTable = null;

$(document).ready(function() {

    dataTable =  $('#nomenclatorRangesTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '/datatable/administration/nomenclatorRanges',
        columnDefs: [
            {
                targets: 0,
                data: 'nomenclator_id',
                name: 'nomenclator_id'
            },
            {
                targets: 1,
                data: 'from_code',
                name: 'from_code'
            },
            {
                targets: 2,
                data: 'to_code',
                name: 'to_code',
                searchable: false
            },
            {
                targets: 3,
                data: 'actions',
                name: 'actions',
                searchable: false
            }
        ],
        language: {url: '/assets/css/datatables/spanish.json'}
    });

});