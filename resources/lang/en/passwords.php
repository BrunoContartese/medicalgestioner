<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menos 8 caracteres y debe coincidir con la confirmación',
    'reset' => 'Su contraseña ha sido blanqueada.',
    'sent' => 'Le hemos envíado un mail para blanquear su contraseña.',
    'token' => 'El token es inválido. Por favor reintente recuperar su contraseña nuevamente.',
    'user' => "El E-Mail ingresado no existe en nuestra base de datos.",

];
