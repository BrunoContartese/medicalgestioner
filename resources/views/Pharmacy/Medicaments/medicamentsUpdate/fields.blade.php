<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-primary">
            <header class="panel-heading">
                <h3 class="panel-title">Por favor seleccione el archivo .ZIP descargado de ALFABETA</h3>
            </header>
            <div class="panel-body">
                @include('layouts.errors')
                <input type="file" class="form-control" name="file" id="file" />
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <button type="button" class="btn btn-warning" onclick="uploadZipFile();">Subir Archivo</button>
            <a href="{!! route('medicaments.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
