<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <h3 class="panel-title">Historial de actualizaciones</h3>
            </header>
            <div class="panel-body">
                @include('layouts.partials.table', [
                            'tableTitle' => '',
                            'tableId' => 'medicamentUpdatesTable',
                            'tableColumns' => ['Fecha', 'Usuario', 'Estado de actualización', 'Tiempo de actualización'],
                        ])
            </div>
        </section>
    </div>
</div>