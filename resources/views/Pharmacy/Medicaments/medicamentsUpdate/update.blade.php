@extends('layouts.app')
@section('pageName')
    Actualizar medicamentos
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                </header>
                <h3 class="panel-title"></h3>
                <div class="panel-body">
                    @include('layouts.errors')
                    @include('Pharmacy.Medicaments.medicamentsUpdate.fields')
                </div>
            </section>
        </div>
    </div>

    @include('Pharmacy.Medicaments.medicamentsUpdate.medicamentUpdatesHistory')
@endsection

@section('scripts')
    {{ Html::script('/assets/js/Pharmacy/medicamentsUpdate.js') }}
@endsection