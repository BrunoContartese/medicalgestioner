@extends('layouts.app')
@section('pageName')
    Editar Medicamento
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h3 class="panel-title"></h3>
                </header>
                <div class="panel-body">
                    @include('layouts.errors')
                    @include('Pharmacy.Medicaments.fields')
                </div>
            </section>
        </div>
    </div>
@endsection

