@if($row->on_stock == 0)
    <button title="Fuera de Stock" type="button" onclick="addMedicamentToStock({{$row->id}});" class="btn btn-danger"><i class="fa fa-times"></i></button>
@else
    <button title="En Stock" type="button" onclick="removeMedicamentFromStock({{$row->id}});" class="btn btn-success"><i class="fa fa-check"></i></button>
@endif