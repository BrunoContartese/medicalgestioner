@can('medicaments.edit')
    <a href="{{route('medicaments.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
@endcan


