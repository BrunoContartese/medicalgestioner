@extends('layouts.app')
@section('pageName')
    Medicamentos
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/Pharmacy/Medicaments/create']),
        'tableId' => 'pharmacyTable',
        'tableColumns' => ['Código de Barras', 'Nombre', 'Presentación', 'Droga', 'Laboratorio', 'Precio', '¿En stock?', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/Pharmacy/index.js') }}
@endsection
