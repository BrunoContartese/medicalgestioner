<div class="form-group">
    {{ Form::label('label', 'Troquel', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('label', $medicament->label, ['class' => 'form-control', 'disabled']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('barcode', 'Código de barras', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('barcode', $medicament->barcode, ['class' => 'form-control', 'disabled']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('name', 'Medicamento', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', $medicament->name, ['class' => 'form-control', 'disabled']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('presentation', 'Presentación', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('presentation', $medicament->presentation, ['class' => 'form-control', 'disabled']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('laboratory_code', 'Laboratorio', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('laboratory_code', \CloudMed\Models\Pharmacy\PharmaceuticalLaboratory::all()->pluck('name','code'), isset($medicament->laboratory) ? $medicament->laboratory->code : null, ['class' => 'form-control chosen-select', 'disabled', 'placeholder' => '-']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('medicament_drug_code', 'Droga', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medicament_drug_code', \CloudMed\Models\Pharmacy\MedicamentDrug::all()->pluck('description','code'), isset($medicament->drug) ? $medicament->drug->code : null, ['class' => 'form-control chosen-select', 'disabled', 'placeholder' => '-']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('medicament_administration_line_code', 'Vía de Administración', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medicament_administration_line_code', \CloudMed\Models\Pharmacy\MedicamentAdministrationLine::all()->pluck('description','code'), isset($medicament->administrationLine) ? $medicament->administrationLine->code : null, ['class' => 'form-control chosen-select', 'disabled', 'placeholder' => '-']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('medicament_category_id', 'Categoría', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medicament_category_id', \CloudMed\Models\Pharmacy\MedicamentCategory::all()->pluck('description', 'id'), $medicament->medicament_category_id, ['class' => 'form-control chosen-select', 'disabled', 'placeholder' => '-']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('medicament_pharmacological_action_code', 'Acción farmacológica', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medicament_pharmacological_action_code', \CloudMed\Models\Pharmacy\MedicamentPharmacologicalAction::all()->pluck('description','code'), isset($medicament->pharmacologicalAction) ? $medicament->pharmacologicalAction->code : null, ['class' => 'form-control chosen-select', 'disabled', 'placeholder' => '-']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('units', 'Unidades x blister', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('units', $medicament->units, ['class' => 'form-control', 'disabled']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('cost_price', 'Precio de Costo:', ['class' => 'col-md-3 control-label', 'disabled']) }}
    <div class="col-md-6">
        {{ Form::text('cost_price', $medicament->cost_price, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('sell_price', 'Precio de Venta:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('sell_price', $medicament->sell_price, ['class' => 'form-control', 'disabled']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('medicaments.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
