{!! Form::open(['id' => $id, 'route' => ['employeesCategories.destroy', $id], 'method' => 'delete']) !!}
@can('employeesCategories.edit')
    <a href="{{route('employeesCategories.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt" title="Editar categoría"></i></a>
@endcan
@can('employeesCategories.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
            'class' => 'btn btn-sm',
            'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar la categoría de empleado?'); return false;",
            'title' => 'Eliminar categoría'
    ]) !!}
@endcan
{!! Form::close() !!}

