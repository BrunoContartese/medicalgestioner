@extends('layouts.app')
@section('pageName')
    Categorías de empleados
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/humanResourcesDepartment/employeesCategories/create']),
        'tableId' => 'employeesCategoriesTable',
        'tableColumns' => ['Código de tango', 'Nombre', 'Acciones']
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/HumanResourcesDepartment/EmployeesCategories/index.js') }}
@endsection
