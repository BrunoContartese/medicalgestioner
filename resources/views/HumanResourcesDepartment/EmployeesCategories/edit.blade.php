@extends('layouts.app')
@section('pageName')
    Editar Deposito
@endsection


@section('content')
    {!! Form::model($category, ['route' => ['employeesCategories.update', $category->id], 'method' => 'patch']) !!}
        @include('HumanResourcesDepartment.EmployeesCategories.fields')
    {!! Form::close() !!}
@endsection

