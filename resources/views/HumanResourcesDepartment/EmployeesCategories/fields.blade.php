<div class="row">
    @include('layouts.errors')
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-featured panel-featured-danger">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Datos Obligatorios</h3>
            </header>

            <div class="panel-body">

                <div class="form-group">
                    {{ Form::label('tango_code', 'Código de tango', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('tango_code', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('name', 'Nombre de la categoría', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('employee_agreement_id', 'Convenio', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::select('employee_agreement_id', \CloudMed\Models\HumanResourcesDepartment\EmployeeAgreement::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una opción']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('employee_settlement_method_id', 'Método de liquidación', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::select('employee_settlement_method_id', \CloudMed\Models\HumanResourcesDepartment\EmployeeSettlementMethod::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una opción']) }}
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>

{{--
/*Datos opcionales*/
            $table->float('suggested_salary_amount')->nullable();
            $table->float('minimum_salary_amount')->nullable();
            $table->float('maximum_salary_amount')->nullable();
            $table->float('extra_hour_amount')->nullable();
            $table->float('salary_matrix_amount')->nullable();
            $table->integer('days_per_month')->nullable();
            $table->integer('days_per_week')->nullable();
            $table->float('hours_per_day')->nullable();
            $table->float('hours_per_week')->nullable();
            $table->float('hours_per_month')->nullable();
            --}}
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-featured panel-featured-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Datos Opcionales</h3>
            </header>

            <div class="panel-body">

                <div class="form-group">
                    {{ Form::label('suggested_salary_amount', 'Salario sugerido', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('suggested_salary_amount', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('minimum_salary_amount', 'Salario mínimo', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('minimum_salary_amount', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('maximum_salary_amount', 'Salario máximo', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('maximum_salary_amount', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('extra_hour_amount', 'Valor de hora extra', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('extra_hour_amount', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('salary_matrix_amount', 'Matriz sueldo', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('salary_matrix_amount', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('days_per_month', 'Días por mes', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('days_per_month', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('days_per_week', 'Días por semana', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('days_per_week', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('hours_per_day', 'Horas por día', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('hours_per_day', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('hours_per_week', 'Horas por semana', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('hours_per_week', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('hours_per_month', 'Horas por mes', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('hours_per_month', null, ['class' => 'form-control']) }}
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('employeesCategories.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
