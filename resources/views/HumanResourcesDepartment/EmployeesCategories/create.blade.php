@extends('layouts.app')
@section('pageName')
    Crear categoría de empleado
@endsection

@section('content')
    {!! Form::open(['route' => 'employeesCategories.store']) !!}
        @include('HumanResourcesDepartment.EmployeesCategories.fields')
    {!! Form::close() !!}
@endsection