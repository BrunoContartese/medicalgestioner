{!! Form::open(['id' => $id, 'route' => ['medicalMalpracticeInsurances.destroy', $id], 'method' => 'delete']) !!}
        @can('medicalMalpracticeInsurances.edit')
                <a href="{{route('medicalMalpracticeInsurances.edit', $id)}}" class="btn btn-sm btn-default" title="Editar seguro"><i class="fa fa-pencil-alt"></i></a>
        @endcan
        @can('medicalMalpracticeInsurances.delete')
                {!! Form::button('<i class="fa fa-trash"></i>', [
                        'class' => 'btn btn-sm',
                        'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el seguro de mala práxis?'); return false;",
                        'title' => 'Eliminar seguro'
                ]) !!}
        @endcan
{!! Form::close() !!}
