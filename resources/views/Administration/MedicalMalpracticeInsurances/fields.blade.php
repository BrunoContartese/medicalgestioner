<div class="form-group">
    {{ Form::label('name', 'Nombre del seguro', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
        <a href="{!! route('medicalMalpracticeInsurances.index') !!}" class="btn btn-danger">Cancelar</a>
    </div>
</div>
