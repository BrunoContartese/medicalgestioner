@extends('layouts.app')
@section('pageName')
    Seguros de mala práxis
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/medicalMalpracticeInsurances/create']),
        'tableId' => 'medicalMalpracticeInsurancesTable',
        'tableColumns' => ['Nombre', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/medicalMalpracticeInsurances/index.js') }}
@endsection