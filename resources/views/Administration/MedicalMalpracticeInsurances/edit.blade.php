@extends('layouts.app')
@section('pageName')
    Seguros de mala práxis
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                </header>
                <h3 class="panel-title"></h3>
                <div class="panel-body">
                    @include('layouts.errors')
                    {!! Form::model($medicalMalpracticeInsurance, ['route' => ['medicalMalpracticeInsurances.update', $medicalMalpracticeInsurance->id], 'method' => 'patch', 'class' => 'form-horizontal form-bordered']) !!}
                        @include('Administration.MedicalMalpracticeInsurances.fields')
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection
