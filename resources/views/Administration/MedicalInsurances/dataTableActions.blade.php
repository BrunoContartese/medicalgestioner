@can('medicalInsurances.edit')
        <a title="Modificar" href="{{route('medicalInsurances.edit', $id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
@endcan
@if(is_null($row->deleted_at))
        @can('medicalInsurances.delete')
            {!! Form::open(['id' => $id, 'route' => ['medicalInsurances.destroy', $id], 'method' => 'delete', 'style' => 'display: inline;']) !!}
                {!! Form::button('<i class="fa fa-check"></i>', [
                        'class' => 'btn btn-sm btn-success',
                        'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea suspender la obra social?'); return false;",
                        'title' => 'Suspender Obra Social'
                ]) !!}
            {!! Form::close() !!}
        @endcan
@else
        {!! Form::open(['id' => $id, 'route' => ['medicalInsurances.restore', $id], 'method' => 'post', 'style' => 'display: inline;']) !!}
            {!! Form::button('<i class="fa fa-times"></i>', [
                        'class' => 'btn btn-sm btn-danger',
                        'onclick' => "confirmSubmit($id, '¿Está seguro de que desea habilitar la obra social?'); return false;",
                        'title' => 'Habilitar Obra Social'
            ]) !!}
        {!! Form::close() !!}
@endif