@extends('layouts.app')
@section('pageName')
    Boletín de noticias
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                </header>
                <h3 class="panel-title"></h3>
                <div class="panel-body">
                    @include('layouts.errors')
                    @include('Administration.MedicalInsurances.newsBoardFields')
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/medicalInsurances/newsBoard.js') }}
@endsection
