<div class="form-group">
    {{ Form::label('name', 'Nombre de la obra social', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('email', 'Cuit', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('cuit', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('username', 'Dirección de facturación', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('billing_address', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group col-lg-12">
    <div class="">
        @if(isset($medicalInsurance))
            @if($medicalInsurance->perceives_gross_income == 1)
                <input type="checkbox" name="perceives_gross_income" value="1" checked/>
            @else
                <input type="checkbox" name="perceives_gross_income" value="1"/>
            @endif
        @else
            <input type="checkbox" name="perceives_gross_income" value="1"/>
        @endif
        <label>¿Percibe IIBB?</label>
    </div>
</div>

<div class="form-group col-lg-12">
    <div class="">
        @if(isset($medicalInsurance))
            @if($medicalInsurance->apply_discount_on_medications == 1)
                <input type="checkbox" name="apply_discount_on_medications" onchange="changeMedicationDiscountPercentageState();" value="1" checked/>
            @else
                <input type="checkbox" name="apply_discount_on_medications" onchange="changeMedicationDiscountPercentageState();" value="1"/>
            @endif
        @else
            <input type="checkbox" name="apply_discount_on_medications" onchange="changeMedicationDiscountPercentageState();" value="1"/>
        @endif
        <label>¿Aplica descuento a medicaciones?</label>
    </div>
</div>

<div class="form-group">
    {{ Form::label('medication_discount_percentage', 'Descuento a medicación (%)', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('medication_discount_percentage', null, ['class' => 'form-control', isset($medicalInsurance) && $medicalInsurance->apply_discount_on_medications == 1 ? "" : 'disabled']) }}
    </div>
</div>

<div class="form-group col-lg-12">
    {{ Form::label('contact', 'Contacto / Contactos', ['class' => 'col-md-3 control-label']) }}

    {{ Form::textarea('contact', null, ['class' => 'form-control editor']) }}
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::button('Guardar', ['class' => 'btn btn-warning', 'onclick' => 'storeMedicalInsurance();']) !!}
        <a href="{!! route('medicalInsurances.index') !!}" class="btn btn-danger">Cancelar</a>
    </div>
</div>
