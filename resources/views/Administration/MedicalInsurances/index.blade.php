@extends('layouts.app')
@section('pageName')
    Obras Sociales
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/medicalInsurances/create']),
        'tableId' => 'medicalInsurancesTable',
        'tableColumns' => ['Nombre', 'Dirección de facturación', 'Estado', 'Última modificación', 'Boletín de Noticias', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/medicalInsurances/index.js') }}
@endsection