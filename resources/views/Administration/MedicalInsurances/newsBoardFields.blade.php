<div class="form-group col-lg-12">
    <textarea class="gorm-control" id="editor" rows="50">
        @if(is_null($medicalInsurance->news))
            <h2><b>Centro de Imágenes</b></h2>
            <ul>
                <li></li>
            </ul>
            <h2><b>Internación</b></h2>
            <ul>
                <li></li>
            </ul>
            <h2><b>Guardia</b></h2>
            <ul>
                <li></li>
            </ul>
            @else
                {!! $medicalInsurance->news !!}
            @endif
    </textarea>
</div>

<div class="form-group">
    {!! Form::button('Guardar', ['class' => 'btn btn-warning', 'onclick' => "storeMedicalInsuranceNews($medicalInsurance->id);"]) !!}
    <a href="{!! route('medicalInsurances.index') !!}" class="btn btn-danger">Cancelar</a>
</div>