@extends('layouts.app')
@section('pageName')
    Editar obra social
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                </header>
                <h3 class="panel-title"></h3>
                <div class="panel-body">
                    @include('layouts.errors')
                    <div id="ajaxErrors"></div>
                    <input type="hidden" name="id" id="id" value="{{$medicalInsurance->id}}" />
                    {!! Form::model($medicalInsurance, ['route' => ['users.update', $medicalInsurance->id], 'method' => 'patch', 'class' => 'form-horizontal form-bordered']) !!}
                        @include('Administration.MedicalInsurances.fields')
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripts')
    {!! Html::script('/assets/js/administration/medicalInsurances/edit.js') !!}
@endsection