{!! Form::open(['id' => $id, 'route' => ['roles.destroy', $id], 'method' => 'delete']) !!}
        @can('roles.edit')
                <a href="{{route('roles.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
        @endcan
        @can('roles.delete')
                {!! Form::button('<i class="fa fa-trash"></i>', [
                        'class' => 'btn btn-sm btn-default',
                        'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el usuario?'); return false;"
                ]) !!}
        @endcan
{!! Form::close() !!}
