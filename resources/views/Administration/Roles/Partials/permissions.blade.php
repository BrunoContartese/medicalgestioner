@foreach(\Spatie\Permission\Models\Permission::all() as $permission)
    <div class="form-group {{$permission->name == 'dashboard.view' ? 'col-lg-12' : 'col-lg-3'}}">
        <div class="">
            @if($role->hasPermissionTo($permission->name))
                <input type="checkbox" name="permissions[]" value="{{$permission->name}}" checked/>
            @else
                <input type="checkbox" name="permissions[]" value="{{$permission->name}}"/>
            @endif
            <label>
                <b>{{$permission->show_name}}</b>
            </label>
        </div>
    </div>
@endforeach

<div class="col-md-6">
    <div class="form-group">
        {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
        <a href="{!! route('roles.index') !!}" class="btn btn-danger">Cancelar</a>
    </div>
</div>
