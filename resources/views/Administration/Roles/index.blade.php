@extends('layouts.app')
@section('pageName')
    Perfiles
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/roles/create']),
        'tableId' => 'rolesTable',
        'tableColumns' => ['Nombre', 'Permisos', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/roles/index.js') }}
@endsection