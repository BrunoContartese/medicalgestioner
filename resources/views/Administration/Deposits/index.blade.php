@extends('layouts.app')
@section('pageName')
    Depositos
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/deposits/create']),
        'tableId' => 'depositsTable',
        'tableColumns' => ['Depositos', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/deposits/index.js') }}
@endsection
