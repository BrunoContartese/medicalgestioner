<div class="form-group">
    {{ Form::label('name', 'Nombre del Deposito', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('deposits.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
