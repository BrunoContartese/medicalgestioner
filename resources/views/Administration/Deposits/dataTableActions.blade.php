{!! Form::open(['id' => $id, 'route' => ['deposits.destroy', $id], 'method' => 'delete']) !!}
@can('deposits.edit')
    <a href="{{route('deposits.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
@endcan
@can('deposits.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
            'class' => 'btn btn-sm',
            'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el Deposito?'); return false;"
    ]) !!}
@endcan
{!! Form::close() !!}

