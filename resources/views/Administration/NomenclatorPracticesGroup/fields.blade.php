<div class="form-group">
    {{ Form::label('name', 'Nombre del Grupo', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('nomenclator_practices_ids', 'Seleccionar Practica / Practicas', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('nomenclator_practices_ids[]', \CloudMed\Models\Administration\Nomenclators\NomenclatorPractice::all()->pluck('code', 'id'), isset($nomenclatorPracticeGroup) ? $nomenclatorPracticeGroup->nomenclatorPractices : null, ['class' => 'form-control chosen-select', 'multiple']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('nomenclatorPracticesGroup.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
