{!! Form::open(['id' => $id, 'route' => ['nomenclatorPracticesGroup.destroy', $id], 'method' => 'delete']) !!}
@can('nomenclatorPracticesGroup.edit')
    <a href="{{route('nomenclatorPracticesGroup.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
@endcan
@can('nomenclatorPracticesGroup.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
            'class' => 'btn btn-sm',
            'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el grupo de practicas ?'); return false;"
    ]) !!}
@endcan
{!! Form::close() !!}
