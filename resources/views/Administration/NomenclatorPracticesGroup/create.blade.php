@extends('layouts.app')
@section('pageName')
    Grupo de Practicas de Nomenclador
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                </header>
                <h3 class="panel-title"></h3>
                <div class="panel-body">
                    @include('layouts.errors')
                    {!! Form::open(['route' => 'nomenclatorPracticesGroup.store']) !!}
                        @include('Administration.NomenclatorPracticesGroup.fields')
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection
