@extends('layouts.app')
@section('pageName')
    Grupos de Practicas de Nomenclador
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/nomenclatorPracticesGroup/create']),
        'tableId' => 'nomenclatorPracticesGroupTable',
        'tableColumns' => ['Nombre', 'Acciones'],
        'tableSubtitle' => '<a href="/administration/nomenclatorPracticesGroup/create" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i>&nbsp;Nuevo Grupo de practicas </a>'
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/nomenclatorPracticesGroup/index.js') }}
@endsection
