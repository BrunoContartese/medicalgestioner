@extends('layouts.app')
@section('pageName')
    Rango Nomencladores
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/nomenclatorRanges/create']),
        'tableId' => 'nomenclatorRangesTable',
        'tableColumns' => ['Nomenclador', 'Desde código', 'Hasta código', 'Acciones'],
        'tableSubtitle' => '<a href="/administration/nomenclatorRanges/create" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i>&nbsp;Nuevo Registro</a>'
    ])

@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/NomenclatorRanges/index.js') }}
@endsection
