{!! Form::open(['id' => $row->id, 'route' => ['nomenclatorRanges.destroy', $row->id], 'method' => 'delete']) !!}
    @can('nomenclatorRanges.edit')
        <a href="{{route('nomenclatorRanges.edit', $row->id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
    @endcan
    @can('nomenclatorRanges.delete')
        {!! Form::button('<i class="fa fa-trash"></i>', [
                'class' => 'btn btn-sm',
                'onclick' => "return confirmSubmit(".$row->id.",'¿Está seguro de que desea eliminar el rango?'); return false;"
        ]) !!}
    @endcan
{!! Form::close() !!}
