<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            {{ Form::label('from_code', 'Desde código', ['class' => 'bmd-label-floating']) }}
            {{ Form::text('from_code', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            {{ Form::label('to_code', 'Hasta código', ['class' => 'bmd-label-floating']) }}
            {{ Form::text('to_code', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            {{ Form::label('nomenclator_id', 'Nomenclador') }}
            {{ Form::select('nomenclator_id', \CloudMed\Models\Administration\Nomenclators\Nomenclator::all()->pluck('name','id'), isset($nomenclatorRange) ? $nomenclatorRange->id : null, ['class' => 'form-control chosen-select']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('nomenclatorRanges.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
