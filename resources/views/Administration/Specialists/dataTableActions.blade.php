{!! Form::open(['id' => $id, 'route' => ['specialists.destroy', $id], 'method' => 'delete']) !!}
        @can('specialists.edit')
                <a href="{{route('specialists.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
        @endcan
        @can('specialists.delete')
                {!! Form::button('<i class="fa fa-trash"></i>', [
                        'class' => 'btn btn-sm',
                        'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el Especialista?'); return false;"
                ]) !!}
        @endcan
{!! Form::close() !!}
