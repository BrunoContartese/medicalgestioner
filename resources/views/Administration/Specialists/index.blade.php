@extends('layouts.app')
@section('pageName')
    Especialistas
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/specialists/create']),
        'tableId' => 'specialistsTable',
        'tableColumns' => ['CUIT', 'N° Matricula', 'Apellido y Nombre', 'Especialidades', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/specialists/index.js') }}
@endsection
