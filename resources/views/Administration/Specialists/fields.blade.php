<div class="form-group">
    {{ Form::label('cuit', 'CUIT', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('cuit', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('email', 'E-Mail', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::email('email', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('licence', 'Matricula', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('licence', null, ['class' => 'form-control']) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('phone_number', 'Telefono', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        <div class="input-group mb-md">
            <span class="input-group-addon">0</span>
            <input name="area_code" type="tel" class="form-control" placeholder="Cód. Área" @if(isset($specialist)) value="{{$specialist->area_code}}" @endif />
            <span class="input-group-addon">15</span>
            <input name="phone_number" type="tel" class="form-control" placeholder="N° de teléfono" @if(isset($specialist)) value="{{$specialist->phone_number}}" @endif/>
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::label('name', 'Apellido y Nombre', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('malpractice_insurance', 'Seguro de mala praxis', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medical_malpractice_insurance_id', \CloudMed\Models\Administration\MedicalMalpracticeInsurance::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una opción']) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('specialties_ids', 'Seleccionar Especialidad / Especialidades', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('specialties_ids[]', \CloudMed\Models\Administration\Specialty::all()->pluck('name', 'id'), isset($specialist) ? $specialist->specialties : null, ['class' => 'form-control chosen-select', 'multiple', 'data-placeholder' => 'Seleccione las especialidades']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
        <a href="{!! route('specialists.index') !!}" class="btn btn-danger">Cancelar</a>
    </div>
</div>
