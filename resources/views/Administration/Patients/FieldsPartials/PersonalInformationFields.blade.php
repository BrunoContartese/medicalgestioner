@if(isset($patient))
    <input type="hidden" value="{{$patient->id}}" name="id" />
@endif
<div class="form-group">
    {{ Form::label('name', 'Apellido y Nombre', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', isset($patient) ? $patient->name : null, ['class' => 'form-control', 'placeholder' => 'Apellido y Nombre']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('birthday', 'Fecha de Nacimiento', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::date('birthday', isset($patient) ? $patient->birthday : null, ['class' => 'form-control', 'placeholder' => 'Fecha de Nacimiento']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('document_type_id', 'Documento', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        <div class="input-group">
            <span class="input-group-addon">Tipo</span>
            {{ Form::select('document_type_id', DB::table('document_types')->get()->pluck('name','id'), isset($patient) ? $patient->document_type_id : null, ['class' => 'form-control']) }}
            <span class="input-group-addon">Número</span>
            {{ Form::text('document_number', isset($patient) ? $patient->document_number : null, ['class' => 'form-control', 'placeholder' => 'Nº de documento']) }}
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::label('gender_id', 'Género', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('gender_id', DB::table('genders')->get()->pluck('name','id'), null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('province_id', 'Provincia', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('province_id', \CloudMed\Models\Administration\Province::all()->pluck('name','id'), isset($patient) ? $patient->city->province->id : null, ['class' => 'form-control chosen-select', 'onchange' => 'getCities();', 'placeholder' => 'Seleccione una provincia']) }}
    </div>
</div>


<div id="citiesDiv" class="form-group">
    {{ Form::label('city_id', 'Ciudad', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        @if(isset($patient))
            {{ Form::select('city_id',\CloudMed\Models\Administration\City::where('province_id', $patient->city->province->id)->get()->pluck('city_name','id'), $patient->city->id, ['class' => 'form-control chosen-select']) }}
        @else
            {{ Form::select('city_id', [], null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione primero una Provincia']) }}
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('phone_number', 'Teléfono Fijo', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('phone_number', isset($patient) ? $patient->phone_number : null, ['class' => 'form-control', 'placeholder' => 'Teléfono Fijo']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('address', 'Dirección', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('address', isset($patient) ? $patient->address : null, ['class' => 'form-control', 'placeholder' => 'Dirección']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('phone_number', 'Telefono celular', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        <div class="input-group mb-md">
            <span class="input-group-addon">0</span>
            <input name="mobile_phone_area_code" type="tel" class="form-control" placeholder="Cód. Área" @if(isset($patient)) value="{{$patient->mobile_phone_area_code}}" @endif />
            <span class="input-group-addon">15</span>
            <input name="mobile_phone_number" type="tel" class="form-control" placeholder="N° de teléfono" @if(isset($patient)) value="{{$patient->mobile_phone_number}}" @endif/>
        </div>
    </div>
</div>


