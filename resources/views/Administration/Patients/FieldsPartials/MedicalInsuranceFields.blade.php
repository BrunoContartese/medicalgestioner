<div class="row">
    <div class="col-md-12">
        <div class="form-group col-md-3">
            {{ Form::label('medical_insurance_id', 'Obra Social') }}
            {{ Form::select('medical_insurance_id', \CloudMed\Models\Administration\MedicalInsurance::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una obra social']) }}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('afilliate_number', 'Nº de Afiliado') }}
            {{ Form::text('afilliate_number', null, ['class' => 'form-control', 'placeholder' => 'Nº de Afiliado']) }}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('expiration_date', 'Fecha de vto') }}
            {{ Form::date('expiration_date', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-md-3">
            {{ Form::label('expiration_date', 'Agregar') }} <br>
            <button type="button" onclick="addMedicalInsuranceToTable();" class="btn btn-warning"><i class="fa fa-plus"></i></button>
        </div>
    </div>
 </div>

<br><br>

<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="patientMedicalInsurancesTable" class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Obra Social</th>
                        <th>Nº de Afiliado</th>
                        <th>Fecha de vencimiento</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>