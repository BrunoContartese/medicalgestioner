<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Datos Personales</h2>
            </header>
            <div class="panel-body">
                @include('Administration.Patients.FieldsPartials.PersonalInformationFields')
            </div>
        </section>
    </div>
</div>