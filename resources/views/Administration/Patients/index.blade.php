@extends('layouts.app')
@section('pageName')
    Pacientes
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/patients/create']),
        'tableId' => 'patientsTable',
        'tableColumns' => ['Nombre', 'Tipo de Documento', 'Número de Documento', 'Ver más datos', 'Acciones']
        ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/Patients/index.js') }}
@endsection
