@extends('layouts.app')
@section('pageName')
    Crear Paciente
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('layouts.errors')
            @include('Administration.Patients.FieldsPartials.PersonalInformation')
            @include('Administration.Patients.FieldsPartials.MedicalInsuranceInformation')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <button type="button" onclick="storePatient();" class="btn btn-warning">Guardar</button>
                <a href="{!! route('patients.index') !!}" class="btn btn-danger">Cancelar</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/Patients/create.js') }}
@endsection
