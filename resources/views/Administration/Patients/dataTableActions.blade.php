{!! Form::open(['id' => $id, 'route' => ['patients.destroy', $id], 'method' => 'delete']) !!}
@can('patients.edit')
    <a href="{{route('patients.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
@endcan
{!! Form::close() !!}

