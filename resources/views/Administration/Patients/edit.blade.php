@extends('layouts.app')
@section('pageName')
    Editar Paciente
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('layouts.errors')
            @include('Administration.Patients.FieldsPartials.PersonalInformation')
            @include('Administration.Patients.FieldsPartials.MedicalInsuranceInformation')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <button type="button" onclick="updatePatient();" class="btn btn-warning">Actualizar</button>
                <a href="{!! route('patients.index') !!}" class="btn btn-danger">Cancelar</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/Patients/edit.js') }}
    <script>
        function addMedicalInsurancesToTable() {

                dataTable.api().rows.add([
                    @foreach($patient->medicalInsurances as $medicalInsurance)
                    {
                        medical_insurance_id: "{{$medicalInsurance->id}}",
                        medical_insurance_name: "{{ $medicalInsurance->name }}",
                        afilliate_number: "{{ $medicalInsurance->pivot->afilliate_number }}",
                        expiration_date: "{{ \Carbon\Carbon::parse($medicalInsurance->pivot->expiration_date)->format('d/m/Y') }}",
                        actions: buttonHtml()
                    },
                    @endforeach
                ]).draw(true);

        }

        $(document).ready(function() {
            @foreach($patient->medicalInsurances as $medicalInsurance)
            disableOptionAndCleanFields("{{ $medicalInsurance->name }}");
            @endforeach
        });
    </script>
@endsection
