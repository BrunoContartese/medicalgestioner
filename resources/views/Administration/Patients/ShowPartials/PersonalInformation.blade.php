<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Datos personales</h3>
            </header>
            <div class="panel-body">
                @include('Administration.Patients.ShowPartials.PersonalInformationShowFields')
            </div>
        </section>
    </div>
</div>