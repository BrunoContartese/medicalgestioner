<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Datos de Obra Social</h3>
            </header>
            <div class="panel-body">
                @include('Administration.Patients.ShowPartials.MedicalInsuranceShowFields')
            </div>
        </section>
    </div>
</div>