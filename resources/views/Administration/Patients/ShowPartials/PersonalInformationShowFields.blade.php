<div class="form-group col-md-6">
    {{ Form::label('name', 'Apellido y Nombre') }}
    {{ Form::text('name', $patient->name, ['class' => 'form-control', 'placeholder' => 'Apellido y Nombre', 'disabled']) }}
</div>

<div class="form-group col-md-6">
    {{ Form::label('birthday', 'Fecha de Nacimiento') }}
    {{ Form::date('birthday', $patient->birthday, ['class' => 'form-control', 'placeholder' => 'Fecha de Nacimiento', 'disabled']) }}
</div>

<div class="form-group col-md-6">
    {{ Form::label('document_type_id', 'Documento') }}
    <div class="input-group">
        <span class="input-group-addon">Tipo</span>
        {{ Form::select('document_type_id', DB::table('document_types')->get()->pluck('name','id'), $patient->document_type, ['class' => 'form-control', 'disabled']) }}
        <span class="input-group-addon">Número</span>
        {{ Form::text('document_number', $patient->document_number, ['class' => 'form-control', 'placeholder' => 'Nº de documento', 'disabled']) }}
    </div>
</div>

<div class="form-group col-md-6">
    {{ Form::label('gender_id', 'Género') }}
    {{ Form::select('gender_id', DB::table('genders')->get()->pluck('name','id'), $patient->gender_id, ['class' => 'form-control', 'disabled']) }}
</div>

<div class="form-group col-md-6">
    {{ Form::label('province_id', 'Provincia') }}
    {{ Form::select('province_id', \CloudMed\Models\Administration\Province::all()->pluck('name','id'), $patient->city->province->id, ['class' => 'form-control chosen-select', 'disabled']) }}
</div>


<div id="citiesDiv" class="form-group col-md-6">
    {{ Form::label('city_id', 'Ciudad') }}
    {{ Form::select('city_id',\CloudMed\Models\Administration\City::where('province_id', $patient->city->province->id)->get()->pluck('city_name','id'), $patient->city->id, ['class' => 'form-control chosen-select', 'disabled']) }}
</div>

<div class="form-group col-md-6">
    {{ Form::label('phone_number', 'Teléfono Fijo') }}
    {{ Form::text('phone_number', $patient->phone_number, ['class' => 'form-control', 'placeholder' => 'Teléfono Fijo', 'disabled']) }}
</div>

<div class="form-group col-md-6">
    {{ Form::label('address', 'Dirección') }}
    {{ Form::text('address', $patient->address, ['class' => 'form-control', 'placeholder' => 'Dirección', 'disabled']) }}
</div>

<div class="form-group col-md-6">
    {{ Form::label('phone_number', 'Telefono celular') }} <br>
    <div class="input-group mb-md">
        <span class="input-group-addon">0</span>
        <input disabled name="mobile_phone_area_code" type="tel" class="form-control" placeholder="Cód. Área" value="{{$patient->mobile_phone_area_code}}" />
        <span class="input-group-addon">15</span>
        <input disabled name="mobile_phone_number" type="tel" class="form-control" placeholder="N° de teléfono" value="{{$patient->mobile_phone_number}}"/>
    </div>
</div>