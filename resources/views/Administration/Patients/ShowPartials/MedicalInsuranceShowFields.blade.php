<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Obra Social</th>
                        <th>Nº de Afiliado</th>
                        <th>Fecha de vencimiento</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($patient->medicalInsurances as $medicalInsurance)
                        <tr>
                            <td>{{ $medicalInsurance->name }}</td>
                            <td>{{ $medicalInsurance->pivot->afilliate_number }}</td>
                            <td>{{ \Carbon\Carbon::parse($medicalInsurance->pivot->expiration_date)->format('d/m/Y') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>