@extends('layouts.app')
@section('pageName')
    Gerenciadoras
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/medicalInsurancesManagers/create']),
        'tableId' => 'medicalInsurancesManagersTable',
        'tableColumns' => ['Obra Social', 'Nombre', 'Dirección de facturación', 'Última modificación', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/medicalInsurancesManagers/index.js') }}
@endsection