@can('medicalInsurancesManagers.edit')
        <a title="Modificar" href="{{route('medicalInsurancesManagers.edit', $id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
@endcan
@if(is_null($row->deleted_at))
        @can('medicalInsurances.delete')
            {!! Form::open(['id' => $id, 'route' => ['medicalInsurancesManagers.destroy', $id], 'method' => 'delete', 'style' => 'display: inline;']) !!}
                {!! Form::button('<i class="fa fa-check"></i>', [
                        'class' => 'btn btn-sm btn-success',
                        'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea suspender la gerenciadora?'); return false;",
                        'title' => 'Suspender gerenciadora'
                ]) !!}
            {!! Form::close() !!}
        @endcan
@else
        {!! Form::open(['id' => $id, 'route' => ['medicalInsurancesManagers.restore', $id], 'method' => 'post', 'style' => 'display: inline;']) !!}
            {!! Form::button('<i class="fa fa-times"></i>', [
                        'class' => 'btn btn-sm btn-danger',
                        'onclick' => "confirmSubmit($id, '¿Está seguro de que desea habilitar la gerenciadora?'); return false;",
                        'title' => 'Habilitar gerenciadora'
            ]) !!}
        {!! Form::close() !!}
@endif