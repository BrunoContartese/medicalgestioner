<div class="form-group">
    {{ Form::label('medical_insurance_id', 'Obra social', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medical_insurance_id', \CloudMed\Models\Administration\MedicalInsurance::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una obra social.']) }}

    </div>
</div>

<div class="form-group">
    {{ Form::label('name', 'Nombre de la gerenciadora', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('cuit', 'Cuit', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('cuit', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('billing_address', 'Dirección de facturación', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('billing_address', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group col-lg-12">
    <div class="checkbox-custom checkbox-primary">
        @if(isset($medicalInsuranceManager))
            @if($medicalInsuranceManager->perceives_gross_income == 1)
                <input type="checkbox" name="perceives_gross_income" value="1" checked/>
            @else
                <input type="checkbox" name="perceives_gross_income" value="1"/>
            @endif
        @else
            <input type="checkbox" name="perceives_gross_income" value="1"/>
        @endif
        <label>¿Percibe IIBB?</label>
    </div>
</div>

<div class="form-group col-lg-12">
    {{ Form::label('contact', 'Contacto / Contactos', ['class' => 'col-md-3 control-label']) }}

    {{ Form::textarea('contact', null, ['class' => 'form-control editor']) }}
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::button('Guardar', ['class' => 'btn btn-warning', 'onclick' => 'storeMedicalInsuranceManager();']) !!}
        <a href="{!! route('medicalInsurancesManagers.index') !!}" class="btn btn-danger">Cancelar</a>
    </div>
</div>
