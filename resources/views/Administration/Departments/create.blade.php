@extends('layouts.app')
@section('pageName')
    Crear Departamento
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                </header>
                <h3 class="panel-title"></h3>
                <div class="panel-body">
                    @include('layouts.errors')
                    {!! Form::open(['route' => 'departments.store']) !!}
                    @include('Administration.Departments.fields')
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection