{!! Form::open(['id' => $id, 'route' => ['departments.destroy', $id], 'method' => 'delete']) !!}
@can('departments.edit')
    <a href="{{route('departments.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
@endcan
@can('departments.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
            'class' => 'btn btn-sm',
            'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el Departamento?'); return false;"
    ]) !!}
@endcan
{!! Form::close() !!}
