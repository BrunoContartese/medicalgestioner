@extends('layouts.app')
@section('pageName')
    Departamentos

@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/departments/create']),
        'tableId' => 'departmentsTable',
        'tableColumns' => ['Departamentos', 'Acciones'],
        'tableSubtitle' => '<a href="/administration/departments/create" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i>&nbsp;Nuevo Departamento</a>'
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/departments/index.js') }}
@endsection
