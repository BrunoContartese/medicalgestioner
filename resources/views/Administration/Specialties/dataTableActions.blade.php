{!! Form::open(['id' => $id, 'route' => ['specialties.destroy', $id], 'method' => 'delete']) !!}
@can('specialties.edit')
    <a href="{{route('specialties.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
@endcan
@can('specialties.delete')
    {!! Form::button('<i class="fa fa-trash"></i>', [
            'class' => 'btn btn-sm',
            'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar la especialidad?'); return false;"
    ]) !!}
@endcan
{!! Form::close() !!}
