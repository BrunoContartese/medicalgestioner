@extends('layouts.app')
@section('pageName')
    Especialidades

@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/specialties/create']),
        'tableId' => 'specialtiesTable',
        'tableColumns' => ['Especialidad', 'Acciones'],
        'tableSubtitle' => '<a href="/administration/specialties/create" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i>&nbsp;Nueva Especialidad</a>'
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/specialties/index.js') }}
@endsection
