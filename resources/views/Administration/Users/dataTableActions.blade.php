{!! Form::open(['id' => $id, 'route' => ['users.destroy', $id], 'method' => 'delete']) !!}
        @can('users.edit')
                <a href="{{route('users.edit', $id)}}" class="btn btn-sm btn-default"><i class="fa fa-pencil-alt"></i></a>
        @endcan
        @can('users.delete')
                {!! Form::button('<i class="fa fa-trash"></i>', [
                        'class' => 'btn btn-sm',
                        'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el usuario?'); return false;"
                ]) !!}
        @endcan
{!! Form::close() !!}
