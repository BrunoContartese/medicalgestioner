@extends('layouts.app')
@section('pageName')
    Usuarios
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/users/create']),
        'tableId' => 'usersTable',
        'tableColumns' => ['Apellido y Nombre', 'Nombre de usuario', 'E-Mail', 'Última vez logueado', 'Última IP', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/users/index.js') }}
@endsection