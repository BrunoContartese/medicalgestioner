<div class="form-group">
    {{ Form::label('name', 'Apellido y Nombre', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('email', 'E-Mail', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::email('email', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('username', 'Nombre de Usuario', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('username', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('password', 'Contraseña', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        <input type="password" name="password" class="form-control" placeholder="Contraseña" />
    </div>
</div>

<div class="form-group">
    {{ Form::label('repeat_password', 'Confirmar Contraseña', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        <input type="password" name="repeat_password" class="form-control" placeholder="Confirmar Contraseña" />
    </div>
</div>

<div class="form-group">
    {{ Form::label('role', 'Seleccionar Perfil', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('role', \Spatie\Permission\Models\Role::all()->pluck('name', 'name'), isset($user->roles) ? $user->roles->pluck('name','name') : null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione un perfil', 'multiple']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
        <a href="{!! route('users.index') !!}" class="btn btn-danger">Cancelar</a>
    </div>
</div>
