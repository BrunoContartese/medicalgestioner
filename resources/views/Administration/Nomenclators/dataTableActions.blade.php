<div class="form-inline">
    @can('medicalInsurances.edit')
        <a title="Modificar nomenclador" href="{{route('nomenclators.edit', $row->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
    @endcan

    @if(is_null($row->deleted_at))
        @can('nomenclators.delete')
            {!! Form::open(['id' => $row->id, 'route' => ['nomenclators.destroy', $row->id], 'method' => 'delete', 'style' => 'display: inline;']) !!}
            {!! Form::button('<i class="fa fa-check"></i>', [
                    'class' => 'btn btn-sm btn-success',
                    'onclick' => "return confirmSubmit(".$row->id.",'¿Está seguro que desea suspender el nomenclador?'); return false;",
                    'title' => 'Suspender Nomenclador'
            ]) !!}
            {!! Form::close() !!}
        @endcan
    @else
        {!! Form::open(['id' => $row->id, 'route' => ['nomenclators.restore', $row->id], 'method' => 'post', 'style' => 'display: inline;']) !!}
        {!! Form::button('<i class="fa fa-times"></i>', [
                    'class' => 'btn btn-sm btn-danger',
                    'onclick' => "confirmSubmit($row->id, '¿Está seguro de que desea habilitar el nomenclador?'); return false;",
                    'title' => 'Habilitar Nomenclador'
            ]) !!}
        {!! Form::close() !!}
    @endif
</div>