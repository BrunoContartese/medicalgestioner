@extends('layouts.app')
@section('pageName')
    Nomencladores
@endsection

@section('content')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/nomenclators/create']),
        'tableId' => 'nomenclatorsTable',
        'tableColumns' => ['Nombre', 'Estado', 'Prácticas', 'Acciones'],
    ])
@endsection
@section('scripts')
    {{ Html::script('/assets/js/administration/Nomenclators/index.js') }}
@endsection


