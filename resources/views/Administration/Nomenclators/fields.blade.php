<div class="form-group">
    {{ Form::label('name', 'Nombre:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

@if(!isset($nomenclator))
    <div class="form-group">
        {{ Form::label('nomenclatorMirror', 'Copiar prácticas de:', ['class' => 'col-md-3 control-label']) }}
        <div class="col-md-6">
            {{ Form::select('nomenclatorMirror', \CloudMed\Models\Administration\Nomenclators\Nomenclator::all()->pluck('name', 'id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'No copiar prácticas']) }}
        </div>
    </div>
@endif

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('nomenclators.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
