@can('nomenclators.edit')
    <a title="Modificar Práctica" class="btn btn-default btn-sm" href="{{url('/administration/nomenclatorPractice/edit',$row->id)}}"><i class="fa fa-pencil-alt"></i></a>
@else
    <button disabled type="button" title="No posee el nivel de permisos para modificar la práctica" class="btn btn-default btn-sm"><i class="fa fa-pencil-alt"></i></button>
@endcan