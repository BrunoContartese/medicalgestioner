<button id="changeHelpersQuantityButton-{{$row->id}}" onclick="confirmChangeHelpersQuantity({{$row->id}})" type="button" class="btn btn-info btn-sm">
    @if(is_null($row->has_two_helpers) || $row->has_two_helpers == 0)
        <i class="fa fa-times"></i>
    @else
        <i class="fa fa-check"></i>
    @endif
</button>