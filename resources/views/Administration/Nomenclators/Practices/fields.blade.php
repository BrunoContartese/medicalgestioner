<div class="form-group">
    {{ Form::label('code', 'Código:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::number('code', null, ['class' => 'form-control']) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('is_module', '¿Es Módulo?:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('is_module', ['1' => "SI", '0' => "NO"], isset($nomenclatorPractice) ? $nomenclatorPractice->is_module : null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una opción']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('description', 'Descripción:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('description', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('honorary_units', 'Unidades de Honorarios:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('honorary_units', isset($nomenclatorPractice) ? $nomenclatorPractice->honorary_units : 0, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('helper_honorary_units', 'Unidades de Honorario de Ayudante:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('helper_honorary_units', isset($nomenclatorPractice) ? $nomenclatorPractice->helper_honorary_units : 0, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('expense_units', 'Unidades de gasto:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('expense_units', isset($nomenclatorPractice) ? $nomenclatorPractice->expense_units : 0, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('anesthesist_honorary_units', 'Unidades de Honorario de Anestesista:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('anesthesist_honorary_units', isset($nomenclatorPractice) ? $nomenclatorPractice->anesthesist_honorary_units : 0, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('helpers_quantity', 'Cantidad de ayudantes:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::number('helpers_quantity', isset($nomenclatorPractice) ? $nomenclatorPractice->helpers_quantity : 0, ['class' => 'form-control', 'placeholder' => 'Cantidad de ayudantes']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Guardar', ['class' => 'btn btn-warning']) !!}
            <a href="{!! route('nomenclators.index') !!}" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</div>
