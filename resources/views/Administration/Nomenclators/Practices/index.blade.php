@extends('layouts.app')
@section('pageName')
    Prácticas del {{$nomenclator->name}}
@endsection

@section('content')
    {!! Form::hidden('nomenclatorId', $nomenclator->id) !!}
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/administration/nomenclatorPractices/create/'.$nomenclator->id]),
        'tableId' => 'nomenclatorPracticesListTable',
        'tableColumns' => ['Código', '¿Es Módulo?', 'Descripción', 'Unidades de Honorario', 'Unidades de Honorario de Ayudante', 'Unidades de Gasto', 'Cantidad de Ayudantes', 'Acciones'],
    ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/administration/Nomenclators/Practices/practicesList.js') }}
@endsection