@extends('layouts.app')
@section('pageName')
    Check in de paciente ambulatorio
@endsection

@section('content')
    @include('PatientCheckingAccounts.Ambulatory.partials.patientSelection')
@endsection

@section('scripts')
    {{ Html::script('/assets/js/PatientCheckingAccounts/Ambulatory/scripts.js') }}
@endsection
