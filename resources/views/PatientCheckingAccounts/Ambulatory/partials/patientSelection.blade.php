<div class="row">
    <div class="col-xs-12">
        <section class="panel form-wizard" id="patientCheckInWizard">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title"></h2>
            </header>
            <div class="panel-body">
                <div class="wizard-progress wizard-progress-lg">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul class="wizard-steps">
                        <li class="active">
                            <a href="#step1" data-toggle="tab"><span>1</span>Datos<br> del paciente</a>
                        </li>
                        <li>
                            <a href="#step2" data-toggle="tab"><span>2</span>Datos<br> de facturación</a>
                        </li>
                        <li>
                            <a href="#step3" data-toggle="tab"><span>3</span>Confirmar <br> Datos</a>
                        </li>
                    </ul>
                </div>

                <form class="form-horizontal" novalidate="novalidate">
                    <div class="tab-content">
                        @include('PatientCheckingAccounts.Ambulatory.partials.wizardSteps')
                    </div>
                </form>


            </div>
            <div class="panel-footer">
                <ul class="pager">
                    <li class="previous disabled">
                        <a><i class="fa fa-angle-left"></i> Anterior</a>
                    </li>
                    <li class="finish hidden pull-right">
                        <a>Confirmar</a>
                    </li>
                    <li class="next">
                        <a>Siguiente <i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>

                @include('layouts.errors')
            </div>
        </section>
    </div>
</div>