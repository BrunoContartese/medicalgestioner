<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Revisión de datos de facturación</h2>
            </header>
            <div class="panel-body">
                @include('PatientCheckingAccounts.Ambulatory.partials.WizardHelpers.partials.SelectedPatientBillingInformationFields')
            </div>
        </section>
    </div>
</div>