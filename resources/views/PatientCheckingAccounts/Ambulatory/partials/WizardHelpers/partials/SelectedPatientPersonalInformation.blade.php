<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Revisión de datos personales</h3>
            </header>
            <div class="panel-body">
                @include('PatientCheckingAccounts.Ambulatory.partials.WizardHelpers.partials.SelectedPatientPersonalInformationFields')
            </div>
        </section>
    </div>
</div>