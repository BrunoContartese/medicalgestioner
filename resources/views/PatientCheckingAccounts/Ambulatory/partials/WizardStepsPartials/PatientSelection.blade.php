<div id="step1" class="tab-pane active">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="document_type_id">Tipo de documento</label>
        <div class="col-sm-4">
            {{ Form::select('document_type_id', \CloudMed\Models\Administration\DocumentType::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select']) }}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="document_number">N° de Documento</label>
        <div class="col-sm-4">
            {{ Form::text('document_number', null, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>