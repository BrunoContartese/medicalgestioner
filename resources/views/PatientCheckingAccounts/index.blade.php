@extends('layouts.app')
@section('pageName')
    Rendiciones
@endsection

@section('content')
    @include('PatientCheckingAccounts.partials.generalInformation')
    @include('PatientCheckingAccounts.partials.table')
@endsection

@section('scripts')
    {{ Html::script('/assets/js/PatientCheckingAccounts/index.js') }}
@endsection
