<div class="row">
    @include('layouts.partials.table', [
        'tableTitle' => '',
        'tableId' => 'checkingAccountsTable',
        'tableColumns' => ['Fecha y Hora de Ingreso', 'Obra Social', 'Tipo de rendición', 'Tipo de Documento', 'Número de documento', 'Paciente', 'Estado'],
    ])
</div>
