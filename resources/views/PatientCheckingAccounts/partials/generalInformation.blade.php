<div class="row">
        <div class="col-lg-12">
            <section class="panel panel-warning">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                    <h3 class="panel-title">Información de interés</h3>
                </header>

                <div class="panel-body">
                    @include('PatientCheckingAccounts.partials.GeneralInformationPartials.openedCheckingAccounts')
                    @include('PatientCheckingAccounts.partials.GeneralInformationPartials.openedAmbulatoryCheckingAccounts')
                    @include('PatientCheckingAccounts.partials.GeneralInformationPartials.openedInternationCheckingAccounts')

                </div>
            </section>
        </div>

</div>