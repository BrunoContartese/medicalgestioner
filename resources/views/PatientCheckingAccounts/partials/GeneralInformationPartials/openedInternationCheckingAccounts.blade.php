<div class="col-md-12 col-lg-6 col-xl-6">
    <section class="panel panel-featured-left panel-featured-secondary">
        <div class="panel-body">
            <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                    <div class="summary-icon bg-secondary">
                        <i class="fa fa-ambulance"></i>
                    </div>
                </div>
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Rendiciones de Internación Abiertas</h4>
                        <div class="info">
                            <strong class="amount">{{ $openedInternationCheckingAccounts }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>