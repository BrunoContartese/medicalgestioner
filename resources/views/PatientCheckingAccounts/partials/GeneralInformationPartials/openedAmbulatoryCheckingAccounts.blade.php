<div class="col-md-12 col-lg-6 col-xl-6">
    <section class="panel panel-featured-left panel-featured-tertiary">
        <div class="panel-body">
            <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                    <div class="summary-icon bg-tertiary">
                        <i class="fa fa-car"></i>
                    </div>
                </div>
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Rendiciones Ambulatorias Abiertas</h4>
                        <div class="info">
                            <strong class="amount">{{ $openedAmbulatoryCheckingAccounts }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>