<section class="panel panel-warning">
    <header class="panel-heading">
        <div class="panel-actions">
        </div>
        <h2 class="panel-title">Por favor seleccione una rendición</h2>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-condensed mb-none" id="openedCheckingAccountsTable">
                <thead>
                    <tr>
                        <th>CUIR</th>
                        <th>Fecha</th>
                        <th>Tipo de rendición</th>
                        <th>Número de rendición</th>
                        <th>Obra Social</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($checkingAccounts as $checkingAccount)
                        <tr>
                            <td>{{ $checkingAccount->id }}</td>
                            <td>{{ \Carbon\Carbon::parse($checkingAccount->created_at)->format('d/m/Y') }}</td>
                            <td>{{ $checkingAccount->checkingAccountType->name }}</td>
                            <td>{{ $checkingAccount->checking_account_number }}</td>
                            @foreach($checkingAccount->checkingAccountBillingInformation as $medicalInsurance)
                                <td>{{ $medicalInsurance->name }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</section>
