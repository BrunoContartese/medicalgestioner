<div class="form-group">
    {{ Form::label('name', 'Ciudad', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('city_id', DB::table('cities')->where('province_id', $province_id)->get()->pluck('city_name','id'), null, ['class' => 'form-control chosen-select']) }}
    </div>
</div>