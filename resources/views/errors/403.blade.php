@extends('errors::minimal')

@section('title', __('Requiere autorización'))
@section('code', '403')
@section('message')
    No posee permisos para realizar esta acción.
    <br>
    Por favor contáctese con el depto. de sistemas.
    <br>
    Sera redirigido automáticamente a la página principal.
@endsection
