@extends('errors::minimal')

@section('title','Página no encontrada')
@section('code', '404')
@section('message')
    Página no encontrada.
    <br>
    Será redirigido a la página principal automáticamente.
@endsection
