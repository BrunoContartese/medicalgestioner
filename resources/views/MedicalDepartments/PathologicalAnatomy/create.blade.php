@extends('layouts.app')
@section('pageName')
    Crear estudio de anatomía patológica
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <section class="panel form-wizard panel-primary" id="createPathologicalAnatomyStudyWizard">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="wizard-progress wizard-progress-lg">
                        <div class="steps-progress">
                            <div class="progress-indicator"></div>
                        </div>
                        <ul class="wizard-steps">
                            <li class="active">
                                <a href="#step1" data-toggle="tab"><span><i class="fa fa-user"></i></span>Datos<br> del paciente</a>
                            </li>
                            <li>
                                <a href="#step2" data-toggle="tab"><span><i class="fa fa-ticket-alt"></i></span>Seleccionar<br> rendición</a>
                            </li>
                            <li>
                                <a href="#step3" data-toggle="tab"><span><i class="fa fa-check"></i></span>Datos <br> del estudio</a>
                            </li>
                        </ul>
                    </div>

                    <form class="form-horizontal" novalidate="novalidate">
                        <div class="tab-content">
                            @include('MedicalDepartments.PathologicalAnatomy.CreatePartials.wizardSteps')
                        </div>
                    </form>


                </div>
                <div class="panel-footer">
                    <ul class="pager">
                        <li class="previous disabled">
                            <a><i class="fa fa-angle-left"></i> Anterior</a>
                        </li>
                        <li class="finish hidden pull-right">
                            <a>Confirmar</a>
                        </li>
                        <li class="next">
                            <a>Siguiente <i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>

                    @include('layouts.errors')
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/MedicalDepartments/PathologicalAnatomy/create.js') }}
@endsection
