<div class="row">
    <div class="col-md-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Prácticas asignadas</h2>
            </header>
            <div class="panel-body">
                <table class="table table-triped table-bordered table-condensed" id="pathologicalAnatomyBillingCodesTable">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($sample->billingCodes))
                            @foreach($sample->billingCodes as $practice)
                                <tr>
                                    <td>{{ $practice->id }}</td>
                                    <td>{{ $practice->code }}</td>
                                    <td>{{ $practice->description }}</td>
                                    <td><button class='btn btn-danger' id='delete'><i class='fa fa-trash'></i></button></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
