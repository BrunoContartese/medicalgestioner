<div class="row">
    <div class="col-xs-12">
        <section class="panel panel-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Selección de práctica</h2>
            </header>
            <div class="panel-body">

                <div class="form-group col-md-8">
                    {{ Form::label('nomenclator_practice_id', 'Código de práctica: ') }}
                    <div class="form-inline">
                        {{ Form::select('nomenclator_practice_id', \CloudMed\Models\Administration\Nomenclators\NomenclatorPractice::all()->pluck('full_name', 'id'), null, ['class' => 'form-control chosen-select']) }}
                        <button class="btn btn-md btn-warning" onclick="addPracticeToTable();"><i class="fa fa-plus"></i></button>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>
