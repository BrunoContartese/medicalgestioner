@extends('layouts.app')
@section('pageName')
    Anatomía Patológica - Asignación de códigos de facturación - Muestra: {{ $sample->code }}
@endsection

@section('content')
    @include('layouts.errors')
    {{ Form::hidden('sample_id', $sample->id) }}
    @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.BillingCodesAssignmentPartials.PracticeSelection')

    @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.BillingCodesAssignmentPartials.PracticesTable')

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-sm btn-success" onclick="syncBillingCodes();">Guardar</button>
            <button type="button" class="btn btn-sm btn-danger" onclick="closeTab();">Volver al listado</button>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/MedicalDepartments/PathologicalAnatomy/MedicalReport/billingCodesAssigment.js') }}
@endsection
