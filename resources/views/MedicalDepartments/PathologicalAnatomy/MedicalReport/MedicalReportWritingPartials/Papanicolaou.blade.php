<div class="row">
    <div class="col-xs-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Plantilla de Papanicolaou</h2>
            </header>
            <div class="panel-body">
                <div class="form-group col-md-6">
                    <button class="btn btn-success form-control" onclick="getPapanicolaouTemplateModal();"><i class="fa fa-file-o"></i>Abrir plantilla de papanicolaous</button>
                </div>
            </div>
        </section>
    </div>
</div>