<div class="row">
    <div class="col-xs-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Firmar</h2>
            </header>
            <div class="panel-body">
                <div class="form-group col-md-6">
                    <button class="btn btn-success form-control" onclick="signMedicalReport();"><i class="fa fa-signature"></i>Colocar firma personal</button>
                </div>
            </div>
        </section>
    </div>
</div>