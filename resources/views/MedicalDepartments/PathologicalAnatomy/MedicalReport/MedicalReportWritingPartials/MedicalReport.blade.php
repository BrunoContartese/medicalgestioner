<div class="row">
    <div class="col-xs-12">
        <section class="panel panel-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Informe</h2>
            </header>
            <div class="panel-body">
                <div class="form-group col-md-12">
                    <textarea id="editor" class="form-control" rows="30"> @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.MedicalReportWritingPartials.MedicalReportHeader', ['sample' => $sample])</textarea>
                </div>
            </div>
        </section>
    </div>
</div>