<div class="row">
    <div class="col-xs-12">
        <section class="panel panel-warning">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Plantillas</h2>
            </header>
            <div class="panel-body">
                <div class="form-group col-md-4">
                    {{ Form::label('medical_report_title_id', 'Tìtulos:') }}
                    {{ Form::select('medical_report_title_id', \CloudMed\Models\MedicalDepartments\PathologicalAnatomy\MedicalReportTitle::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione un título', 'onchange' => 'getTitle();']) }}
                </div>

                <div class="form-group col-md-4">
                    {{ Form::label('medical_report_template_category_id', 'Categoría de plantilla:') }}
                    {{ Form::select('medical_report_template_category_id', \CloudMed\Models\MedicalDepartments\PathologicalAnatomy\MedicalReportTemplateCategory::all()->pluck('name','id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione la categoría de plantilla', 'onchange' => 'getTemplatesByCategory();']) }}
                </div>


                <div class="form-group col-md-4" id="templates"></div>
            </div>
        </section>
    </div>
</div>