<hr />
<div class="row">
    <ul>
        <li>Fecha: <b>{{ \Carbon\Carbon::parse($sample->created_at)->format('d/m/Y') }}</b></li>

        <li>PRACTICADO A: <b>{{ $sample->patientCheckingAccount->patient->name }} ({{$sample->patientCheckingAccount->patient->documentType->name}}: {{ $sample->patientCheckingAccount->patient->document_number }})</b></li>

        <li>EDAD: <b>{{ \Carbon\Carbon::parse($sample->patientCheckingAccount->patient->birthday)->diff(\Carbon\Carbon::now())->format('%y Años') }}</b></li>

        <li>POR INDICACIÓN DEL Dr/a: <b>{{$sample->specialist->name}}</b></li>

        <li>Obra Social: <b>{{ $sample->patientCheckingAccount->checkingAccountBillingInformation[0]->name }}</b></li>
    </ul>
</div>
<hr />