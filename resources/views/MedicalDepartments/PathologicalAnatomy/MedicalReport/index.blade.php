@extends('layouts.app')
@section('pageName')
    Anatomía patológica - Reporte médico
@endsection

@section('content')
    @include('flash::message')
    @include('layouts.partials.table', [
        'tableTitle' => '',
        'tableId' => 'pathologicalAnatomySamplesTable',
        'tableColumns' => ['Fecha', 'Código de barras', 'Tipo', 'Paciente', 'Asignar códigos de Facturación', 'Categorizar estudio', 'Informar', 'Acciones'],
        ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/MedicalDepartments/PathologicalAnatomy/MedicalReport/index.js') }}
@endsection
