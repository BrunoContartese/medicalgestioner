@extends('layouts.app')
@section('pageName')
    Anatomía Patológica - Informe médico - Muestra: {{ $sample->code }}
@endsection

@section('content')
    @include('layouts.errors')
    {{ Form::hidden('sample_id', $sample->id) }}
    @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.MedicalReportWritingPartials.Papanicolaou')
    @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.MedicalReportWritingPartials.Templates')
    @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.MedicalReportWritingPartials.MedicalReport')
    @include('MedicalDepartments.PathologicalAnatomy.MedicalReport.MedicalReportWritingPartials.Signature')

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-sm btn-success" onclick="storeMedicalReport();">Guardar</button>
            <button type="button" class="btn btn-sm btn-danger" onclick="closeTab();">Volver al listado</button>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/MedicalDepartments/PathologicalAnatomy/MedicalReport/writeMedicalReport.js') }}
@endsection
