@extends('layouts.app')
@section('pageName')
    Anatomía Patológica - Categorización de muestra - Muestra: {{ $sample->code }}
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <section class="panel panel-primary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                    <h3 class="panel-title"></h3>
                </header>
                <div class="panel-body">
                    @include('layouts.errors')
                    {{ Form::hidden('sample_id', $sample->id) }}

                    {{ Form::label('pathological_anatomy_category_level_one_id', 'Categoría nivel I') }}
                    {{ Form::select('pathological_anatomy_category_level_one_id', \CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelOne::all()->pluck('name', 'id'), $sample->pathological_anatomy_category_level_one_id, ['class' => 'form-control chosen-select', 'placeholder' => 'Por favor seleccione una categoría', 'onchange' => 'getLevelTwoCategories();']) }}

                    <div id="pathological_anatomy_category_level_two">
                        {{ Form::label('pathological_anatomy_category_level_two_id', 'Categoría nivel II') }}
                        {{ Form::select('pathological_anatomy_category_level_two_id',\CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelTwo::all()->pluck('name','id'), $sample->pathological_anatomy_category_level_two_id, ['class' => 'form-control chosen-select', 'placeholder' => 'Primero seleccione la categoría nivel I']) }}
                    </div>

                    <div id="pathological_anatomy_category_level_three">
                        {{ Form::label('pathological_anatomy_category_level_three_id', 'Categoría nivel III') }}
                        {{ Form::select('pathological_anatomy_category_level_three_id',\CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelThree::all()->pluck('name','id'), $sample->pathological_anatomy_category_level_three_id, ['class' => 'form-control chosen-select', 'placeholder' => 'Primero seleccione la categoría nivel II']) }}
                    </div>

                    <div id="pathological_anatomy_category_level_four">
                        {{ Form::label('pathological_anatomy_category_level_four_id', 'Categoría nivel IV') }}
                        {{ Form::select('pathological_anatomy_category_level_four_id',\CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelFour::all()->pluck('name','id'), $sample->pathological_anatomy_category_level_four_id, ['class' => 'form-control chosen-select', 'placeholder' => 'Primero seleccione la categoría nivel III']) }}
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-sm btn-success" onclick="syncCategories();">Guardar</button>
            <button type="button" class="btn btn-sm btn-danger" onclick="window.close();">Volver al listado</button>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/assets/js/MedicalDepartments/PathologicalAnatomy/MedicalReport/categorizeSample.js') }}
@endsection
