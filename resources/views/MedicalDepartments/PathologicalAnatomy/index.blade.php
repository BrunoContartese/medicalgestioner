@extends('layouts.app')
@section('pageName')
    Anatomía patológica
@endsection

@section('content')
    @include('flash::message')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/medicalDepartments/pathologicalAnatomy/create']),
        'tableId' => 'pathologicalAnatomySamplesTable',
        'tableColumns' => ['Id', 'Institución', 'Departamento', 'Código de barras', 'Tipo', 'Paciente', 'Fecha de recepción', 'Acciones'],
        ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/MedicalDepartments/PathologicalAnatomy/index.js') }}
@endsection
