<div class="form-group">
    <label class="col-sm-3 control-label" for="institution_id">Institución derivante</label>
    <div class="col-sm-4">
        {{ Form::select('institution_id', \CloudMed\Models\Administration\Institution::all()->pluck('name','id'), 1, ['class' => 'form-control chosen']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="pathological_anatomy_study_type_id">Tipo de estudio</label>
    <div class="col-sm-4">
        {{ Form::select('pathological_anatomy_study_type_id', \CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyType::all()->pluck('name','id'), null, ['class' => 'form-control chosen', 'placeholder' => 'Seleccione el tipo de estudio']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="medical_specialist_id">Especialista</label>
    <div class="col-sm-4">
        {{ Form::select('medical_specialist_id', \CloudMed\Models\Administration\Specialist::all()->pluck('name','id'), null, ['class' => 'form-control chosen', 'placeholder' => 'Seleccione un médico']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="medical_department_id">Departamento solicitante</label>
    <div class="col-sm-4">
        {{ Form::select('medical_department_id', \CloudMed\Models\Administration\Department::all()->pluck('name','id'), null, ['class' => 'form-control chosen', 'placeholder' => 'Seleccione un departamento']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="comments">Comentarios</label>
    <div class="col-sm-4">
        {{ Form::textarea('comments', null, ['class' => 'form-control', 'id' => 'comments']) }}
    </div>
</div>
