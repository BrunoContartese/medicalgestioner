{!! Form::open(['id' => $id, 'route' => ['pathologicalAnatomyStudy.destroy', $id], 'method' => 'delete']) !!}
    @can('pathologicalAnatomy.edit')
        <a href="/medicalDepartments/pathologicalAnatomy/printSticker/{{$row->code}}" type="button" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-barcode"></i></a>
    @endcan
    @can('pathologicalAnatomy.delete')
        {!! Form::button('<i class="fa fa-trash"></i>', [
                'class' => 'btn btn-sm btn-danger',
                'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el estudio?'); return false;",
                'title' => 'Eliminar estudio'
        ]) !!}
    @endcan
{!! Form::close() !!}
