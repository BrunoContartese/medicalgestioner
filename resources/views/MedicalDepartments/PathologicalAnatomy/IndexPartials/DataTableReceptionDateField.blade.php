@if(isset($row->received_at))
    <button type="button" class="btn btn-success btn-sm">{{ \Carbon\Carbon::parse($row->received_at)->format('d/m/Y H:i') }}</button>
@else
    <button type="button" class="btn btn-warning btn-sm">NO RECEPCIONADO</button>
@endif
