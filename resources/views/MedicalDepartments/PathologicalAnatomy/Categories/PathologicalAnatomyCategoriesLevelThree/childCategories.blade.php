{{ Form::label('pathological_anatomy_category_level_four_id', 'Categoría nivel IV') }}
{{ Form::select('pathological_anatomy_category_level_four_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una categoría']) }}
