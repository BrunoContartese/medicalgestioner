{{ Form::label('pathological_anatomy_category_level_three_id', 'Categoría nivel III') }}
{{ Form::select('pathological_anatomy_category_level_three_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una categoría', 'onchange' => 'getLevelFourCategories();']) }}
