{{ Form::label('pathological_anatomy_category_level_two_id', 'Categoría nivel II') }}
{{ Form::select('pathological_anatomy_category_level_two_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una categoría', 'onchange' => 'getLevelThreeCategories();']) }}
