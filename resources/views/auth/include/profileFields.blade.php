<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {{ Form::label('name', 'Apellido y nombre', ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-4">
                {{ Form::text('name', $user->name, ['class' => 'form-control', 'disabled']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Correo electrónico', ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-4">
                {{ Form::text('email', $user->email, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('username', 'Nombre de usuario', ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-4">
                {{ Form::text('username', $user->username, ['class' => 'form-control', 'disabled']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password', 'Contraseña', ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-4">
                <input type="password" name="password" class="form-control" placeholder="Nueva contraseña" />
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('repeat_password', 'Repita la contraseña', ['class' => 'col-md-2 control-label']) }}
            <div class="col-md-4">
                <input type="password" name="repeat_password" class="form-control" placeholder="Repita la nueva contraseña" />
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-warning" type="button" onclick="updateProfile();">Actualizar datos</button>
        </div>
    </div>
</div>
