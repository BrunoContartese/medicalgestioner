<div class="form-group col-md-6">
    <div style="text-align: center;">
        <img class="rounded-circle" width="200px" height="200px" alt="100x100" src="{{$user->signature_image_url}}" data-holder-rendered="true">
    </div>
    <input type="file" class="form-control" name="signatureImageFile" id="signatureImageFile"/>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-warning" type="button" onclick="updateSignatureImage();">Actualizar firma</button>
        </div>
    </div>
</div>