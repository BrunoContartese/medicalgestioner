@extends('layouts.app')
@section('pageName')
    Firma
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel panel-warning">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                    <h3 class="panel-title">Cambiar Firma</h3>
                </header>

                <div class="panel-body">
                    @include('layouts.errors')
                    @include('auth.include.profileSignatureFields')
                </div>
            </section>
        </div>
    </div>
@endsection

@section("scripts")
    {{ Html::script("/assets/js/users/profile.js") }}
@endsection