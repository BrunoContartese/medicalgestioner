@extends('auth.loginLayout')

@section('fields')
    @include('layouts.errors')
    @include('auth.loginFields')
@stop
