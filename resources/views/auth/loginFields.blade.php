<div class="form-group">
    {!! Form::label('username', 'Usuario', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Nombre de usuario']) !!}
    </div>

</div>
<div class="form-group">
    {!! Form::label('password', 'Contraseña', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <input type="password" class="form-control" name="password" placeholder="Contraseña">
    </div>
</div>