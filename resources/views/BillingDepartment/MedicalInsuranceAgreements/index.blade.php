@extends('layouts.app')
@section('pageName')
    Convenios
@endsection

@section('content')
    @include('flash::message')
    @include('layouts.partials.table', [
        'tableTitle' => view('layouts.partials.newRowButton', ['url' => '/billingDepartment/medicalInsuranceAgreements/create']),
        'tableId' => 'medicalInsuranceAgreementsTable',
        'tableColumns' => ['Obra Social', 'Nomenclador', 'Descripción', 'Válido desde', 'Válido Hasta', 'Acciones'],
        ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/BillingDepartment/MedicalInsuranceAgreements/index.js') }}
@endsection
