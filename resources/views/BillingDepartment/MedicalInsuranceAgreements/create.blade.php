@extends('layouts.app')
@section('pageName')
    Crear Convenio
@endsection

@section('content')
    <div class="row">
        @include('layouts.errors')
        <div id="ajaxErrors"></div>
    </div>
        @include('BillingDepartment.MedicalInsuranceAgreements.agreementInfoPanel')
        @include('BillingDepartment.MedicalInsuranceAgreements.rangesAgreementPanel')
        <br>
        @include('BillingDepartment.MedicalInsuranceAgreements.confirmAgreementPanel')
@endsection

@section('scripts')
    {{ Html::script('/assets/js/BillingDepartment/MedicalInsuranceAgreements/create.js') }}
@endsection