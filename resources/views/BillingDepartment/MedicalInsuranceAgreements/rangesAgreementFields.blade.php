<div class="form-inline">
    <div class="form-group col-md-12">
        {!! Form::select('range_id', \CloudMed\Models\Administration\Nomenclators\NomenclatorRange::all()->pluck('description', 'id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Rango']) !!}
        {!! Form::text('expense_unit_value', null, ['class' => 'form-control', 'placeholder' => 'Valor de unidad de gasto']) !!}
        {!! Form::text('honorary_unit_value', null, ['class' => 'form-control', 'placeholder' => 'Valor de unidad de honorario']) !!}
        <button type="button" class="btn btn-sm form-control btn-warning" onclick="addRow();">Agregar</button>
    </div>
</div>