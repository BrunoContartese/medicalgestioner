<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Confirmación</h3>
            </header>

            <div class="panel-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <button type="button" onclick="storeAgreement();" class="btn btn-warning">Guardar</button>
                        <a href="{!! route('medicalInsuranceAgreements.index') !!}" class="btn btn-danger">Cancelar</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>