@extends('layouts.app')
@section('pageName')
    Editar Convenio
@endsection

@section('content')
    <input type="hidden" value="{{$agreement->id}}" name="agreementId">
    <div class="row">
        @include('layouts.errors')
        <div id="ajaxErrors"></div>
    </div>
    @include('BillingDepartment.MedicalInsuranceAgreements.agreementInfoPanel', ['agreement', $agreement])
    @include('BillingDepartment.MedicalInsuranceAgreements.rangesAgreementPanel')
    <br>
    @include('BillingDepartment.MedicalInsuranceAgreements.confirmAgreementPanel')
@endsection

@section('scripts')
    {{ Html::script('/assets/js/BillingDepartment/MedicalInsuranceAgreements/edit.js') }}
    <script>
        function addRangesValuesAgreementToTable() {
            dataTable.api().rows.add([
                @foreach($agreement->rangesValuesAgreement as $range)
                    {
                        code: {{ $range->id }},
                        description: "{{ $range->from_code }}-{{ $range->to_code }}",
                        expense_unit_value: {{ $range->pivot->expense_unit_value }},
                        honorary_unit_value: {{ $range->pivot->honorary_unit_value }},
                        actions: buttonHtml()
                    },
                @endforeach
            ]).draw(true);
        }

        $(document).ready(function() {
            @foreach($agreement->rangesValuesAgreement as $range)
                disableOption("{{ $range->from_code }}-{{ $range->to_code }}");
            @endforeach
        });
    </script>
@endsection