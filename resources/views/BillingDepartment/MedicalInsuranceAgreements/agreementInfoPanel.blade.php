<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Datos del convenio</h3>
            </header>

            <div class="panel-body">
                @include('layouts.errors')

                @include('BillingDepartment.MedicalInsuranceAgreements.fields')

            </div>
        </section>
    </div>
</div>