@extends('layouts.app')
@section('pageName')
    Prácticas convenidas en "{{ $medicalInsuranceAgreement->description }}"
@endsection

@section('content')
    {{ Form::hidden('medicalInsuranceAgreementId', $medicalInsuranceAgreement->id) }}
    @include('flash::message')
    @include('layouts.partials.table', [
        'tableTitle' => "<button type='button' class='btn btn-primary' onclick='addPracticeModal();'>Añadir práctica</button>",
        'tableId' => 'medicalInsurancePracticesAgreementsTable',
        'tableColumns' => ['Código', 'Descripción', 'Valor de gasto', 'Valor de honorario','Cantidad de ayudantes', 'Acciones'],
        ])
@endsection

@section('scripts')
    {{ Html::script('/assets/js/BillingDepartment/MedicalInsuranceAgreements/practicesAgreement.js') }}
@endsection