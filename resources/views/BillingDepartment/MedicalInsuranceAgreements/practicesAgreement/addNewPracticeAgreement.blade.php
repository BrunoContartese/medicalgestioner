<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Agregar práctica al convenio</h3>
            </header>

            <div class="panel-body">
                <div id="ajaxErrors"></div>
                <div class="form-group">
                    {{ Form::label('code', 'Seleccione una práctica', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::select('nomenclator_practice_id', \CloudMed\Models\Administration\Nomenclators\NomenclatorPractice::all()->pluck('code', 'id'), null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una práctica', 'onchange' => 'getPracticeInfoById();']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Descripción', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::textarea('description', null, ['class' => 'form-control', 'disabled']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('fixed_expense_value', 'Valor del gasto', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('fixed_expense_value', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('fixed_honorary_value', 'Valor del honorario', ['class' => 'col-md-3 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('fixed_honorary_value', null, ['class' => 'form-control']) }}
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>