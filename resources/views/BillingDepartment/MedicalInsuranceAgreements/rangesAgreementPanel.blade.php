<div class="row">
    <div class="col-lg-12">
        <section class="rangesPanel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Valorización de rangos</h3>
            </header>

            <div class="panel-body">
                <div class="row">
                    @include('BillingDepartment.MedicalInsuranceAgreements.rangesAgreementFields')
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('BillingDepartment.MedicalInsuranceAgreements.rangesAgreementTable')
                    </div>

                </div>

            </div>
        </section>
    </div>
</div>