{{--{!! Form::open(['route' => ['medicalInsuranceAgreement.uploadPractices', $medicalInsuranceAgreementId], 'id' => 'uploadPracticesAgreement']) !!}--}}
<div class="row">
    <div class="col-lg-12">
        <div id="ajaxErrors"></div>
        <section class="rangesPanel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h3 class="panel-title">Subir archivo de prácticas</h3>
            </header>

            <div class="panel-body">
                <section class="panelWarningInfo panel-featured panel-featured-warning">
                    <header class="panel-heading">
                        <h2 class="panel-title">ATENCION!</h2>
                    </header>

                    <div class="panel-body">
                        <h5>Si el convenio ya tiene prácticas valorizadas serán pisadas por las prácticas que estén cargadas en el archivo a subir.</h5>
                    </div>
                </section>
                <div class="form-group col-md-12">
                    <label for="practicesAgreementFile">Subir archivo de prácticas</label>
                    <input type="file" id="practicesAgreementFile">
                    <p class="help-block"><a href="/files/BillingDepartment/modelo_excel_practicas_convenios.xlsx" target="_blank">Haga click aquí para descargar el archivo base</a></p>
                </div>
            </div>
        </section>
    </div>
</div>
{{--{!! Form::close() !!}--}}