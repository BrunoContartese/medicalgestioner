{!! Form::open(['id' => $id, 'route' => ['medicalInsuranceAgreements.destroy', $id], 'method' => 'delete']) !!}
    @can('medicalInsuranceAgreements.edit')
        <button type="button" class="btn btn-sm btn-default" title="Cargar archivo de convenio de prácticas" style="display:inline;" onclick="showUploadPracticesAgreementModal({{$id}});"><i class="fa fa-cloud-upload-alt"></i></button>
    @endcan

    @can('medicalInsuranceAgreements.edit')
        <a href="/billingDepartment/practicesAgreement/{{$id}}" target="_blank" class="btn btn-sm btn-default" title="Ver convenio de prácticas" style="display:inline;" onclick="showPracticesAgreementModal({{$id}});"><i class="fa fa-th-list"></i></a>
    @endcan

    @can('medicalInsuranceAgreements.edit')
        <a href="{{route('medicalInsuranceAgreements.edit', $id)}}" class="btn btn-sm btn-default" title="Editar convenio"><i class="fa fa-pencil-alt"></i></a>
    @endcan
    @can('medicalInsuranceAgreements.delete')
        {!! Form::button('<i class="fa fa-trash"></i>', [
                'class' => 'btn btn-sm',
                'onclick' => "return confirmSubmit(".$id.",'¿Está seguro de que desea eliminar el convenio?'); return false;",
                'title' => 'Eliminar convenio'
        ]) !!}
    @endcan
{!! Form::close() !!}

