<div class="form-group">
    {{ Form::label('description', 'Nombre del convenio', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('description', isset($agreement) ? $agreement->description : null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('medical_insurance_id', 'Obra Social', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('medical_insurance_id', \CloudMed\Models\Administration\MedicalInsurance::all()->pluck('name','id'), isset($agreement) ? $agreement->medical_insurance_id : null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una obra social']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('nomenclator_id', 'Nomenclador', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('nomenclator_id', \CloudMed\Models\Administration\Nomenclators\Nomenclator::all()->pluck('name','id'), isset($agreement) ? $agreement->nomenclator_id : null, ['class' => 'form-control chosen-select', 'placeholder' => 'Seleccione una nomenclador']) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('valid_from', 'Válido desde:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::date('valid_from', isset($agreement) ? $agreement->valid_from : null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('valid_until', 'Válido hasta:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        {{ Form::date('valid_until', isset($agreement) ? $agreement->valid_until : null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('without_end_date', 'Sin fecha de vencimiento:', ['class' => 'col-md-3 control-label']) }}
    <div class="col-md-6">
        <input type="checkbox" value="1" name="without_end_date" {{isset($agreement) && $agreement->without_end_date ? 'checked' : ''}} />
    </div>
</div>

