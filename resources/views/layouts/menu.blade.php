<ul class="nav nav-main">
    @include('layouts.partials.simpleMenu', [
        'permission' => 'dashboard.view',
        'request' => '*',
        'url' => '/',
        'icon' => 'fa fa-home',
        'name' => 'Principal'
        ])

    {{--Check In Menu--}}
    @include('layouts.partials.Menus.PatientCheckingAccountsMenu')

    {{--Patients Menu--}}
    @include('layouts.partials.Menus.PatientsMenu')

    {{--Pharmacy Menu--}}
    @include('layouts.partials.Menus.PharmacyMenu')

    {{--Billing Department Menu--}}
    @include('layouts.partials.Menus.BillingDepartmentMenu')

    {{--Human Resources Menu--}}
    @include('layouts.partials.Menus.HumanResourcesMenu')

    {{--Administration Menu--}}
    @include('layouts.partials.Menus.AdministrationMenu')

    {{--Medical Deparments Menu--}}
    @include('layouts.partials.Menus.MedicalDepartmentsMenu')

    {{--Support Menu--}}
    @include('layouts.partials.Menus.SupportMenu')



</ul>
