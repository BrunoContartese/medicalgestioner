<!doctype html>
<html class="fixed">
    @include('layouts.head')
    <body>
    <section class="body">

        <!-- start: header -->
        <header class="header">
            <div class="logo-container">

                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <!-- start: search & user box -->
            <div class="header-right">
                <span class="separator"></span>

                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="{{Auth::user()->profile_image_url}}" alt="Joseph Doe" class="img-circle" data-lock-picture="{{Auth::user()->profile_image_url}}" />
                        </figure>
                        <div class="profile-info" data-lock-name="{{Auth::user()->name}}" data-lock-email="{{Auth::user()->email}}">
                            <span class="name">{{Auth::user()->name}}</span>
                            <span class="role"></span>
                        </div>

                        <i class="fa fa-caret-down"></i>
                    </a>

                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>

                            <form id="logout" method="post" action="/logout">
                                @csrf
                                <li>
                                    <a role="menuitem" tabindex="-1" href="/profile"><i class="fa fa-user"></i> Mi perfil</a>
                                </li>
                                <li>
                                    <a role="menuitem" tabindex="-1" href="/profile/image"><i class="fa fa-file"></i> Mi imágen de perfil</a>
                                </li>
                                <li>
                                    <a role="menuitem" tabindex="-1" href="/profile/signature"><i class="fa fa-file-signature"></i> Mi firma</a>
                                </li>
                                <li>
                                    <a role="menuitem" onclick="$('#logout').submit();" href="#"><i class="fa fa-sign-out"></i> Cerrar sesión</a>
                                </li>
                            </form>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                    <div class="sidebar-title">

                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            @include('layouts.menu')
                        </nav>
                </div>

            </aside>
            <!-- end: sidebar -->

            <section role="main" class="content-body">

                @include('layouts.header')

                @yield('content')

            </section>
        </div>

    </section>

        @include('layouts.requiredJS')
    </body>
    <div class="se-pre-con"></div>
    @toastr_render
</html>