@can($permission)
    <li class="{{Request::is($request) ? 'nav-active' : ''}}">
        <a href="{{$url}}">
            <i class="{{$icon}}" aria-hidden="true"></i>
            <span>{{$name}}</span>
        </a>
    </li>
@endcan