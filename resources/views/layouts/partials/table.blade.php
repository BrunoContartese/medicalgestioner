
{{--Parameters:
    $tableTitle
    $tableSubtitle
    $tableId
    $tableColumns
--}}

<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
        </div>

        <h2 class="panel-title">{!! $tableTitle !!}</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">

                    <table class="table table-striped table-condensed mb-none" id="{{$tableId}}">
                        <thead>
                        <tr>
                            @foreach($tableColumns as $column)
                                <th>{{$column}}</th>
                            @endforeach
                        </tr>
                        </thead>
                    </table>

            </div>
        </div>
    </div>
</section>