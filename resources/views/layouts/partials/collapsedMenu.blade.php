@canany($permissions)
<li class="nav-parent  {{Request::is($request) ? 'nav-active' : ''}}">
    <a>
        <i class="{{$icon}}" aria-hidden="true"></i>
        <span>{{$name}}</span>
    </a>
    <ul class="nav nav-children">
        @foreach($items as $item)
            {!! $item !!}
        @endforeach
    </ul>
@endcanany