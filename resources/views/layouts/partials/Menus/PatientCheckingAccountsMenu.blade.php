@include('layouts.partials.collapsedMenu', [
   'id' => 'patientCheckingAccounts',
   'name' => 'Check In de Pacientes',
   'request' => 'patientCheckingAccounts*',
   'icon' => 'fa fa-ticket-alt',
   'permissions' => ['ambulatoryPatientCheckingAccounts.view'],
   'items' => [
       /*Seguros de mala práxis*/
       view('layouts.partials.simpleMenu')->with(['request' => 'patientCheckingAccounts/ambulatory/checkIn*' , 'icon' => 'fa fa-sign-in-alt' , 'name' => 'Ambulatorio' , 'url' => '/patientCheckingAccounts/ambulatory/checkIn' , 'permission' => 'ambulatoryPatientCheckingAccounts.view']),
       ]
])