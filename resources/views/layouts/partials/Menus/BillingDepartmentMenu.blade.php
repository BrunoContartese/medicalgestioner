@include('layouts.partials.collapsedMenu', [
   'id' => 'billingDepartment',
   'name' => 'Facturación',
   'request' => 'BillingDepartment*',
   'icon' => 'fa fa-money-bill',
   'permissions' => ['medicalInsuranceAgreements.view', 'medicalInsurances.view', 'medicalInsurancesManagers.view', 'nomenclators.view', 'nomenclatorRanges.view', 'nomenclatorPracticesGroup.view', 'PatientCheckingAccounts.view'],
   'items' => [
     /*Nomenclators*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/nomenclators*', 'icon' => 'fa fa-list', 'name' => 'Nomencladores', 'url' => '/administration/nomenclators', 'permission' => 'nomenclators.view']),
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/nomenclatorRanges*', 'icon' => 'fa fa-sliders-h', 'name' => 'Rango de nomencladores', 'url' => '/administration/nomenclatorRanges', 'permission' => 'nomenclatorRanges.view']),
       /*Medical Insurances*/
       view('layouts.partials.collapsedMenu', [
           'id' => 'medicalInsurances',
           'name' => 'Obras Sociales',
           'request' => 'administration/medicalInsurances*',
           'icon' => 'fa fa-credit-card',
           'permissions' => ['medicalInsurances.view', 'medicalInsurancesManagers.view'],
           'items' => [
               view('layouts.partials.simpleMenu')->with(['request' => 'administration/medicalInsurances*', 'icon' => 'assignment_ind', 'name' => 'Obras Sociales', 'url' => '/administration/medicalInsurances', 'permission' => 'medicalInsurances.view']),
               view('layouts.partials.simpleMenu')->with(['request' => 'administration/medicalInsurancesManagers*', 'icon' => 'assignment_ind', 'name' => 'Gerenciadoras', 'url' => '/administration/medicalInsurancesManagers', 'permission' => 'medicalInsurancesManagers.view']),
               ]
           ]),
       /*Agreements*/
       view('layouts.partials.simpleMenu')->with(['request' => 'billingDepartment/medicalInsuranceAgreements*', 'icon' => 'fas fa-file-signature', 'name' => 'Convenios', 'url' => '/billingDepartment/medicalInsuranceAgreements', 'permission' => 'medicalInsuranceAgreements.view']),
       /*Grupos de Practicas de Nomencladores*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/nomenclatorPracticesGroup*' , 'icon' => 'fa fa-object-group' , 'name' => 'Grupos de Practicas' , 'url' => '/administration/nomenclatorPracticesGroup' , 'permission' => 'nomenclatorPracticesGroup.view']),

       view('layouts.partials.simpleMenu')->with(['request' => 'billingDepartment/patientCheckingAccounts/*' , 'icon' => 'fa fa-file-invoice' , 'name' => 'Rendiciones' , 'url' => '/billingDepartment/patientCheckingAccounts' , 'permission' => 'PatientCheckingAccounts.view']),

       ]
])