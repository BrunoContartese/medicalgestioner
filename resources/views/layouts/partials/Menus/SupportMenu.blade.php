@include('layouts.partials.collapsedMenu', [
   'id' => 'support',
   'name' => 'Soporte',
   'request' => 'support*',
   'icon' => 'fa fa-tools',
   'permissions' => ['toolsMenu.view'],
   'items' => [
       /*Phpmyadmin*/
       view('layouts.partials.simpleMenu')->with(['request' => 'tools*', 'icon' => 'fa fa-database', 'name' => 'PhpMyAdmin', 'url' => '/phpmyadmin', 'permission' => 'toolsMenu.view']),
       /*Log viewer*/
       view('layouts.partials.simpleMenu')->with(['request' => 'tools*', 'icon' => 'fa fa-bug', 'name' => 'Logs', 'url' => '/log-viewer', 'permission' => 'toolsMenu.view']),
        ]
])
