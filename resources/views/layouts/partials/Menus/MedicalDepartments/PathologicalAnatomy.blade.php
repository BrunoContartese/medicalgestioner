@include('layouts.partials.collapsedMenu', [
   'id' => 'pathologicalAnatomy',
   'name' => 'Anatomía Patológica',
   'request' => 'medicalDepartments/pathologicalAnatomy*',
   'icon' => 'fa fa-microscope',
   'permissions' => ['pathologicalAnatomy.view', 'pathologicalAnatomy.makeMedicalReport',],
   'items' => [
       /*Index*/
       view('layouts.partials.simpleMenu')->with(['request' => 'medicalDeparments/pathologicalAnatomy*', 'icon' => 'fa fa-microscope', 'name' => 'Muestras', 'url' => '/medicalDepartments/pathologicalAnatomy', 'permission' => 'pathologicalAnatomy.view']),
       /*Informar Muestras*/
       view('layouts.partials.simpleMenu')->with(['request' => 'medicalDeparments/pathologicalAnatomy/medicalReport*', 'icon' => 'fa fa-stethoscope', 'name' => 'Informar Muestras', 'url' => '/medicalDepartments/pathologicalAnatomy/medicalReport', 'permission' => 'pathologicalAnatomy.makeMedicalReport']),

       ]
])
