@include('layouts.partials.collapsedMenu', [
   'id' => 'pharmacy',
   'name' => 'Farmacia',
   'request' => 'Pharmacy*',
   'icon' => 'fa fa-medkit',
   'permissions' => ['medicaments.view', 'medicaments.update'],
   'items' => [
       /*Profiles*/
       view('layouts.partials.simpleMenu')->with(['request' => 'Pharmacy/Medicaments*', 'icon' => 'fa fa-syringe', 'name' => 'Medicamentos', 'url' => '/pharmacy/medicaments', 'permission' => 'medicaments.view']),

       /*Medicaments Updates*/
       view('layouts.partials.simpleMenu')->with(['request' => 'Pharmacy/medicamentsUpdate*', 'icon' => 'fa fa-sync', 'name' => 'Actualizar medicamentos', 'url' => '/pharmacy/medicamentsUpdate', 'permission' => 'medicaments.update']),
       ]
])