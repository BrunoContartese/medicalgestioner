@include('layouts.partials.collapsedMenu', [
   'id' => 'administration',
   'name' => 'Administración',
   'request' => 'administration*',
   'icon' => 'fa fa-cog',
   'permissions' => ['roles.view', 'users.view', 'specialties.view', 'departments.view', 'deposits.view','specialists.view', 'nomenclatorPracticesGroup.view', 'medicalMalpracticeInsurances.view', ],
   'items' => [
       /*Profiles*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/roles*', 'icon' => 'fa fa-lock', 'name' => 'Perfiles', 'url' => '/administration/roles', 'permission' => 'roles.view']),
       /*Users*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/users*', 'icon' => 'fa fa-users', 'name' => 'Usuarios', 'url' => '/administration/users', 'permission' => 'users.view']),
      /*Especialidades*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/specialties*' , 'icon' => 'fa fa-stethoscope' , 'name' => 'Especialidades' , 'url' => '/administration/specialties' , 'permission' => 'specialties.view']),
       /*Departamentos*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/departments*' , 'icon' => 'fa fa-building' , 'name' => 'Departamentos' , 'url' => '/administration/departments' , 'permission' => 'departments.view']),
       /*Depositos*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/deposits*' , 'icon' => 'fa fa-warehouse' , 'name' => 'Depositos' , 'url' => '/administration/deposits' , 'permission' => 'deposits.view']),
       /*Especialistas*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/specialists*' , 'icon' => 'fa fa-user-md' , 'name' => 'Especialistas' , 'url' => '/administration/specialists' , 'permission' => 'specialists.view']),
       /*Seguros de mala práxis*/
       view('layouts.partials.simpleMenu')->with(['request' => 'administration/medicalMalpracticeInsurances*' , 'icon' => 'fa fa-shield-alt' , 'name' => 'Seguros de Mala Práxis' , 'url' => '/administration/medicalMalpracticeInsurances' , 'permission' => 'medicalMalpracticeInsurances.view']),
       ]
])