@include('layouts.partials.collapsedMenu', [
   'id' => 'medicalDepartments',
   'name' => 'Departamentos Médicos',
   'request' => 'medicalDepartments*',
   'icon' => 'fa fa-briefcase-medical',
   'permissions' => ['pathologicalAnatomy.view'],
   'items' => [
        view('layouts.partials.Menus.MedicalDepartments.PathologicalAnatomy')
        ]
    ])
