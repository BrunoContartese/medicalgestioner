@include('layouts.partials.collapsedMenu', [
    'id' => 'patients',
   'name' => 'Pacientes',
   'request' => 'administration/patients/*',
   'icon' => 'fa fa-procedures',
   'permissions' => ['patients.view'],
   'items' => [
          view('layouts.partials.simpleMenu')->with(['request' => 'administration/patients*' , 'icon' => 'fa fa-users' , 'name' => 'Listado de Pacientes' , 'url' => '/administration/patients' , 'permission' => 'patients.view'])
   ]
])