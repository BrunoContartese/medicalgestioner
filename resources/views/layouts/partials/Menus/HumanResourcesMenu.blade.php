{{--Human Resources Menu--}}
@include('layouts.partials.collapsedMenu', [
   'id' => 'humanResources',
   'name' => 'Recursos Humanos',
   'request' => 'humanResources*',
   'icon' => 'fa fa-users',
   'permissions' => ['employeesCategories.view'],
   'items' => [
       /*Agreements*/
       view('layouts.partials.simpleMenu')->with(['request' => 'humanResourcesDepartment/employeesCategories*', 'icon' => 'fas fa-file-signature', 'name' => 'Categorías de Empleados', 'url' => '/humanResourcesDepartment/employeesCategories', 'permission' => 'employeesCategories.view']),

       ]
])