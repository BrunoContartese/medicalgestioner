<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>CloudMed</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="Software HIS" />
    <meta name="author" content="Bruno Contartese">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    @include('layouts.requiredCSS')

</head>