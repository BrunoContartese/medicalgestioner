{{-- Jquery --}}
{{ Html::script("/assets/vendor/jquery/jquery.js") }}
{{ Html::script("/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js") }}
{{ Html::script("/assets/vendor/jquery-placeholder/jquery.placeholder.js") }}

{{-- Bootstrap --}}
{{ Html::script("/assets/vendor/bootstrap/js/bootstrap.js") }}
{{ Html::script("/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js") }}

{{-- NanoScroller --}}
{{ Html::script("/assets/vendor/nanoscroller/nanoscroller.js") }}

{{-- Magnific Popup --}}
{{ Html::script("/assets/vendor/magnific-popup/magnific-popup.js") }}

{{-- Octopus theme --}}
{{ Html::script("/assets/javascripts/theme.js") }}
{{ Html::script("/assets/javascripts/theme.custom.js") }}
{{ Html::script("/assets/javascripts/theme.init.js") }}

{{-- Datatables --}}
{{ Html::script("/assets/vendor/DataTables/jquery.dataTables.min.js") }}
{{ Html::script("/assets/vendor/DataTables/dataTables.bootstrap.min.js") }}
{{ Html::script("/assets/vendor/DataTables/responsive.bootstrap.min.js") }}
{{ Html::script("/assets/vendor/DataTables/dataTables.select.min.js") }}

{{-- JQuery validate --}}
{{ Html::script("/assets/vendor/jquery-validation/jquery.validate.js") }}

{{-- Jquery bootstrap wizard --}}
{{ Html::script("/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js")}}

{{-- PNotify --}}
{{ Html::script("/assets/vendor/pnotify/pnotify.custom.js") }}

{{-- Toastr --}}
{{ Html::script('/assets/vendor/toastr/toastr.min.js') }}

{{-- APP specifyc scripts --}}
{{ Html::script('/assets/js/generalScripts.js') }}

{{-- Bootbox --}}
{{ Html::script('/components/bootbox/bootbox.all.min.js') }}
{{ Html::script('/components/bootbox/bootbox.locales.min.js') }}

{{-- Textbox IO --}}
{{ Html::script('/components/textboxio/textboxio.js') }}

{{-- Axios --}}
{{ Html::script("/assets/vendor/axios/axios.min.js") }}

{{-- Validator --}}
{{ Html::script("/assets/vendor/validator/validator.min.js") }}

{{-- Moment --}}
{{ Html::script("/assets/vendor/moment.js") }}

{{-- Chosen select --}}
{{ Html::script('/components/chosen-select/chosen.jquery.min.js') }}

@yield('scripts')
