{{-- FONTS --}}
{{ Html::style('/assets/vendor/googleFonts/ShadowsIntoLight.css') }}

{{-- Bootstrap--}}
{{ Html::style("/assets/vendor/bootstrap/css/bootstrap.css" ) }}

{{-- FontAwesome --}}
{{ Html::style("/assets/vendor/fontawesome5/css/all.min.css" ) }}
{{ Html::style("/assets/vendor/fontawesome5/css/fontawesome.min.css" ) }}

{{-- Magnific Popup--}}
{{ Html::style("/assets/vendor/magnific-popup/magnific-popup.css" ) }}

{{-- Bootstrap Datepicker--}}
{{ Html::style("/assets/vendor/bootstrap-datepicker/css/datepicker3.css" ) }}

{{-- Select 2--}}
{{ Html::style("/assets/vendor/select2/select2.css" ) }}

{{-- Jquery UI--}}
{{ Html::style("/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" ) }}

{{-- Bootstrap multiselect--}}
{{ Html::style("/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" ) }}

{{-- Chosen Select --}}
{{ Html::style('/components/chosen-select/chosen.min.css') }}
{{ Html::style("/assets/stylesheets/chosen-bootstrap.css") }}

{{-- Toastr--}}
{{ Html::style('/assets/css/plugins/toastr.css') }}

{{-- Octopus Theme --}}
{{ Html::style("/assets/stylesheets/theme.css" ) }}

{{ Html::style("/assets/stylesheets/skins/default.css" ) }}
{{ Html::style("/assets/stylesheets/theme-custom.css") }}

{{-- Custom APP styles --}}
{{ Html::style("/assets/stylesheets/style.css") }}

{{-- DataTables --}}
{{ Html::style("/assets/vendor/DataTables/responsive.bootstrap.min.css") }}
{{ Html::style("/assets/vendor/DataTables/select.bootstrap.min.css") }}

<!-- Modernizr -->
{{ Html::script("/assets/vendor/modernizr/modernizr.js") }}

@yield('styles')
