<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function() {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/profile', 'Administration\UsersController@profile')->name('profile');
    Route::post('/profile', 'Administration\UsersController@updateProfile')->name('profile.update');
    Route::get('/profile/image', 'Administration\UsersController@profileImage');
    Route::post('/profile/image', 'Administration\UsersController@updateProfileImage')->name('profileImage.update');
    Route::get('/profile/signature', 'Administration\UsersController@signatureImage');
    Route::post('/profile/signature', 'Administration\UsersController@updateSignatureImage')->name('signatureImage.update');
    Route::get('/profile/getSignature', 'Administration\UsersController@getSignature');


    /**
     * Usuarios
     */
    Route::resource('administration/users', 'Administration\UsersController');
    Route::get('/datatable/administration/users', 'Administration\UsersController@dataTable');

    /**
     * Roles / Perfiles
     */
    Route::resource('administration/roles', 'Administration\RolesController');
    Route::get('/datatable/administration/roles', 'Administration\RolesController@dataTable');
    Route::get('/administration/roles/{RoleId}/setPermissions', 'Administration\RolesController@editPermissions')->name('roles.editPermissions');
    Route::post('/administration/roles/{RoleId}/setPermissions', 'Administration\RolesController@updatePermissions')->name('roles.updatePermissions');

    /**
     * Obras Sociales
     */
    Route::resource('administration/medicalInsurances', 'Administration\MedicalInsurances');
    Route::get('/datatable/administration/medicalInsurances', 'Administration\MedicalInsurances@dataTable');
    Route::get('/administration/medicalInsurance/writeNews/{medicalInsuranceId}', 'Administration\MedicalInsurances@writeNews');
    Route::post('/administration/medicalInsurance/storeNews/{medicalInsuranceId}', 'Administration\MedicalInsurances@storeNews');
    Route::post('/administration/medicalInsurance/{medicalInsuranceId}/restore', 'Administration\MedicalInsurances@restoreTrashed')->name('medicalInsurances.restore');

    /**
     * Gerenciadoras
     */
    Route::resource('administration/medicalInsurancesManagers', 'Administration\MedicalInsurancesManagers');
    Route::get('/datatable/administration/medicalInsurancesManagers', 'Administration\MedicalInsurancesManagers@dataTable');
    Route::post('/administration/medicalInsurancesManagers/{medicalInsuranceManagerId}/restore', 'Administration\MedicalInsurancesManagers@restoreTrashed')->name('medicalInsurancesManagers.restore');

    /**
     * Nomencladores
     */
    Route::resource('administration/nomenclators', 'Administration\Nomenclators');
    Route::post('/administration/nomenclators/{nomenclator}/restore', 'Administration\Nomenclators@restoreTrashed')->name('nomenclators.restore');
    Route::get('/datatable/administration/nomenclators', 'Administration\Nomenclators@dataTable');

    /**
     * Prácticas de Nomencladores
     */
    //LIST
    Route::get('/administration/nomenclatorPractices/{nomenclatorId}', 'Administration\NomenclatorPractices@practicesList');
    Route::get('/datatable/administration/nomenclatorPractices/{nomenclatorId}', 'Administration\NomenclatorPractices@dataTable');
    //CREATE
    Route::get('/administration/nomenclatorPractices/create/{nomenclatorId}', 'Administration\NomenclatorPractices@create')->name('nomenclatorPractice.create');
    Route::post('/administration/nomenclatorPractices/create/{nomenclatorId}', 'Administration\NomenclatorPractices@store')->name('nomenclatorPractice.store');
    //EDIT
    Route::get('/administration/nomenclatorPractice/edit/{nomenclatorPracticeId}', 'Administration\NomenclatorPractices@edit')->name('nomenclatorPractice.edit');
    Route::patch('/administration/nomenclatorPractices/edit/{nomenclatorPracticeId}', 'Administration\NomenclatorPractices@update')->name('nomenclatorPractice.update');
    //GET INFO
    Route::get('/administration/nomenclatorPractices/get/{nomenclatorPracticeId}', 'Administration\NomenclatorPractices@getInfoById');
    Route::get('/administration/nomenclatorPractices/getByCode/{nomenclatorPracticeCode}', 'Administration\NomenclatorPractices@getInfoByCode');


    /** Rutas Especialidades */

    Route::resource('administration/specialties', 'Administration\SpecialtiesController');
    Route::post('/administration/specialties/{specialties}/restore', 'Administration\SpecialtiesController@restoreTrashed')->name('specialties.restore');
    Route::get('/datatable/administration/specialties', 'Administration\SpecialtiesController@dataTable');

    /** Rutas Departamentos */

    Route::resource('administration/departments', 'Administration\DepartmentsController');
    Route::post('/administration/departments/{departments}/restore', 'Administration\DepartmentsController@restoreTrashed')->name('departments.restore');
    Route::get('/datatable/administration/departments', 'Administration\DepartmentsController@dataTable');

    /** Rutas Depositos */

    Route::resource('administration/deposits', 'Administration\DepositsController');
    Route::post('/administration/deposits/{deposits}/restore', 'Administration\DepositsController@restoreTrashed')->name('deposits.restore');
    Route::get('/datatable/administration/deposits', 'Administration\DepositsController@dataTable');


    /** Seguros de mala práxis */
    Route::resource('administration/medicalMalpracticeInsurances', 'Administration\MedicalMalpracticeInsurances');
    Route::get('/datatable/administration/medicalMalpracticeInsurances', 'Administration\MedicalMalpracticeInsurances@dataTable');

    /**Especialistas */

    Route::resource('administration/specialists', 'Administration\SpecialistsController');
    Route::get('/datatable/administration/specialists', 'Administration\SpecialistsController@dataTable');


    /**
     * Rangos de prácticas de nomencladores
     */
    Route::resource('administration/nomenclatorRanges', 'Administration\NomenclatorRanges');
    Route::get('/datatable/administration/nomenclatorRanges', 'Administration\NomenclatorRanges@dataTable');

    /**Grupos de Practicas de Nomencladores */

    Route::resource('administration/nomenclatorPracticesGroup', 'Administration\NomenclatorPracticesGroupController');
    Route::get('/datatable/administration/nomenclatorPracticesGroup', 'Administration\NomenclatorPracticesGroupController@dataTable');

    /**
     * Convenios con obras sociales
     */
    /*RESOURCE*/
    Route::resource('billingDepartment/medicalInsuranceAgreements', 'BillingDepartment\MedicalInsuranceAgreements');
    /*DataTable de convenios*/
    Route::get('/datatable/billingDepartment/medicalInsuranceAgreements', 'BillingDepartment\MedicalInsuranceAgreements@dataTable');
    /*Modal para importar archivo de excel con las practicas valorizadas*/
    Route::get('/modals/billingDepartment/practicesAgreementModal/{medicalInsuranceAgreementId}', 'BillingDepartment\MedicalInsuranceAgreements@practicesAgreementModal');
    /*POST para subir archivo de practicas valorizadas*/
    Route::post('/billingDepartment/uploadPracticesAgreement/{medicalInsuranceAgreementId}', 'BillingDepartment\MedicalInsuranceAgreements@uploadPracticesAgreement')->name('medicalInsuranceAgreement.uploadPractices');
    /*DataTable de practicas convenidas*/
    Route::get('/datatable/billingDepartment/medicalInsurancePracticesAgreement/{medicalInsuranceAgreementId}', 'BillingDepartment\MedicalInsuranceAgreements@practicesAgreementDataTable');
    /*Vista de practicas convenidas*/
    Route::get('/billingDepartment/practicesAgreement/{medicalInsuranceAgreementId}', 'BillingDepartment\MedicalInsuranceAgreements@practicesAgreement');

    /*Modal para agregar una pràctica al convenio manualmente*/
    Route::get('/modals/billingDepartment/addPracticeAgreementModal', 'BillingDepartment\MedicalInsuranceAgreements@addPracticeAgreementModal');
    Route::post('/billingDepartment/practicesAgreement/{medicalInsuranceAgreementId}', 'BillingDepartment\MedicalInsuranceAgreements@storePracticeAgreement');
    /*Eliminar una pràctica convenida*/
    Route::post('/billingDepartment/practicesAgreement/{medicalInsuranceAgreementId}/delete', 'BillingDepartment\MedicalInsuranceAgreements@deleteSinglePracticeAgreement');

    /**
     * Categorías de empleados
     */
    Route::resource('/humanResourcesDepartment/employeesCategories', 'HumanResourcesDepartment\EmployeesCategories');
    Route::get('/datatable/humanResourcesDepartment/employeesCategories', 'HumanResourcesDepartment\EmployeesCategories@dataTable');

    /**
     * Pacientes
     */
    Route::resource('administration/patients', 'Administration\PatientsController');
    Route::get('/datatable/administration/patients', 'Administration\PatientsController@dataTable');
    Route::get('/administration/patient/findByDocumentNumber/{documentTypeId}/{documentNumber}', 'Administration\PatientsController@findByDocumentNumber');
    Route::get('/administration/patient/patientMedicalInsurances/{documentTypeId}/{documentNumber}', 'Administration\PatientsController@getPatientMedicalInsurances');

    /**
     * Helpers
     **/
    //Ciudades
    Route::get('/helpers/cities/cityByProvinceId/{provinceId}', 'HelpersController@citiesByProvinceId');
    //Rendiciones abiertas
    Route::get('/helpers/getOpenedCheckingAccounts/{documentTypeId}/{documentNumber}', 'HelpersController@getOpenedCheckingAccounts');

    /**
     * Farmacia
     */
    Route::resource('pharmacy/medicaments', 'Pharmacy\MedicamentsController');
    Route::get('pharmacy/medicamentsUpdate', 'Pharmacy\MedicamentsController@medicamentsUpdate');
    Route::post('pharmacy/medicamentsUpdate', 'Pharmacy\MedicamentsController@uploadAlphaBetaZipFile');
    Route::get('/datatable/pharmacy/medicaments', 'Pharmacy\MedicamentsController@dataTable');
    Route::get('/datatable/pharmacy/medicamentUpdates', 'Pharmacy\MedicamentsController@medicamentUpdatesDataTable');


    /*Rendiciones*/
    Route::get('/billingDepartment/patientCheckingAccounts', 'PatientCheckingAccounts\PatientCheckingAccounts@index');
    Route::get('/datatable/billingDepartment/patientCheckingAccounts', 'PatientCheckingAccounts\PatientCheckingAccounts@dataTable');

    /*Check in de pacientes*/
    /*Ambulatorios*/
    Route::get('/patientCheckingAccounts/ambulatory/checkIn', 'PatientCheckingAccounts\Ambulatory\AmbulatoryPatientsCheckingAccount@checkIn');
    Route::post('/patientCheckingAccounts/ambulatory/checkIn', 'PatientCheckingAccounts\Ambulatory\AmbulatoryPatientsCheckingAccount@storeNewCheckingAccount');
    Route::get('/patientCheckingAccounts/ambulatory/finalStepInformation/{documentTypeId}/{documentNumber}/{medicalInsuranceId}', 'PatientCheckingAccounts\Ambulatory\AmbulatoryPatientsCheckingAccount@getAmbulatoryCheckingAccountFinalStepInformation');

    /*Departamentos Medicos*/


    Route::prefix('medicalDepartments')->group(function() {

        /*Anatomía patológica*/
        Route::get('/pathologicalAnatomy', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@index'); //Indice de muestras
        Route::get('/pathologicalAnatomy/create', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@create'); //Crear muestra
        Route::post('/pathologicalAnatomy/create', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@store'); //Crear muestra
        Route::delete('/pathologicalAnatomy/{id}/delete', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@destroy')->name('pathologicalAnatomyStudy.destroy'); //Eliminar muestra
        Route::get('/datatable/PathologicalAnatomy', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@dataTable'); //Tabla de muestras
        Route::get('/pathologicalAnatomy/printSticker/{studyCode}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@printSticker');

        /*Reporte medico anatomía patológica*/
        Route::get('/datatable/PathologicalAnatomy/medicalReport', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@dataTableForMedicalReport'); //Tabla de muestras para crear reporte medico
        Route::get('/pathologicalAnatomy/medicalReport', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@medicalReport'); //Indice de muestras para crear reporte medico
        Route::get('/pathologicalAnatomy/billingCodesAssignment/{sampleId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@billingCodesAssignment'); //Asignar codigos de facturacion a muestras
        Route::post('/pathologicalAnatomy/billingCodesAssignment/{sampleId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@syncBillingCodesWithSample'); //Asignar codigos de facturacion a muestras
        Route::get('/pathologicalAnatomy/categorizeSample/{sampleId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@categorizeSample'); //Categorizar la muestra
        Route::post('/pathologicalAnatomy/categorizeSample/{sampleId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@syncCategoriesWithSample'); //Categorizar la muestra
        Route::get('/pathologicalAnatomy/{sampleId}/writeMedicalReport', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@writeMedicalReport');
        Route::post('/pathologicalAnatomy/{sampleId}/writeMedicalReport', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@storeMedicalReport');
        Route::get('/pathologicalAnatomy/medicalReportTemplates/getTitle/{titleId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@getMedicalReportTitle');
        Route::get('/pathologicalAnatomy/medicalReportTemplates/getTemplatesByCategory/{templateCategoryId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@getTemplatesByCategory');
        Route::get('/pathologicalAnatomy/medicalReportTemplates/getTemplate/{templateId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@getMedicalReportTemplate');
        Route::get('/pathologicalAnatomy/PapanicolaouTemplateDialog', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@papanicolaouTemplateDialog');
        Route::get('/pathologicalAnatomy/getPapanicolaouTemplateDescription/{templateId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyController@getPapanicolaouTemplateDescription');

        /*Categorías de estudios"*/
        Route::get('/pathologicalAnatomy/categories/levelOne/childCategories/{CategoryId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyLevelOneCategories@childCategories');
        Route::get('/pathologicalAnatomy/categories/levelTwo/childCategories/{CategoryId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyLevelTwoCategories@childCategories');
        Route::get('/pathologicalAnatomy/categories/levelThree/childCategories/{CategoryId}', 'MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyLevelThreeCategories@childCategories');

    });
});
