<?php

use Illuminate\Database\Seeder;

class AmbulatoryPatientCheckingAccountsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'ambulatoryPatientCheckingAccounts.view', 'show_name' => 'Ver Rendiciones Ambulatorias ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'ambulatoryPatientCheckingAccounts.create', 'show_name' => 'Crear Rendiciones Ambulatorias ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'ambulatoryPatientCheckingAccounts.edit', 'show_name' => 'Editar Rendiciones Ambulatorias ', 'guard_name' => 'web']);
    }
}
