<?php

use Illuminate\Database\Seeder;

class AlphaBetaMedicamentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Obtengo el archivo*/
        $file = Storage::get('public/alphabeta/manual.dat');

        /*Separo cada linea*/
        $lines = explode(chr(13).chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $medicaments = $this->splitCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $medicaments = $this->trimWhiteSpaces($medicaments);

        /*Guardo en la base de datos*/
        $this->store($medicaments);
    }

    private function splitCodesAndDescriptions($lines)
    {
        $medicaments = [];
        foreach($lines as $key => $val)
        {
            array_push($medicaments, array(
                'label' => substr($val,0,7),
                'name' => substr($val,7,44),
                'presentation' => substr($val,51,24),
                'laboratory_name' => substr($val,85,16),
                'sell_price' => ((float) substr($val,101,9) / 100),
                'price_since' => substr($val,110,8),
                'laboratory_code' => count(\CloudMed\Models\Pharmacy\PharmaceuticalLaboratory::where('code', intval(substr($val,123,3)))->get()) > 0 ? (int) substr($val,123,3) : 9999,
                'alphabeta_medicament_id' => (int) substr($val,126,5),
                'is_active' => (int) substr($val,131,1),
                'barcode' => substr($val,132,13),
                'units' => (int) substr($val,145,4),
                'medicament_size_code' => count(\CloudMed\Models\Pharmacy\MedicamentSize::where('code', intval(substr($val,149,1)))->get()) > 0 ? (int) substr($val,149,1) : 10,
            ));
        }
        return $medicaments;
    }

    private function trimWhiteSpaces($medicaments)
    {
        foreach($medicaments as $medicament)
        {
            /*Saco los espacios en blanco*/
            $medicament['label'] = trim($medicament['label']);
            $medicament['name'] = trim($medicament['name']);
            $medicament['presentation'] = trim($medicament['presentation']);
            $medicament['laboratory_name'] = trim($medicament['laboratory_name']);
            $medicament['barcode'] = trim($medicament['barcode']);
        }

        return $medicaments;
    }

    private function store($medicaments)
    {
        foreach($medicaments as $medicament)
        {
            /*Guardo en la base de datos*/
            if($medicament['label'] != '') {
                \CloudMed\Models\Pharmacy\Medicament::create(array(
                    'label' => mb_convert_encoding($medicament['label'], 'UTF-8'),
                    'name' => mb_convert_encoding($medicament['name'], 'UTF-8'),
                    'presentation' => mb_convert_encoding($medicament['presentation'], 'UTF-8'),
                    'laboratory_name' => mb_convert_encoding($medicament['laboratory_name'], 'UTF-8'),
                    'sell_price' => $medicament['sell_price'],
                    'price_since' => $medicament['price_since'],
                    'laboratory_code' => $medicament['laboratory_code'],
                    'alphabeta_medicament_id' => $medicament['alphabeta_medicament_id'],
                    'is_active' => $medicament['is_active'],
                    'barcode' => mb_convert_encoding($medicament['barcode'], 'UTF-8'),
                    'units' => $medicament['units'],
                    'medicament_size_code' => $medicament['medicament_size_code'],
                ));
            }
        }
    }
}
