<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_templates.sql';
        DB::unprepared(file_get_contents($path));
    }
}
