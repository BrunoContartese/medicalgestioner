<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyPapanicolausTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_papanicolaus_templates.sql';
        DB::unprepared(file_get_contents($path));
    }
}
