<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'pathologicalAnatomy.view', 'show_name' => 'Ver anatomías patológicas', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'pathologicalAnatomy.create', 'show_name' => 'Crear muestras de A. Patológica', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'pathologicalAnatomy.edit', 'show_name' => 'Editar muestras de A. Patológica', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'pathologicalAnatomy.delete', 'show_name' => 'Eliminar muestras de A. Patológica', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'pathologicalAnatomy.makeMedicalReport', 'show_name' => 'Informar anatomías patológicas', 'guard_name' => 'web']);

    }
}
