<?php

use Illuminate\Database\Seeder;

class EmployeesAgreementsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesAgreements.view', 'show_name' => 'Ver Convenios de empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesAgreements.create', 'show_name' => 'Crear Convenios de empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesAgreements.edit', 'show_name' => 'Editar Convenios de empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesAgreements.delete', 'show_name' => 'Eliminar Convenios de empleados ', 'guard_name' => 'web']);

    }
}
