<?php

use Illuminate\Database\Seeder;

class AlphaBetaPharmacologicalActionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Obtengo el archivo*/
        $file = Storage::get('public/alphabeta/acciofar.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13) . chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $medicaments = $this->splitCodesAndDescriptions($lines);

        /*Guardo en la base de datos*/
        $this->store($medicaments);
    }

    private function splitCodesAndDescriptions($lines)
    {
        $medicaments = [];
        foreach ($lines as $key => $val) {
            array_push($medicaments, array(
                'code' => (int)substr($val, 0, 5),
                'description' => substr($val, 5, 32)
            ));
        }
        return $medicaments;
    }

    private function store($medicaments)
    {
        foreach ($medicaments as $medicament) {
            /*Guardo en la base de datos*/
            if ($medicament['code'] != '') {
                    \CloudMed\Models\Pharmacy\MedicamentPharmacologicalAction::create([
                        'code' => $medicament['code'],
                        'description' => mb_convert_encoding($medicament['description'], 'UTF-8'),
                    ]);
                }
        }
    }
}
