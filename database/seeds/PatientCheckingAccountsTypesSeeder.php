<?php

use Illuminate\Database\Seeder;

class PatientCheckingAccountsTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('patient_checking_accounts_types')->insert([
            'name' => 'AMBULATORIO'
        ]);

        DB::table('patient_checking_accounts_types')->insert([
            'name' => 'INTERNADO'
        ]);

    }
}
