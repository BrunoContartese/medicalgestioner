<?php

use Illuminate\Database\Seeder;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;

class NomenclatorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nomenclator::create([
           'name' => 'NOMENCLADOR NACIONAL'
        ]);

        Nomenclator::create([
            'name' => 'NOMENCLADOR PAMI'
        ]);

    }
}
