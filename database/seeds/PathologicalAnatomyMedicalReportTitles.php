<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyMedicalReportTitles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_medical_report_titles.sql';
        DB::unprepared(file_get_contents($path));
    }
}
