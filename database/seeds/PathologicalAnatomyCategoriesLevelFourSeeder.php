<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyCategoriesLevelFourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_categories_level_four.sql';
        DB::unprepared(file_get_contents($path));
    }
}
