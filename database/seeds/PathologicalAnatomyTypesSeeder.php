<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pathological_anatomy_types')->insert([
            'name' => 'BIOPSIA'
        ]);
        DB::table('pathological_anatomy_types')->insert([
            'name' => 'PAPANICOLAOUS'
        ]);
    }
}
