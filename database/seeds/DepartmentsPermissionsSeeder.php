<?php

use Illuminate\Database\Seeder;

class DepartmentsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'departments.view', 'show_name' => 'Ver Departamentos ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'departments.create', 'show_name' => 'Crear Departamentos ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'departments.edit', 'show_name' => 'Editar Departamentos ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'departments.delete', 'show_name' => 'Eliminar Departamentos ', 'guard_name' => 'web']);

    }
}
