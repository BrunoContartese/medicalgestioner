<?php

use Illuminate\Database\Seeder;

class AlphaBetaMedicamentInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Obtengo el archivo*/
        $file = Storage::get('public/alphabeta/manextra.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13).chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $medicaments = $this->splitCodesAndDescriptions($lines);

        /*Guardo en la base de datos*/
        $this->store($medicaments);
    }

    private function splitCodesAndDescriptions($lines)
    {
        $medicaments = [];
        foreach($lines as $key => $val)
        {
            array_push($medicaments, array(
                'alphabeta_medicament_id' => (int) substr($val,0,5),
                'medicament_drug_code' => (int) substr($val,12,5),
                'medicament_pharmacological_action_code' => (int) substr($val,7,5),
                'medicament_administration_line_code' => (int) substr($val,48,5),

            ));
        }
        return $medicaments;
    }

    private function store($medicaments)
    {
        foreach($medicaments as $medicament)
        {
            /*Guardo en la base de datos*/
            if($medicament['alphabeta_medicament_id'] != '') {
                if(count(\CloudMed\Models\Pharmacy\Medicament::where('alphabeta_medicament_id', $medicament['alphabeta_medicament_id'])->get()) > 0) {
                    \CloudMed\Models\Pharmacy\Medicament::where('alphabeta_medicament_id', $medicament['alphabeta_medicament_id'])->update([
                        'medicament_pharmacological_action_code' => $medicament['medicament_pharmacological_action_code'],
                        'medicament_drug_code' => $medicament['medicament_drug_code'],
                        'medicament_administration_line_code' => $medicament['medicament_administration_line_code'],
                        'medicament_unit_type_code' => $medicament['medicament_unit_type_code'],
                    ]);
                }

            }
        }
    }
}
