<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyCategoriesLevelOneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_categories_level_one.sql';
        DB::unprepared(file_get_contents($path));
    }
}
