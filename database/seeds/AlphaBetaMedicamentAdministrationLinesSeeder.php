<?php

use Illuminate\Database\Seeder;

class AlphaBetaMedicamentAdministrationLinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Obtengo el archivo*/
        $file = Storage::get('public/alphabeta/vias.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13) . chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $medicaments = $this->splitCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $medicaments = $this->trimWhiteSpaces($medicaments);

        /*Guardo en la base de datos*/
        $this->store($medicaments);
    }

    private function splitCodesAndDescriptions($lines)
    {
        $medicaments = [];

        foreach ($lines as $key => $val) {
            array_push($medicaments, array(
                'code' => (int) substr($val, 0, 5),
                'description' => substr($val, 5, 50)
            ));
        }
        return $medicaments;
    }

    private function trimWhiteSpaces($medicaments)
    {
        foreach($medicaments as $medicament)
        {
            /*Saco los espacios en blanco*/
            $medicament['description'] = trim($medicament['description'], " \t\n\r\0\x0B" );
        }

        return $medicaments;
    }

    private function store($medicaments)
    {
        foreach ($medicaments as $medicament) {
            /*Guardo en la base de datos*/
            if ($medicament['code'] != '') {
                \CloudMed\Models\Pharmacy\MedicamentAdministrationLine::create([
                        'code' => $medicament['code'],
                        'description' => mb_convert_encoding($medicament['description'], 'UTF-8'),
                    ]);
                }
        }
    }
}
