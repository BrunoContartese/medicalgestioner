<?php

use Illuminate\Database\Seeder;

class SystemPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'dashboard.view', 'show_name' => 'Ver página principal ', 'guard_name' => 'web']);
    }
}
