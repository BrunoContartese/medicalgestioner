<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * First i seed all the modules permissions
         */
        $this->call(UsersPermissionsSeeder::class);
        $this->call(SystemPermissionsSeeder::class);
        $this->call(MedicalInsurancesPermissionsSeeder::class);
        $this->call(MedicalInsurancesManagersPermissionsSeeder::class);
        $this->call(NomenclatorsPermissionsSeeder::class);
        $this->call(NomenclatorRangesPermissionsSeeder::class);
        $this->call(RolesPermissionsSeeder::class);
        $this->call(SpecialtiesPermissionsSeeder::class);
        $this->call(DepartmentsPermissionsSeeder::class);
        $this->call(DepositsPermissionsSeeder::class);
        $this->call(MedicalInsuranceAgreementsPermissionsSeeder::class);
        $this->call(SpecialistsPermissionsSeeder::class);
        $this->call(NomenclatorPracticesGroupPermissionsSeeder::class);
        $this->call(MedicalMalpracticeInsurancesPermissionsSeeder::class);
        $this->call(PatientsPermissionsSeeder::class);
        $this->call(PharmacyPermissionsSeeder::class);
        $this->call(PatientCheckingAccountsPermissionsSeeder::class);
        $this->call(AmbulatoryPatientCheckingAccountsPermissionsSeeder::class);
        $this->call(ToolsMenuPermissionsSeeder::class);
        $this->call(PathologicalAnatomyPermissionsSeeder::class);


        /**
         * Then necessary data
         */
        $this->call(ProvincesTableSeeder::class); //Provincias

        $this->call(GendersTableSeeder::class); //Géneros
        $this->call(DocumentTypesSeeder::class); //Tipos de documento
        $this->call(PharmaceuticalLaboratoriesTableSeeder::class); //Laboratorios farmaceuticos
        $this->call(MedicamentCategoriesSeeder::class); //Categorias de insumos de farmacia
        $this->call(MedicamentUpdateStatusesSeeder::class); //Estados de actualizacion de medicamentos*/
        $this->call(PatientCheckingAccountsTypesSeeder::class); //Tipos de rendiciones
        $this->call(PatientCheckingAccountsStatusesSeeder::class); //Estado de rendiciones*/

        /**
         * Users
         */
        $this->call(UsersTableSeeder::class);
        /**
         * Nomenclators
         */
        $this->call(NomenclatorsSeeder::class);
        /**
         * Nomenclator practices
         */
        $this->call(NomenclatorPracticesTableSeeder::class);

        $this->call(CitiesTableSeeder::class); //Ciudades*/
        $this->call(PathologicalAnatomyCategoriesLevelOneSeeder::class); //Categories de anatomia patological nivel 1
        $this->call(PathologicalAnatomyCategoriesLevelTwoSeeder::class); //Categories de anatomia patological nivel 2
        $this->call(PathologicalAnatomyCategoriesLevelThreeSeeder::class); //Categories de anatomia patological nivel 3
        $this->call(PathologicalAnatomyCategoriesLevelFourSeeder::class); //Categories de anatomia patological nivel 4
        $this->call(PathologicalAnatomyTemplatesCategoriesSeeder::class); //Categorias de plantillas de anatomia patologica
        $this->call(PathologicalAnatomyTemplatesSeeder::class); //Categorias de plantillas de anatomia patologica
        $this->call(PathologicalAnatomyPapanicolausTemplatesSeeder::class); //Plantillas de informe de papanicolaus*/
        $this->call(PathologicalAnatomyMedicalReportTitles::class); //Plantillas de informe de papanicolaus*/

    }
}
