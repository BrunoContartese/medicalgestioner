<?php

use Illuminate\Database\Seeder;

class AlphaBetaDrugsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Obtengo el archivo*/
        $file = Storage::get('public/alphabeta/monodro.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13).chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $drugs = $this->splitCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $drugs = $this->trimWhiteSpaces($drugs);

        /*Guardo en la base de datos*/
        $this->store($drugs);
    }

    private function splitCodesAndDescriptions($lines)
    {
        $drugs = [];
        foreach($lines as $key => $val)
        {
            array_push($drugs, array(
                'code' => substr($val,0,5),
                'description' => substr($val,5,32)
            ));
        }
        return $drugs;
    }

    private function trimWhiteSpaces($drugs)
    {
        foreach($drugs as $drug)
        {
            /*Saco los espacios en blanco*/
            $drug['description'] = trim($drug['description'], " \t\n\r\0\x0B" );
        }

        return $drugs;
    }

    private function store($drugs)
    {
        foreach($drugs as $drug)
        {
            /*Guardo en la base de datos*/
            if($drug['code'] != '') {
                \CloudMed\Models\Pharmacy\MedicamentDrug::create(array(
                    'code' => $drug['code'],
                    'description' => mb_convert_encoding($drug['description'], 'UTF-8')
                ));
            }
        }
    }
}
