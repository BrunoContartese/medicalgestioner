<?php

use Illuminate\Database\Seeder;

class EmployeesSettlementMethodsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesSettlementMethods.view', 'show_name' => 'Ver Metodos de liquidación de empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesSettlementMethods.create', 'show_name' => 'Crear Metodos de liquidación de empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesSettlementMethods.edit', 'show_name' => 'Editar Metodos de liquidación de empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesSettlementMethods.delete', 'show_name' => 'Eliminar Metodos de liquidación de empleados ', 'guard_name' => 'web']);

    }
}
