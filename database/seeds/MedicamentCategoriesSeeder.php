<?php

use Illuminate\Database\Seeder;

class MedicamentCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \CloudMed\Models\Pharmacy\MedicamentCategory::create([
            'description' => 'Medicamentos'
        ]);
    }
}
