<?php

use Illuminate\Database\Seeder;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'roles.view', 'show_name' => 'Ver Perfiles ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'roles.create', 'show_name' => 'Crear Perfiles ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'roles.edit', 'show_name' => 'Editar Perfiles ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'roles.delete', 'show_name' => 'Eliminar Perfiles ', 'guard_name' => 'web']);
    }
}
