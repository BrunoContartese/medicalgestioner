<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PharmacyPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'medicaments.view', 'show_name' => 'Ver Medicamentos ', 'guard_name' => 'web']);
        Permission::create(['name' => 'medicaments.create', 'show_name' => 'Crear Medicamentos ', 'guard_name' => 'web']);
        Permission::create(['name' => 'medicaments.edit', 'show_name' => 'Editar Medicamentos ', 'guard_name' => 'web']);
        Permission::create(['name' => 'medicaments.update', 'show_name' => 'Actualizar Medicamentos ', 'guard_name' => 'web']);
    }
}
