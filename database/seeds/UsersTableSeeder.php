<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \CloudMed\User::create([
            'name' => 'Contartese Bruno Andrés',
            'username' => 'bcontartese',
            'email' => 'bcontartese@fnsr.com.ar',
            'password' => bcrypt('36604064dd')
        ]);

        $role = \Spatie\Permission\Models\Role::create(['name' => 'Administrador']);
        $role->syncPermissions(\Spatie\Permission\Models\Permission::all());

        $user->syncRoles("Administrador");

        $user = \CloudMed\User::create([
            'name' => 'Martinet Mónica',
            'username' => 'mmartinet',
            'email' => 'mmartinet@fnsr.com.ar',
            'password' => bcrypt('12345678')
        ]);

        $user->syncRoles("Administrador");


        $user = \CloudMed\User::create([
            'name' => 'Marco Bertolaccini',
            'username' => 'mbertolaccini',
            'email' => '121marco@gmail.com',
            'password' => bcrypt('121marco')
        ]);

        $user->syncRoles("Administrador");


    }
}
