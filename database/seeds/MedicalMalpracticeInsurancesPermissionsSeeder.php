<?php

use Illuminate\Database\Seeder;

class MedicalMalpracticeInsurancesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalMalpracticeInsurances.view', 'show_name' => 'Ver Seguros de mala práxis ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalMalpracticeInsurances.create', 'show_name' => 'Crear Seguros de mala práxis ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalMalpracticeInsurances.edit', 'show_name' => 'Editar Seguros de mala práxis ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalMalpracticeInsurances.delete', 'show_name' => 'Eliminar Seguros de mala práxis ', 'guard_name' => 'web']);
    }
}
