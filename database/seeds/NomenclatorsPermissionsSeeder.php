<?php

use Illuminate\Database\Seeder;

class NomenclatorsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclators.view', 'show_name' => 'Ver Nomencladores ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclators.create', 'show_name' => 'Crear Nomencladores ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclators.edit', 'show_name' => 'Editar Nomencladores ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclators.delete', 'show_name' => 'Eliminar Nomencladores ', 'guard_name' => 'web']);
    }
}
