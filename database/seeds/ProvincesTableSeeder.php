<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'name' => 'Buenos Aires',
            ),
            1 => 
            array (
                'name' => 'Capital Federal',
            ),
            2 => 
            array (
                'name' => 'Catamarca',
            ),
            3 => 
            array (
                'name' => 'Chaco',
            ),
            4 => 
            array (
                'name' => 'Chubut',
            ),
            5 => 
            array (
                'name' => 'Córdoba',
            ),
            6 => 
            array (
                'name' => 'Corrientes',
            ),
            7 => 
            array (
                'name' => 'Entre Ríos',
            ),
            8 => 
            array (
                'name' => 'Formosa',
            ),
            9 => 
            array (
                'name' => 'Jujuy',
            ),
            10 => 
            array (
                'name' => 'La Pampa',
            ),
            11 => 
            array (
                'name' => 'La Rioja',
            ),
            12 => 
            array (
                'name' => 'Mendoza',
            ),
            13 => 
            array (
                'name' => 'Misiones',
            ),
            14 => 
            array (
                'name' => 'Neuquén',
            ),
            15 => 
            array (
                'name' => 'Río Negro',
            ),
            16 => 
            array (
                'name' => 'Salta',
            ),
            17 => 
            array (
                'name' => 'San Juan',
            ),
            18 => 
            array (
                'name' => 'San Luis',
            ),
            19 => 
            array (
                'name' => 'Santa Cruz',
            ),
            20 => 
            array (
                'name' => 'Santa Fé',
            ),
            21 => 
            array (
                'name' => 'Santiago del Estero',
            ),
            22 => 
            array (
                'name' => 'Tierra del Fuego',
            ),
            23 => 
            array (
                'name' => 'Tucumán',
            ),
        ));
        
        
    }
}