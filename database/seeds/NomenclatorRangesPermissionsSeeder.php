<?php

use Illuminate\Database\Seeder;

class NomenclatorRangesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorRanges.view', 'show_name' => 'Ver Rangos de Nomencladores ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorRanges.create', 'show_name' => 'Crear Rangos de Nomencladores ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorRanges.edit', 'show_name' => 'Editar Rangos de Nomencladores ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorRanges.delete', 'show_name' => 'Eliminar Rangos de Nomencladores ', 'guard_name' => 'web']);
    }
}
