<?php

use Illuminate\Database\Seeder;

class MedicamentUpdateStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \CloudMed\Models\Pharmacy\MedicamentUpdateStatus::create([
            'name' => 'Exitosa'
        ]);

        \CloudMed\Models\Pharmacy\MedicamentUpdateStatus::create([
            'name' => 'Procesando'
        ]);

        \CloudMed\Models\Pharmacy\MedicamentUpdateStatus::create([
            'name' => 'Fallida'
        ]);
    }
}
