<?php

use Illuminate\Database\Seeder;

class PatientCheckingAccountsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'PatientCheckingAccounts.view', 'show_name' => 'Ver Rendiciones ', 'guard_name' => 'web']);
    }
}
