<?php

use Illuminate\Database\Seeder;

class EmployeesCategoriesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesCategories.view', 'show_name' => 'Ver Categorías de Empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesCategories.create', 'show_name' => 'Crear Categorías de Empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesCategories.edit', 'show_name' => 'Editar Categorías de Empleados ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'employeesCategories.delete', 'show_name' => 'Eliminar Categorías de Empleados ', 'guard_name' => 'web']);
    }
}
