<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PatientsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'patients.view', 'show_name' => 'Ver Pacientes ', 'guard_name' => 'web']);
        Permission::create(['name' => 'patients.create', 'show_name' => 'Crear Pacientes ', 'guard_name' => 'web']);
        Permission::create(['name' => 'patients.edit', 'show_name' => 'Editar Pacientes ', 'guard_name' => 'web']);
        Permission::create(['name' => 'patients.delete', 'show_name' => 'Eliminar Pacientes ', 'guard_name' => 'web']);

    }
}
