<?php

use Illuminate\Database\Seeder;

class DocumentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_types')->insert([
            'name' => 'DNI',
            'code' => 96
        ]);

        DB::table('document_types')->insert([
            'name' => 'LC',
            'code' => 90
        ]);

        DB::table('document_types')->insert([
            'name' => 'LE',
            'code' => 89
        ]);

        DB::table('document_types')->insert([
            'name' => 'PASAPORTE',
            'code' => 94
        ]);

        DB::table('document_types')->insert([
            'name' => 'CI EXTRANJERA',
            'code' => 91
        ]);

        DB::table('document_types')->insert([
            'name' => 'ACTA DE NACIMIENTO',
            'code' => 93
        ]);

    }
}
