<?php

use Illuminate\Database\Seeder;

class PatientCheckingAccountsStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patient_checking_accounts_statuses')->insert([
            'name' => 'ABIERTA'
        ]);

        DB::table('patient_checking_accounts_statuses')->insert([
            'name' => 'CERRADA'
        ]);

        DB::table('patient_checking_accounts_statuses')->insert([
            'name' => 'FACTURADA'
        ]);
    }
}
