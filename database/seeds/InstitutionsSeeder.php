<?php

use Illuminate\Database\Seeder;

class InstitutionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutions')->insert([
           'name' => 'Fundación Ntra. Sra. del Rosario'
        ]);
    }
}
