<?php

use Illuminate\Database\Seeder;

class SpecialistsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'specialists.view', 'show_name' => 'Ver Especialistas ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'specialists.create', 'show_name' => 'Crear Especialistas ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'specialists.edit', 'show_name' => 'Editar Especialistas ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'specialists.delete', 'show_name' => 'Eliminar Especialistas ', 'guard_name' => 'web']);

    }
}
