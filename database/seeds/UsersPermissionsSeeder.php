<?php

use Illuminate\Database\Seeder;

class UsersPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'users.view', 'show_name' => 'Ver usuarios', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'users.create', 'show_name' => 'Crear usuarios', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'users.edit', 'show_name' => 'Editar usuarios', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'users.delete', 'show_name' => 'Eliminar usuarios', 'guard_name' => 'web']);
    }
}
