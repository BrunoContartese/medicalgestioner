<?php

use Illuminate\Database\Seeder;

class NomenclatorPracticesGroupPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorPracticesGroup.view', 'show_name' => 'Ver Grupos de Practicas ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorPracticesGroup.create', 'show_name' => 'Crear Grupos de Practicas ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorPracticesGroup.edit', 'show_name' => 'Editar Grupos de Practicas ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'nomenclatorPracticesGroup.delete', 'show_name' => 'Eliminar Grupos de Practicas ', 'guard_name' => 'web']);

    }
}
