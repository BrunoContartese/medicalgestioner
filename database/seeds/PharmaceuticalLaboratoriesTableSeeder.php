<?php

use Illuminate\Database\Seeder;
use CloudMed\Models\Pharmacy\PharmaceuticalLaboratory;

class PharmaceuticalLaboratoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('pharmaceutical_laboratories')->insertOrIgnore(array (
            0 => 
            array (
                'code' => 1,
                'name' => 'Abbott',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'code' => 2,
                'name' => 'Alcon',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'code' => 3,
                'name' => 'Allergan-Loa',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'code' => 4,
                'name' => 'Amhof',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'code' => 5,
                'name' => 'Andrómaco',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'code' => 6,
                'name' => 'Ariston',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'code' => 7,
                'name' => 'Asofarma',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'code' => 8,
                'name' => 'AstraZeneca',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'code' => 9,
                'name' => 'Bagó',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'code' => 10,
                'name' => 'Baliarda',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'code' => 11,
                'name' => 'Bausch & Lomb Ar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'code' => 12,
            'name' => 'Bayer (BSP)',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'code' => 13,
                'name' => 'Bayer Consumer',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'code' => 14,
                'name' => 'Beta',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'code' => 15,
                'name' => 'Biotoscana',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'code' => 16,
                'name' => 'Boehringer Ingel',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'code' => 17,
                'name' => 'Bristol',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'code' => 18,
                'name' => 'Buxton',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'code' => 19,
                'name' => 'CAIF',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'code' => 20,
                'name' => 'Casasco',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'code' => 21,
                'name' => 'Craveri',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'code' => 22,
                'name' => 'Dallas',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'code' => 23,
                'name' => 'Dupomar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'code' => 24,
                'name' => 'Elea',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'code' => 25,
                'name' => 'Eli Lilly',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'code' => 26,
                'name' => 'Elisium',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'code' => 27,
                'name' => 'Eurofarma',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'code' => 28,
                'name' => 'Eurolab',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'code' => 29,
                'name' => 'Ferring',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'code' => 30,
                'name' => 'Finadiet',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'code' => 31,
                'name' => 'Gador',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'code' => 32,
                'name' => 'Galderma',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'code' => 33,
                'name' => 'Genomma Lab.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'code' => 34,
                'name' => 'GlaxoSmithKline',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'code' => 35,
                'name' => 'Gobbi',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'code' => 36,
                'name' => 'Gramón-Millet',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'code' => 37,
                'name' => 'HLB Pharma',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'code' => 38,
                'name' => 'Investi',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'code' => 39,
                'name' => 'Ivax Arg.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'code' => 40,
                'name' => 'Janssen-Cilag',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'code' => 41,
                'name' => 'Johnson & Johnso',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'code' => 42,
                'name' => 'Laboratorios Ber',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'code' => 43,
                'name' => 'Lazar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'code' => 44,
                'name' => 'LKM Onco/Especia',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'code' => 45,
                'name' => 'LKM Pharma',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'code' => 46,
                'name' => 'Lundbeck',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'code' => 47,
                'name' => 'Mead Johnson',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'code' => 48,
                'name' => 'Mead Johnson Nut',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'code' => 49,
                'name' => 'Menarini',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'code' => 50,
                'name' => 'Merck Química',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'code' => 51,
                'name' => 'Merck Serono',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'code' => 52,
                'name' => 'Merck Sharp & Do',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'code' => 53,
                'name' => 'Montpellier',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'code' => 54,
                'name' => 'Nova Argentia',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'code' => 55,
                'name' => 'Novartis',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'code' => 56,
                'name' => 'Novartis Consume',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'code' => 57,
                'name' => 'Novartis Vacunas',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'code' => 58,
                'name' => 'Novo Nordisk',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'code' => 59,
                'name' => 'Nutricia-Bagó',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'code' => 60,
                'name' => 'Omega',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'code' => 61,
                'name' => 'Penn Pharmaceuti',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'code' => 62,
                'name' => 'Pfizer',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'code' => 63,
                'name' => 'Phoenix',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'code' => 64,
                'name' => 'Pierre Fabre Der',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'code' => 65,
                'name' => 'Pierre Fabre Méd',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'code' => 66,
                'name' => 'Poen',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'code' => 67,
                'name' => 'Purissimus',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'code' => 68,
                'name' => 'Raffo',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'code' => 69,
                'name' => 'Raymos',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'code' => 70,
                'name' => 'Richmond',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'code' => 71,
                'name' => 'Roche',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'code' => 72,
                'name' => 'Roemmers',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'code' => 73,
                'name' => 'Roux Ocefa',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'code' => 74,
                'name' => 'Sandoz',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'code' => 75,
                'name' => 'Sanofi Pasteur',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'code' => 76,
                'name' => 'Sanofi-Aventis',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'code' => 77,
                'name' => 'Sanofi-Aventis O',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'code' => 78,
                'name' => 'Schering-Plough',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'code' => 79,
                'name' => 'Servier',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'code' => 80,
                'name' => 'Sidus',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'code' => 81,
                'name' => 'Spedrog Caillon',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'code' => 82,
                'name' => 'Stiefel Arg.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'code' => 83,
                'name' => 'Takeda',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'code' => 84,
                'name' => 'Temis-Lostaló',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'code' => 85,
                'name' => 'Trb-Pharma',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'code' => 86,
                'name' => 'Wyeth Div.VACUNA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 =>
                array (
                    'code' => 9999,
                    'name' => 'Laboratorio a cargar.',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));
        
        
    }
}