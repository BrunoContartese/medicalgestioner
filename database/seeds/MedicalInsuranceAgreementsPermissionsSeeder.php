<?php

use Illuminate\Database\Seeder;

class MedicalInsuranceAgreementsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsuranceAgreements.view', 'show_name' => 'Ver convenios con o.s ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsuranceAgreements.create', 'show_name' => 'Crear convenios con o.s ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsuranceAgreements.edit', 'show_name' => 'Editar convenios con o.s ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsuranceAgreements.delete', 'show_name' => 'Eliminar convenios con o.s ', 'guard_name' => 'web']);
    }
}
