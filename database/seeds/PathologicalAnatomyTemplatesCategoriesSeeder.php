<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyTemplatesCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_templates_categories.sql';
        DB::unprepared(file_get_contents($path));
    }
}
