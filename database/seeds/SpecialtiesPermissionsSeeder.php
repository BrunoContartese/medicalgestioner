<?php

use Illuminate\Database\Seeder;

class SpecialtiesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'specialties.view', 'show_name' => 'Ver Especialidades ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'specialties.create', 'show_name' => 'Crear Especialidades ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'specialties.edit', 'show_name' => 'Editar Especialidades ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'specialties.delete', 'show_name' => 'Eliminar Especialidades ', 'guard_name' => 'web']);

    }
}
