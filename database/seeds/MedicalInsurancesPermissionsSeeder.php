<?php

use Illuminate\Database\Seeder;

class MedicalInsurancesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurances.view', 'show_name' => 'Ver obras sociales ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurances.create', 'show_name' => 'Crear obras sociales ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurances.edit', 'show_name' => 'Editar obras sociales ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurances.delete', 'show_name' => 'Eliminar obras sociales ', 'guard_name' => 'web']);
    }
}
