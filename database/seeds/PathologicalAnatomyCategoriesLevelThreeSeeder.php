<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyCategoriesLevelThreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_categories_level_three.sql';
        DB::unprepared(file_get_contents($path));
    }
}
