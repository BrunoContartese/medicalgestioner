<?php

use Illuminate\Database\Seeder;

class PathologicalAnatomyCategoriesLevelTwoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/SqlData/pathological_anatomy_categories_level_two.sql';
        DB::unprepared(file_get_contents($path));
    }
}
