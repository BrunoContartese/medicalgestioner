<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ToolsMenuPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'toolsMenu.view', 'show_name' => 'Ver herramientas de soporte ', 'guard_name' => 'web']);
    }
}
