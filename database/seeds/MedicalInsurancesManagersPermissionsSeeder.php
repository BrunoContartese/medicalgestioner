<?php

use Illuminate\Database\Seeder;

class MedicalInsurancesManagersPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurancesManagers.view', 'show_name' => 'Ver gerenciadoras de o.s ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurancesManagers.create', 'show_name' => 'Crear gerenciadoras de o.s ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurancesManagers.edit', 'show_name' => 'Editar gerenciadoras de o.s ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'medicalInsurancesManagers.delete', 'show_name' => 'Eliminar gerenciadoras de o.s ', 'guard_name' => 'web']);
    }
}
