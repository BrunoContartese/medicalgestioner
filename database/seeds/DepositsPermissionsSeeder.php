<?php

use Illuminate\Database\Seeder;

class DepositsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create(['name' => 'deposits.view', 'show_name' => 'Ver Depositos ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'deposits.create', 'show_name' => 'Crear Depositos ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'deposits.edit', 'show_name' => 'Editar Depositos ', 'guard_name' => 'web']);
        \Spatie\Permission\Models\Permission::create(['name' => 'deposits.delete', 'show_name' => 'Eliminar Depositos ', 'guard_name' => 'web']);

    }
}
