<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientCheckingAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_checking_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('patient_checking_account_type_id');
            $table->foreign("patient_checking_account_type_id", 'checking_account_type_id_foreign')->references('id')->on('patient_checking_accounts_types');

            $table->unsignedBigInteger('patient_id');
            $table->foreign("patient_id")->references('id')->on('patients');

            $table->unsignedBigInteger("checking_account_number")->nullable();

            $table->unsignedBigInteger('patient_checking_account_status_id')->default(1);
            $table->foreign("patient_checking_account_status_id", 'checking_account_status_id_foreign')->references('id')->on('patient_checking_accounts_statuses');

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_checking_accounts');
    }
}
