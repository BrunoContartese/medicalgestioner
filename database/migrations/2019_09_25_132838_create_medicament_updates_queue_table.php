<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicamentUpdatesQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicament_updates_queue', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger(("user_id"));
            $table->foreign("user_id")->references('id')->on('users');
            $table->unsignedBigInteger("medicament_update_status_id");
            $table->foreign("medicament_update_status_id")->references('id')->on('medicament_update_statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicament_updates_queue');
    }
}
