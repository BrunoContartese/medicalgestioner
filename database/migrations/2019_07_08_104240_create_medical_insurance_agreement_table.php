<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalInsuranceAgreementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_insurance_agreement', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description');

            $table->unsignedBigInteger('nomenclator_id');
            $table->foreign('nomenclator_id')->references('id')->on('nomenclators');

            $table->unsignedBigInteger('medical_insurance_id');
            $table->foreign('medical_insurance_id')->references('id')->on('medical_insurances');

            $table->date('valid_from');

            $table->date('valid_until')->nullable();

            $table->boolean('without_end_date')->default(0);

            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_insurance_agreement');
    }
}
