<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathologicalAnatomyStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathological_anatomy_studies', function (Blueprint $table) {
            $table->bigIncrements('id');

            /*Codigo único de muestra*/
            $table->string('code')->nullable();

            /*Tipo de muestra*/
            $table->unsignedBigInteger('pathological_anatomy_type_id');
            $table->foreign('pathological_anatomy_type_id', 'pathological_anatomy_type_id_foreign')->references('id')->on('pathological_anatomy_types');

            /*Especialista derivante*/
            $table->unsignedBigInteger('medical_specialist_id');
            $table->foreign('medical_specialist_id', 'medical_specialist_id_foreign')->references('id')->on('medical_specialists');

            /*Rendición*/
            $table->unsignedBigInteger('patient_checking_account_id');
            $table->foreign('patient_checking_account_id', 'patient_checking_account_id_foreign')->references('id')->on('patient_checking_accounts');

            /*Institución derivante*/
            $table->unsignedBigInteger('institution_id')->default(1);
            $table->foreign('institution_id', 'institution_id_foreign')->references('id')->on('institutions');

            /*Departamento medico*/
            $table->unsignedBigInteger('medical_department_id');
            $table->foreign('medical_department_id', 'medical_department_id_foreign')->references('id')->on('medical_departments');

            /*Fecha de recepción de muestra*/
            $table->dateTime('received_at')->nullable();

            /*Categoría de anatomia patologica nivel 1*/
            $table->unsignedBigInteger('pathological_anatomy_category_level_one_id')->nullable();
            $table->foreign('pathological_anatomy_category_level_one_id', 'pathological_anatomy_category_level_one_id_foreign')->references('id')->on('pathological_anatomy_categories_level_one');

            /*Categoría de anatomia patologica nivel 2*/
            $table->unsignedBigInteger('pathological_anatomy_category_level_two_id')->nullable();
            $table->foreign('pathological_anatomy_category_level_two_id', 'pathological_anatomy_category_level_two_id_foreign')->references('id')->on('pathological_anatomy_categories_level_two');

            /*Categoría de anatomia patologica nivel 3*/
            $table->unsignedBigInteger('pathological_anatomy_category_level_three_id')->nullable();
            $table->foreign('pathological_anatomy_category_level_three_id', 'pathological_anatomy_category_level_three_id_foreign')->references('id')->on('pathological_anatomy_categories_level_three');

            /*Categoría de anatomia patologica nivel 4*/
            $table->unsignedBigInteger('pathological_anatomy_category_level_four_id')->nullable();
            $table->foreign('pathological_anatomy_category_level_four_id', 'pathological_anatomy_category_level_four_id_foreign')->references('id')->on('pathological_anatomy_categories_level_four');

            $table->string('comments')->nullable();

            $table->unsignedInteger('created_by')->nullable();

            $table->unsignedInteger('updated_by')->nullable();

            $table->unsignedInteger('deleted_by')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathological_anatomy_studies');
    }
}
