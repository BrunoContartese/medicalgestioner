<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathologicalAnatomyBillingCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathological_anatomy_billing_codes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('pathological_anatomy_study_id');
            $table->foreign('pathological_anatomy_study_id', 'pathological_anatomy_billing_codes_study_id_foreign')->references('id')->on('pathological_anatomy_studies');

            $table->unsignedBigInteger('nomenclator_practice_id');
            $table->foreign('nomenclator_practice_id', 'nomenclator_practice_id_foreign')->references('id')->on('nomenclator_practices');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathological_anatomy_billing_codes');
    }
}
