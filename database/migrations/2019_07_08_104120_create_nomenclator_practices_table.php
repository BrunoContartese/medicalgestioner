<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNomenclatorPracticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomenclator_practices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_module')->default(0);
            $table->unsignedBigInteger('code');
            $table->text('description');
            $table->float('expense_units')->default(0);
            $table->float('honorary_units')->default(0);
            $table->float('helper_honorary_units')->default(0);
            $table->float('anesthesist_honorary_units')->default(0);
            $table->unsignedBigInteger('nomenclator_id')->nullable();
            $table->foreign('nomenclator_id')->references('id')->on('nomenclators');
            $table->integer('helpers_quantity')->nullable();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nomenclator_practices');
    }
}
