<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalInsurancesManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_insurances_managers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('medical_insurance_id');
            $table->foreign('medical_insurance_id')->references('id')->on('medical_insurances');
            $table->string('name');
            $table->string('cuit');
            $table->string('billing_address')->nullable();
            $table->text('contact')->nullable();
            $table->boolean('perceives_gross_income')->nullable()->default(false);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_insurances_managers');
    }
}
