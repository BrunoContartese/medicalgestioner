<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathologicalAnatomyCategoriesLevelOneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathological_anatomy_categories_level_one', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('code');

            $table->string('name');

            $table->timestamps();

            $table->softDeletes();

            $table->unsignedInteger('created_by')->nullable()->default(null);

            $table->unsignedInteger('updated_by')->nullable()->default(null);

            $table->unsignedInteger('deleted_by')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathological_anatomy_categories_level_one');
    }
}
