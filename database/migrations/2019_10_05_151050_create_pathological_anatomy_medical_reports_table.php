<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathologicalAnatomyMedicalReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathological_anatomy_medical_reports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('pathological_anatomy_study_id');
            $table->foreign('pathological_anatomy_study_id', 'pathological_anatomy_study_id_foreign')->references('id')->on('pathological_anatomy_studies');

            $table->text('medical_report');

            $table->unsignedInteger('created_by')->nullable();

            $table->unsignedInteger('updated_by')->nullable();

            $table->unsignedInteger('deleted_by')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathological_anatomy_medical_reports');
    }
}
