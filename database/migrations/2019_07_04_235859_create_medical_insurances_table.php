<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_insurances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('cuit');
            $table->string('billing_address')->nullable();
            $table->text('contact')->nullable();
            $table->boolean('perceives_gross_income')->nullable()->default(false);
            $table->boolean('apply_discount_on_medications')->nullable()->default(false);
            $table->float('medication_discount_percentage')->nullable();
            $table->text('news')->nullable();
            $table->boolean("has_webservice")->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_insurances');
    }
}
