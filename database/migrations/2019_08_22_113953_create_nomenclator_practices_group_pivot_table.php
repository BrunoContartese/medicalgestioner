<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNomenclatorPracticesGroupPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomenclator_practices_group_pivot', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('nomenclator_practice_id');
            $table->foreign('nomenclator_practice_id', 'np_practice_id')->references('id')->on('nomenclator_practices');
            $table->unsignedBigInteger('nomenclator_practice_group_id');
            $table->foreign('nomenclator_practice_group_id', 'np_group_id')->references('id')->on('nomenclator_practices_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nomenclator_practices_group_pivot');
    }
}
