<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathologicalAnatomyTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathological_anatomy_templates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('template_category_id');
            $table->foreign('template_category_id')->references('id')->on('pathological_anatomy_templates_categories');

            $table->string('code');

            $table->string('description',1000);

            $table->softDeletes();

            $table->timestamps();

            $table->unsignedInteger('created_by')->nullable()->default(null);

            $table->unsignedInteger('updated_by')->nullable()->default(null);

            $table->unsignedInteger('deleted_by')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathological_anatomy_templates');
    }
}
