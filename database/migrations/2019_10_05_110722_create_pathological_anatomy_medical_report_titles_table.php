<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathologicalAnatomyMedicalReportTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathological_anatomy_medical_report_titles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');

            $table->unsignedInteger('created_by')->nullable()->default(null);

            $table->unsignedInteger('updated_by')->nullable()->default(null);

            $table->unsignedInteger('deleted_by')->nullable()->default(null);

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathological_anatomy_medical_report_titles');
    }
}
