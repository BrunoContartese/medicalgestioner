<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            /*Datos obligatorios*/
            $table->string('name');
            $table->string('tango_code');
            $table->unsignedBigInteger('employee_agreement_id');
            $table->foreign('employee_agreement_id')->references('id')->on('employees_agreements');
            $table->unsignedBigInteger('employee_settlement_method_id');
            $table->foreign('employee_settlement_method_id')->references('id')->on('employees_settlement_methods');
            /*Datos opcionales*/
            $table->float('suggested_salary_amount')->nullable();
            $table->float('minimum_salary_amount')->nullable();
            $table->float('maximum_salary_amount')->nullable();
            $table->float('extra_hour_amount')->nullable();
            $table->float('salary_matrix_amount')->nullable();
            $table->integer('days_per_month')->nullable();
            $table->integer('days_per_week')->nullable();
            $table->float('hours_per_day')->nullable();
            $table->float('hours_per_week')->nullable();
            $table->float('hours_per_month')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_categories');
    }
}
