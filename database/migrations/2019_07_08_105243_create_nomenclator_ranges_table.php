<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNomenclatorRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomenclator_ranges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('from_code');
            $table->bigInteger('to_code');
            $table->unsignedBigInteger('nomenclator_id');
            $table->foreign('nomenclator_id')->references('id')->on('nomenclators');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_insurance_nomenclator_values');
    }
}
