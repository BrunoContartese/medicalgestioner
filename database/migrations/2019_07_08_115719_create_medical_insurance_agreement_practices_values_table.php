<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalInsuranceAgreementPracticesValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_insurance_agreement_practices_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('medical_insurance_agreement_id');
            $table->foreign('medical_insurance_agreement_id','mi_agreement_id')->references('id')->on('medical_insurance_agreement');
            $table->unsignedBigInteger('nomenclator_practice_id');
            $table->foreign('nomenclator_practice_id', 'nm_practice_id')->references('id')->on('nomenclator_practices');
            $table->float('honorary_unit_value')->default(0);
            $table->float('expense_unit_value')->default(0);
            $table->float('fixed_honorary_value')->default(0);
            $table->float('fixed_expense_value')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_insurance_agreement_practices_values');
    }
}
