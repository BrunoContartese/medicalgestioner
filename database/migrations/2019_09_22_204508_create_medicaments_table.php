<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicaments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label'); //troquel
            $table->string('name'); //Nombre de medicamento
            $table->string('presentation'); //Pesentacion
            $table->string('laboratory_name'); //Nombre de laboratorio
            $table->float('cost_price', 13, 2)->default(0); //Precio de costo
            $table->float('sell_price', 13, 2); //Precio de venta
            $table->unsignedBigInteger('alphabeta_medicament_id'); //Nro. de registro en alfabeta
            $table->boolean('is_active'); //Presentacion activa?
            $table->string('barcode'); //Código de barras
            $table->integer('units'); //Cantidad de unidades x blister

            $table->unsignedBigInteger('laboratory_code'); //Codigo de laboratorio
            $table->foreign('laboratory_code')->references('code')->on('pharmaceutical_laboratories');

            $table->unsignedBigInteger('medicament_drug_code')->nullable(); //Segun tabla de drogas
            $table->foreign('medicament_drug_code')->references('code')->on('medicament_drugs');

            $table->unsignedBigInteger('medicament_administration_line_code')->nullable(); //Segun tabla de vias de administración
            $table->foreign('medicament_administration_line_code')->references('code')->on('medicament_administration_lines');

            $table->unsignedBigInteger('medicament_pharmacological_action_code')->nullable(); //Segun tabla de acciones farmacologicas
            $table->foreign('medicament_pharmacological_action_code')->references('code')->on('medicament_pharmacological_actions');

            $table->unsignedBigInteger('medicament_category_id')->nullable()->default(1);
            $table->foreign('medicament_category_id')->references('id')->on('medicament_categories');

            $table->boolean('on_stock')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicaments');
    }
}
