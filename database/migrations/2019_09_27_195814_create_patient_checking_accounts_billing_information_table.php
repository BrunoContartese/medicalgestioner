<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientCheckingAccountsBillingInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_checking_accounts_billing_information', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('patient_checking_account_id');
            $table->foreign("patient_checking_account_id", 'checking_account_id_foreign')->references('id')->on('patient_checking_accounts');

            $table->unsignedBigInteger('medical_insurance_id');
            $table->foreign("medical_insurance_id", 'medical_insurance_id_foreign')->references('id')->on('medical_insurances');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_checking_account_billing_information');
    }
}
