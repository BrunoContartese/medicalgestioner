<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalSpecialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_specialists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cuit');
            $table->string('licence');
            $table->string('name');
            $table->string('email');
            $table->string('area_code');
            $table->string('phone_number');
            $table->unsignedBigInteger('medical_malpractice_insurance_id')->nullable();
            $table->foreign('medical_malpractice_insurance_id')->references('id')->on('medical_malpractice_insurances');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_specialists');
    }
}
