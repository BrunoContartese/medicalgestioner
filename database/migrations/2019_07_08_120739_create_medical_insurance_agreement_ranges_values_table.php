<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalInsuranceAgreementRangesValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_insurance_agreement_ranges_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('medical_insurance_agreement_id');
            $table->foreign('medical_insurance_agreement_id', 'min_id')->references('id')->on('medical_insurance_agreement');
            $table->unsignedBigInteger('nomenclator_range_id');
            $table->foreign('nomenclator_range_id', 'n_range_id')->references('id')->on('nomenclator_ranges');
            $table->float('expense_unit_value')->default(0);
            $table->float('honorary_unit_value')->default(0);
            $table->float("fixed_honorary_amount")->default(0);
            $table->float("fixed_expense_amount")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_insurance_agreement_ranges_values');
    }
}
