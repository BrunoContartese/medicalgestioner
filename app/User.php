<?php

namespace CloudMed;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'last_login_at',
        'last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|same:repeat_password,',
        'role' => 'required'
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre es obligatorio.',
        'email.required' => 'El campo E-Mail es obligatorio.',
        'email.email' => 'El E-Mail ingresado es inválido.',
        'password.required' => 'El campo Contraseña es obligatorio y debe contener como mínimo 8 caracteres.',
        'password.same' => 'Las contraseñas no coinciden.',
        'role.required' => 'Debe seleccionar el perfil del usuario.'
    ];
}
