<?php

namespace CloudMed\Jobs\Pharmacy;

use Facades\CloudMed\Services\Pharmacy\MedicamentsUpdateService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateMedicamentsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 0;

    private $medicamentUpdateQueueId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($medicamentUpdateQueueId)
    {
        $this->medicamentUpdateQueueId = $medicamentUpdateQueueId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MedicamentsUpdateService::processTxtFiles($this->medicamentUpdateQueueId);
    }
}
