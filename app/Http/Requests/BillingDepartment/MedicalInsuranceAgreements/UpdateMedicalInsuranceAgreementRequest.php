<?php

namespace CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements;

use CloudMed\Models\BillingDepartment\MedicalInsuranceAgreement;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMedicalInsuranceAgreementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsuranceAgreements.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = MedicalInsuranceAgreement::$rules;
        $rules['description'] = 'required|unique:medical_insurance_agreement,description,' . $this->route()->parameter('medicalInsuranceAgreement');

        $validUntil = $this->get('valid_until');

//        if($validUntil == "") {
//            \Log::info("FECHA DE FIN VACÌA");
//        }

        return $rules;
    }

    public function messages()
    {
        return MedicalInsuranceAgreement::$messages;
    }
}
