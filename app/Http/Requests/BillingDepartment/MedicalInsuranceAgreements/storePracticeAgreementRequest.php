<?php

namespace CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements;

use CloudMed\Models\BillingDepartment\MedicalInsuranceAgreement;
use Illuminate\Foundation\Http\FormRequest;

class storePracticeAgreementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsuranceAgreements.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nomenclator_practice_id' => 'required',
            'fixed_expense_value' => 'required',
            'fixed_honorary_value' => 'required'
        ];

        $fixedExpenseValue = $this->get('fixed_expense_value');
        $fixedHonoraryValue = $this->get('fixed_honorary_value');

        \Log::debug($fixedExpenseValue);

        \Log::debug($fixedHonoraryValue);

        $isExpenseValueValid = 0;
        $isHonoraryValueValid = 0;

        if(is_numeric($fixedExpenseValue)) {

            $isExpenseValueValid = 1;

        }

        if(is_numeric($fixedHonoraryValue)) {

            $isHonoraryValueValid = 1;

        }



        if(!$isExpenseValueValid) {
            $rules['fixedExpenseValue'] = 'required';
        }

        if(!$isHonoraryValueValid) {
            $rules['fixedHonoraryValue'] = 'required';
        }

        if($this->checkIfAlreadyAttached()) {
            $rules['exists'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        $messages['nomenclator_practice_id.required'] = 'Debe ingresar el código de la práctica.';
        $messages['fixed_expense_value.required'] = 'Debe ingresar el valor del gasto de la práctica.';
        $messages['fixed_honorary_value.required'] = 'Debe ingresar el valor del honorario de la práctica.';
        $messages['fixedExpenseValue.required'] = 'Verifique que el valor del gasto de la práctica sea un importe válido';
        $messages['fixedHonoraryValue.required'] = 'Verifique que el valor del honorario de la práctica sea un importe válido.';
        $messages['exists.required'] = 'La práctica que desea agregar al convenio ya está valorizada.';

        return $messages;
    }

    private function checkIfAlreadyAttached()
    {
        $practiceId = $this->get('nomenclator_practice_id');
        $medicalInsuranceAgreementId = $this->route()->parameter('medicalInsuranceAgreementId');

        $medicalInsuranceAgreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId);

        $exists = 0;

        foreach($medicalInsuranceAgreement->practicesValuesAgreement as $practice) {
            if($practice->id == $practiceId) {
                $exists = 1;
            }
        }

        return $exists;
    }
}
