<?php

namespace CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements;

use CloudMed\Models\BillingDepartment\MedicalInsuranceAgreement;
use Illuminate\Foundation\Http\FormRequest;

class StoreMedicalInsuranceAgreementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsuranceAgreements.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = MedicalInsuranceAgreement::$rules;

        $withoutEndDate = $this->get('without_end_date');

        if($withoutEndDate == 1) {
            $agreements = MedicalInsuranceAgreement::where('medical_insurance_id', $this->get('medical_insurance_id'))->where('without_end_date', 1)->get();
            $acceptEmptyEndDate = count($agreements) > 0 ? false : true;

            if(!$acceptEmptyEndDate) {
                $rules['existsWithoutEndDate'] = 'required';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = MedicalInsuranceAgreement::$messages;
        $messages['existsWithoutEndDate.required'] = 'Ya existe un convenio con fecha indefinida. Por favor verifique bien los perìodos.';
        return $messages;
    }
}
