<?php

namespace CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements;

use Illuminate\Foundation\Http\FormRequest;

class UploadPracticesAgreementsFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsuranceAgreements.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'practicesAgreementFile' => 'required'
        ];
        return $rules;
    }

    public function messages()
    {
        $messages = [
            'practicesAgreementFile.mimes' => 'El archivo a cargar debe ser un archivo de excel 2007 en adelante (.xlsx).'
        ];

        return $messages;
    }
}
