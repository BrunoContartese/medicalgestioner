<?php

namespace CloudMed\Http\Requests\Administration\Departments;

use CloudMed\Models\Administration\Department;
use CloudMed\Models\Administration\Departments;
use Illuminate\Foundation\Http\FormRequest;

class UpdateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('departments.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Department::$rules;
    }

    public function messages()
    {
        return Department::$messages;
    }
}
