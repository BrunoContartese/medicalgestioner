<?php

namespace CloudMed\Http\Requests\Administration\Departments;

use CloudMed\Http\Controllers\Administration\DepartmentsController;
use CloudMed\Models\Administration\Department;
use Illuminate\Foundation\Http\FormRequest;

class StoreDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('departments.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Department::$rules;
    }

    public function messages()
    {
        return Department::$messages;
    }
}
