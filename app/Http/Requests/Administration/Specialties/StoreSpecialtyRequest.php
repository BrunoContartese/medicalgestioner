<?php

namespace CloudMed\Http\Requests\Administration\Specialties;

use CloudMed\Http\Controllers\Administration\Specialties;
use CloudMed\Models\Administration\Specialty;
use Illuminate\Foundation\Http\FormRequest;

class StoreSpecialtyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('specialties.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Specialty::$rules;
    }

    public function messages()
    {
        return Specialty::$messages;
    }
}
