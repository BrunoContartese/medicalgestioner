<?php

namespace CloudMed\Http\Requests\Administration\Specialties;

use CloudMed\Models\Administration\Specialty;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSpecialtyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('specialties.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Specialty::$rules;
    }

    public function messages()
    {
       return Specialty::$messages;
    }
}
