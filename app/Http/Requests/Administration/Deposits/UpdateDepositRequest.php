<?php

namespace CloudMed\Http\Requests\Administration\Deposits;

use CloudMed\Models\Administration\Deposit;
use Illuminate\Foundation\Http\FormRequest;

class UpdateDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('deposits.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Deposit::$rules;
    }

    public function messages()
    {
        return Deposit::$messages;
    }
}
