<?php

namespace CloudMed\Http\Requests\Administration\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'repeat_password' => 'required_with:password|same:password'
        ];
    }

    public function messages()
    {
        return [
          'email.required' => 'Debe ingresar la dirección de email.',
          'repeat_password.required_with' => 'Debe repetir la contraseña.',
          'repeat_password.same' => 'Las contraseñas ingresadas no coinciden.',
          'email.email' => 'La dirección de email ingresada no es válida.'
        ];
    }
}
