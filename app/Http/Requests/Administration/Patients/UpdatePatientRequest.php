<?php

namespace CloudMed\Http\Requests\Administration\Patients;

use CloudMed\Models\Administration\Patient;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('patients.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Patient::$rules;

        /**
         * I need to check if the document_number value already exists for the selected document type.
         */
        $selectedDocumentTypeId = $this->get('document_type_id');
        $documentNumberInput = $this->get('document_number');

        $query = Patient::where('document_type_id', $selectedDocumentTypeId)
            ->where('document_number', $documentNumberInput)
            ->where('id', '<>', $this->route()->parameter('patient'))
            ->get();

        $isUnique = count($query) > 0 ? false : true;

        /**
         * So, if already exists, i return an error message.
         */
        if(!$isUnique) {
            $rules['document_number'] = 'required|unique:patients,document_number';
        }
        return $rules;
    }

    public function messages()
    {
        return Patient::$messages;
    }
}
