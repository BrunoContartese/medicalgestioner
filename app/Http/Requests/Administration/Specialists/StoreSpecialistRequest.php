<?php

namespace CloudMed\Http\Requests\Administration\Specialists;

use CloudMed\Http\Controllers\Administration\SpecialistsController;
use CloudMed\Models\Administration\Specialist;
use Illuminate\Foundation\Http\FormRequest;

class StoreSpecialistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('specialists.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Specialist::$rules;
    }

    public function messages()
    {
        return Specialist::$messages;
    }
}
