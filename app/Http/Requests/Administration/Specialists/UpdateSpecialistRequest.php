<?php

namespace CloudMed\Http\Requests\Administration\Specialists;

use CloudMed\Models\Administration\Specialist;
use CloudMed\Models\Administration\Specialty;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSpecialistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('specialists.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return Specialist::$rules;

    }

    public function messages()
    {
        return Specialist::$messages;
    }
}
