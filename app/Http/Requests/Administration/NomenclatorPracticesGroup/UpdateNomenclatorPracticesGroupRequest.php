<?php

namespace CloudMed\Http\Requests\Administration\NomenclatorPracticesGroup;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPracticeGroup;
use Illuminate\Foundation\Http\FormRequest;

class UpdateNomenclatorPracticesGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('nomenclatorPracticesGroup.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = NomenclatorPracticeGroup::$rules;
        $rules['name'] = 'required|unique:nomenclator_practices_group,id,' . $this->route()->parameter('practice_group_id');
        return $rules;
    }

    public function messages()
    {
        return NomenclatorPracticeGroup::$messages;
    }
}
