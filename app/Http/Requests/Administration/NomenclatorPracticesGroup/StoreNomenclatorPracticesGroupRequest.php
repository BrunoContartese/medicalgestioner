<?php

namespace CloudMed\Http\Requests\Administration\NomenclatorPracticesGroup;


use CloudMed\Models\Administration\Nomenclators\NomenclatorPracticeGroup;
use Illuminate\Foundation\Http\FormRequest;

class StoreNomenclatorPracticesGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('nomenclatorPracticesGroup.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return NomenclatorPracticeGroup::$rules;
    }

    public function messages()
    {
        return NomenclatorPracticeGroup::$messages;
    }
}
