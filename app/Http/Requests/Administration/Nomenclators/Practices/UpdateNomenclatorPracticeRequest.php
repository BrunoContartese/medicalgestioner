<?php

namespace CloudMed\Http\Requests\Administration\Nomenclators\Practices;

use Illuminate\Foundation\Http\FormRequest;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;

class UpdateNomenclatorPracticeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('nomenclators.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = NomenclatorPractice::$rules;

        /**
         * Check if the expense_units is float
         */
        $expenseUnits = $this->get('expense_units');
        if(!is_numeric($expenseUnits) || !is_float($expenseUnits)) {
            $rules['expense_units'] = 'numeric';
        }

        /**
         * Check if the honorary_units is float
         */
        $honorary_units = $this->get('honorary_units');
        if(!is_numeric($honorary_units) || !is_float($honorary_units)) {
            $rules['honorary_units'] = 'numeric';
        }

        /**
         * Check if the helper_honorary_units is float
         */
        $helper_honorary_units = $this->get('helper_honorary_units');
        if(!is_numeric($helper_honorary_units) || !is_float($helper_honorary_units)) {
            $rules['helper_honorary_units'] = 'numeric';
        }

        /**
         * Check if the anesthesist_honorary_units is float
         */
        $anesthesist_honorary_units = $this->get('anesthesist_honorary_units');
        if(!is_numeric($anesthesist_honorary_units) || !is_float($anesthesist_honorary_units)) {
            $rules['anesthesist_honorary_units'] = 'numeric';
        }

        return $rules;
    }

    public function messages()
    {
        return NomenclatorPractice::$messages;
    }
}
