<?php

namespace CloudMed\Http\Requests\Administration\Nomenclators\Practices;

use Illuminate\Foundation\Http\FormRequest;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;

class StoreNomenclatorPracticeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('nomenclators.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = NomenclatorPractice::$rules;

        $nomenclatorId = $this->route()->parameter('nomenclatorId');

        /**
         * check if the nomenclator exists
         */
        if(null == $nomenclator = Nomenclator::find($nomenclatorId)) {
            $rules['nomenclator'] = 'exists:nomenclators,id';
        }

        /**
         * Check if the code of the practice already exists for the requested nomenclator.
         */
        if(count(NomenclatorPractice::where('code', $this->get('code'))->where('nomenclator_id', $nomenclatorId)->get()) > 0) {
            $rules['code'] = 'unique:nomenclator_practices,code';
        }

        /**
         * Check if the expense_units is float
         */
        $expenseUnits = $this->get('expense_units');
        if(!is_numeric($expenseUnits) || !is_float($expenseUnits)) {
            $rules['expense_units'] = 'numeric';
        }

        /**
         * Check if the honorary_units is float
         */
        $honorary_units = $this->get('honorary_units');
        if(!is_numeric($honorary_units) || !is_float($honorary_units)) {
            $rules['honorary_units'] = 'numeric';
        }

        /**
         * Check if the helper_honorary_units is float
         */
        $helper_honorary_units = $this->get('helper_honorary_units');
        if(!is_numeric($helper_honorary_units) || !is_float($helper_honorary_units)) {
            $rules['helper_honorary_units'] = 'numeric';
        }

        /**
         * Check if the anesthesist_honorary_units is float
         */
        $anesthesist_honorary_units = $this->get('anesthesist_honorary_units');
        if(!is_numeric($anesthesist_honorary_units) || !is_float($anesthesist_honorary_units)) {
            $rules['anesthesist_honorary_units'] = 'numeric';
        }


        return $rules;
    }

    public function messages()
    {
        $messages = NomenclatorPractice::$messages;


        return $messages;
    }
}
