<?php

namespace CloudMed\Http\Requests\Administration\Nomenclators;

use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateNomenclatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('nomenclators.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Nomenclator::$rules;
        $rules['unique'] = 'unique:nomenclators,name,' . $this->route()->parameter('nomenclator');
        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return Nomenclator::$messages;
    }
}
