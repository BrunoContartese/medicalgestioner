<?php

namespace CloudMed\Http\Requests\Administration\Nomenclators\NomenclatorRanges;

use CloudMed\Models\Administration\Nomenclators\NomenclatorRange;
use Illuminate\Foundation\Http\FormRequest;

class UpdateNomenclatorRangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('nomenclatorRanges.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = NomenclatorRange::$rules;

        if($this->checkIfRangeExists()) {
            $rules['exists'] = 'required';
        }


        return $rules;
    }

    private function checkIfRangeExists()
    {
        $fromCode = $this->get('from_code');
        $toCode = $this->get('to_code');

        $createdRanges = NomenclatorRange::where('nomenclator_id', $this->get('nomenclator_id'))->get();

        $exists = false;

        foreach($createdRanges as $range) {
            if(( $fromCode > $range->from_code && $fromCode < $range->to_code ) || $fromCode == $range->from_code || $fromCode == $range->to_code) {
                $exists = true;
            }

            if(( $toCode > $range->from_code && $toCode < $range->to_code ) || $toCode == $range->from_code || $toCode == $range->to_code) {
                $exists = true;
            }
        }

        return $exists;
    }

    public function messages()
    {
        $messages = NomenclatorRange::$messages;

        $messages['exists.required'] = "El rango ingresado ya está contenido entre los rangos creados. Por favor verifique.";

        return $messages;
    }
}
