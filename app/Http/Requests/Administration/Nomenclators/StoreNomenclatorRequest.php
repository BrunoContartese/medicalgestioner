<?php

namespace CloudMed\Http\Requests\Administration\Nomenclators;

use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use Illuminate\Foundation\Http\FormRequest;

class StoreNomenclatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('nomenclators.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Nomenclator::$rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return Nomenclator::$messages;
    }
}
