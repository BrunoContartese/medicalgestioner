<?php

namespace CloudMed\Http\Requests\Administration\MedicalInsurances;

use CloudMed\Models\Administration\MedicalInsurance;
use Illuminate\Foundation\Http\FormRequest;

class StoreMedicalInsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsurances.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return MedicalInsurance::$rules;
    }

    /**
     * Get the messages for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return MedicalInsurance::$messages;
    }
}
