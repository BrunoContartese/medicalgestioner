<?php

namespace CloudMed\Http\Requests\Administration\MedicalInsurances;

use CloudMed\Models\Administration\MedicalInsurance;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMedicalInsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsurances.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = MedicalInsurance::$rules;
        $rules['name'] = 'required|unique:medical_insurances,name,' . $this->route()->parameter('medicalInsurance');
        $rules['cuit'] = 'required|min:11|max:11|unique:medical_insurances,cuit,' . $this->route()->parameter('medicalInsurance');
        return $rules;
    }

    /**
     * Get the messages for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return MedicalInsurance::$messages;
    }
}
