<?php

namespace CloudMed\Http\Requests\Administration\MedicalInsurancesManager;

use Illuminate\Foundation\Http\FormRequest;

class RestoreMedicalInsuranceManagerTrashedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicalInsurancesManagers.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }
}
