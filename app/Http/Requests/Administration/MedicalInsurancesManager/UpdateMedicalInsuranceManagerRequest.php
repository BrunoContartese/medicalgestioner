<?php

namespace CloudMed\Http\Requests\Administration\MedicalInsurancesManager;

use CloudMed\Models\Administration\MedicalInsuranceManager;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMedicalInsuranceManagerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('medicalInsurancesManagers.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = MedicalInsuranceManager::$rules;
        $rules['name'] = 'required|unique:medical_insurances_managers,name,' . $this->route()->parameter('medicalInsurancesManager');
        $rules['cuit'] = 'required|unique:medical_insurances_managers,cuit,' . $this->route()->parameter('medicalInsurancesManager');

        return $rules;
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return MedicalInsuranceManager::$messages;
    }
}
