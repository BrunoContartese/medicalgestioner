<?php

namespace CloudMed\Http\Requests\Administration\MedicalInsurancesManager;

use CloudMed\Models\Administration\MedicalInsuranceManager;
use Illuminate\Foundation\Http\FormRequest;

class StoreMedicalInsuranceManagerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('medicalInsurancesManagers.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return MedicalInsuranceManager::$rules;
    }

    /**
     * Get the messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return MedicalInsuranceManager::$messages;
    }
}
