<?php

namespace CloudMed\Http\Requests\Administration\MedicalMalpracticeInsurances;

use CloudMed\Models\Administration\MedicalMalpracticeInsurance;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMedicalMalpracticeInsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('medicalMalpracticeInsurances.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = MedicalMalpracticeInsurance::$rules;
        $rules['name'] = 'required|unique:medical_malpractice_insurances,name,' . $this->route()->parameter('medicalMalpracticeInsurance');

        return $rules;
    }

    public function messages()
    {
        return MedicalMalpracticeInsurance::$messages;
    }
}
