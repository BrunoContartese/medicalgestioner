<?php

namespace CloudMed\Http\Requests\Administration\MedicalMalpracticeInsurances;

use CloudMed\Models\Administration\MedicalMalpracticeInsurance;
use Illuminate\Foundation\Http\FormRequest;

class StoreMedicalMalpracticeInsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('medicalMalpracticeInsurances.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return MedicalMalpracticeInsurance::$rules;
    }

    public function messages()
    {
        return MedicalMalpracticeInsurance::$messages;
    }
}
