<?php

namespace CloudMed\Http\Requests\HumanResourcesDepartment\EmployeesCategories;

use CloudMed\Models\HumanResourcesDepartment\EmployeeCategory;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('employeesCategories.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = EmployeeCategory::$rules;
        $rules['name'] = "required|unique:employees_categories,name,NULL,id,deleted_at,NULL";
        $rules['tango_code'] = "required|unique:employees_categories,tango_code,NULL,id,deleted_at,NULL";
        return $rules;
    }

    public function messages()
    {
        return EmployeeCategory::$messages;
    }
}
