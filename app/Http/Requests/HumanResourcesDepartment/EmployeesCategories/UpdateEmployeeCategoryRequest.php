<?php

namespace CloudMed\Http\Requests\HumanResourcesDepartment\EmployeesCategories;

use CloudMed\Models\HumanResourcesDepartment\EmployeeCategory;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('employeesCategories.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('employeesCategory');
        $rules = EmployeeCategory::$rules;
        $rules['name'] = "required|unique:employees_categories,name," . $id;
        $rules['tango_code'] = "required|unique:employees_categories,tango_code," . $id;
        return $rules;
    }

    public function messages()
    {
        return EmployeeCategory::$messages;
    }
}
