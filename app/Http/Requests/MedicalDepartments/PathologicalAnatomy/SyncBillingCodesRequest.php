<?php

namespace CloudMed\Http\Requests\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use Illuminate\Foundation\Http\FormRequest;

class SyncBillingCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('pathologicalAnatomy.edit') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        return $messages;
    }
}
