<?php

namespace CloudMed\Http\Requests\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyStudy;
use Illuminate\Foundation\Http\FormRequest;

class createPathologicalAnatomyStudyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('pathologicalAnatomy.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PathologicalAnatomyStudy::$rules;
    }

    public function messages()
    {
        return PathologicalAnatomyStudy::$messages;
    }
}
