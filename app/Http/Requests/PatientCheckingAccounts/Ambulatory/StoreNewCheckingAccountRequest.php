<?php

namespace CloudMed\Http\Requests\PatientCheckingAccounts\Ambulatory;

use CloudMed\Models\Administration\Patient;
use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use Illuminate\Foundation\Http\FormRequest;

class StoreNewCheckingAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->can('ambulatoryPatientCheckingAccounts.create') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = PatientCheckingAccount::$rules;

        /*Verifico si el paciente existe en la base de datos.*/
        $patient = Patient::where('document_type_id', $this->get('document_type_id'))->where('document_number', $this->get('document_number'))->get();

        if(count($patient) < 1) {
            $rules['patient_id'] = 'required';
        } else {
            if($this->checkForOpenedCheckingAccountWithSameMedicalInsurance($patient[0], $this->get('medical_insurance_id'))) {
                $rules['medical_insurance'] = 'required';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = PatientCheckingAccount::$messages;
        $messages['medical_insurance.required'] = 'El paciente ya tiene una rendición abierta con la obra social seleccionada.';
        return $messages;
    }

    private function checkForOpenedCheckingAccountWithSameMedicalInsurance($patient, $medicalInsuranceId)
    {
        $checkingAccounts = PatientCheckingAccount::where('patient_id', $patient->id)->where('patient_checking_account_status_id', 1)->where('patient_checking_account_type_id', 1)->get();

        $hasOpenedCheckingAccount = false;

        if(count($checkingAccounts) > 0) {
            foreach($checkingAccounts as $checkingAccount) {
                $medicalInsurance = $checkingAccount->checkingAccountBillingInformation()->where('medical_insurance_id', $medicalInsuranceId)->get();
                if(count($medicalInsurance) > 0) {
                    $hasOpenedCheckingAccount = true;
                }
            }
        }

        return $hasOpenedCheckingAccount;
    }
}
