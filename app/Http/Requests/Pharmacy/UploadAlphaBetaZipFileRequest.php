<?php

namespace CloudMed\Http\Requests\Pharmacy;

use CloudMed\Models\Pharmacy\MedicamentUpdateQueue;
use Illuminate\Foundation\Http\FormRequest;

class UploadAlphaBetaZipFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermissionTo('medicaments.update') ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'file' => 'required|mimes:zip'
        ];

        $updateInQueue = MedicamentUpdateQueue::where('medicament_update_status_id', 2)->get();

        if(count($updateInQueue) > 0) {
            $rules['inQueue'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'file.required' => 'Debe seleccionar un archivo.',
            'file.mimes' => 'El archivo a subir debe ser un archivo comprimido ZIP.',
            'inQueue.required' => 'En este momento hay una actualización en proceso. Por favor espere a que finalice y reintente.'
        ];
    }
}
