<?php

namespace CloudMed\Http\Controllers;

use Facades\CloudMed\Services\HelpersService;
use Illuminate\Http\Request;

class HelpersController extends Controller
{
    /*Devuelvo un select con las ciudades pertenecientes al id de la provincia requerida*/
    public function citiesByProvinceId($provinceId)
    {
        return view('Helpers.Cities.CitiesByProvince')->with(['province_id' => $provinceId]);
    }

    /*Obtengo las rendiciones abiertas del paciente, si es que tiene devuelvo una tabla, si no, devuelvo un mensaje.*/
    public function getOpenedCheckingAccounts($documentTypeId, $documentNumber)
    {
        return HelpersService::getOpenedCheckingAccounts($documentTypeId, $documentNumber);
    }
}
