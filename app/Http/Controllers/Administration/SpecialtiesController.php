<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Specialties\StoreSpecialtyRequest;
use CloudMed\Http\Requests\Administration\Specialties\UpdateSpecialtyRequest;
use CloudMed\Models\Administration\Specialty;
use Facades\CloudMed\Services\Administration\SpecialtiesService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;


class SpecialtiesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Specialties.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Specialties.create');
    }

    /**
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function store(StoreSpecialtyRequest $request)
    {
        return SpecialtiesService::store($request);
    }

    /**
     * @param $SpecialtyId
     * @return mixed
     */
    public function edit($SpecialtyId)
    {
        $SpecialtyId = $SpecialtyId;
        return SpecialtiesService::edit($SpecialtyId);
    }

    /**
     * @param $SpecialtyId
     * @param UpdateSpecialtyRequest $request
     * @return mixed
     */
    public function update($SpecialtyId, UpdateSpecialtyRequest $request)
    {
        return SpecialtiesService::update($SpecialtyId, $request);
    }

    /**
     * @param $SpecialtyId
     * @return mixed
     */
    public function destroy($SpecialtyId)
    {
        return SpecialtiesService::destroy($SpecialtyId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return SpecialtiesService::dataTable();
    }
}
