<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\MedicalInsurances\RestoreMedicalInsuranceTrashedRequest;
use CloudMed\Http\Requests\Administration\MedicalInsurances\StoreMedicalInsuranceRequest;
use CloudMed\Http\Requests\Administration\MedicalInsurances\StoreMedicalInsurancesNewsBoardRequest;
use CloudMed\Http\Requests\Administration\MedicalInsurances\UpdateMedicalInsuranceRequest;
use Facades\CloudMed\Services\Administration\MedicalInsurancesService;
use CloudMed\Http\Controllers\Controller;

class MedicalInsurances extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.MedicalInsurances.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.MedicalInsurances.create');
    }

    /**
     * @param StoreMedicalInsuranceRequest $request
     * @return mixed
     */
    public function store(StoreMedicalInsuranceRequest $request)
    {
        return MedicalInsurancesService::store($request);
    }

    /**
     * @param $medicalInsuranceId
     * @return mixed
     */
    public function edit($medicalInsuranceId)
    {
        return MedicalInsurancesService::edit($medicalInsuranceId);
    }

    /**
     * @param $medicalInsuranceId
     * @param UpdateMedicalInsuranceRequest $request
     * @return mixed
     */
    public function update($medicalInsuranceId, UpdateMedicalInsuranceRequest $request)
    {
        return MedicalInsurancesService::update($medicalInsuranceId, $request);
    }

    /**
     * @param $medicalInsuranceId
     * @return mixed
     */
    public function destroy($medicalInsuranceId)
    {
        return MedicalInsurancesService::destroy($medicalInsuranceId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return MedicalInsurancesService::dataTable();
    }

    /**
     * @param $medicalInsuranceId
     * @param RestoreMedicalInsuranceTrashedRequest $request
     * @return mixed
     */
    function restoreTrashed($medicalInsuranceId, RestoreMedicalInsuranceTrashedRequest $request)
    {
        return MedicalInsurancesService::restoreTrashed($medicalInsuranceId);
    }

    public function writeNews($medicalInsuranceId)
    {
        return MedicalInsurancesService::writeNews($medicalInsuranceId);
    }

    public function storeNews($medicalInsuranceId, StoreMedicalInsurancesNewsBoardRequest $request)
    {
        return MedicalInsurancesService::storeNews($medicalInsuranceId, $request);
    }
}
