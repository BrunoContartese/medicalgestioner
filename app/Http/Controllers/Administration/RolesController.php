<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Roles\StoreRoleRequest;
use CloudMed\Http\Requests\Administration\Roles\UpdatePermissionsRequest;
use CloudMed\Http\Requests\Administration\Roles\UpdateRoleRequest;
use Facades\CloudMed\Services\Administration\RolesService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Yoeunes\Toastr\Facades\Toastr;

class RolesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Roles.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Roles.create');
    }

    /**
     * @param StoreRoleRequest $request
     * @return mixed
     */
    public function store(StoreRoleRequest $request)
    {
        return RolesService::store($request);
    }

    /**
     * @param $roleId
     * @return mixed
     */
    public function edit($roleId)
    {
        return RolesService::edit($roleId);
    }

    /**
     * @param $roleId
     * @param UpdateRoleRequest $request
     * @return mixed
     */
    public function update($roleId, UpdateRoleRequest $request)
    {
        return RolesService::update($roleId, $request);
    }

    /**
     * @param $roleId
     * @return mixed
     */
    public function destroy($roleId)
    {
        return RolesService::destroy($roleId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return RolesService::dataTable();
    }

    /**
     * edit permissions for the selected role method
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function editPermissions($roleId)
    {
        if(null == $role = Role::find($roleId)) {
            Toastr::error('Perfil no encontrado.');
            return redirect(route('roles.index'));
        }

        return view('Administration.Roles.permissions')->with('role', $role);
    }

    public function updatePermissions($roleId, UpdatePermissionsRequest $request)
    {
        return RolesService::updatePermissions($roleId, $request);
    }
}
