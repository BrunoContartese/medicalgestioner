<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Departments\StoreDepartmentRequest;
use CloudMed\Http\Requests\Administration\Departments\UpdateDepartmentRequest;
use Facades\CloudMed\Services\Administration\DepartmentsService;

use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;


class DepartmentsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Departments.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Departments.create');
    }

    /**
     * @param StoreDepartmentRequest $request
     * @return mixed
     */
    public function store(StoreDepartmentRequest $request)
    {
        return DepartmentsService::store($request);
    }

    /**
     * @param $DepartmentId
     * @return mixed
     */
    public function edit($DepartmentId)
    {
        $DepartmentId = $DepartmentId;
        return DepartmentsService::edit($DepartmentId);
    }

    /**
     * @param $DepartmentId
     * @param UpdateDepartmentRequest $request
     * @return mixed
     */
    public function update($DepartmentId, UpdateDepartmentRequest $request)
    {
        return DepartmentsService::update($DepartmentId, $request);
    }

    /**
     * @param $DepartmentId
     * @return mixed
     */
    public function destroy($DepartmentId)
    {
        return DepartmentsService::destroy($DepartmentId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return DepartmentsService::dataTable();
    }
}
