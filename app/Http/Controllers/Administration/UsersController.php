<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Users\StoreUserRequest;
use CloudMed\Http\Requests\Administration\Users\UpdateUserRequest;
use Facades\CloudMed\Services\Administration\UsersService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Users.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Users.create');
    }

    /**
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {
        return UsersService::store($request);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function edit($userId)
    {
        return UsersService::edit($userId);
    }

    /**
     * @param $userId
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update($userId, UpdateUserRequest $request)
    {
        return UsersService::update($userId, $request);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function destroy($userId)
    {
        return UsersService::destroy($userId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return UsersService::dataTable();
    }

    public function profile()
    {
        return view('auth.profile')->with(['user' => \Auth::user()]);
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        return UsersService::updateProfile($request);
    }

    public function profileImage()
    {
        return view('auth.profileImage')->with('user', \Auth::user());
    }

    public function updateProfileImage(Request $request)
    {
        return UsersService::updateProfileImage($request);
    }

    public function signatureImage()
    {
        return view('auth.profileSignature')->with('user', \Auth::user());
    }

    public function updateSignatureImage(Request $request)
    {
        return UsersService::updateSignatureImage($request);
    }

    public function getSignature()
    {
        return UsersService::getSignature();
    }
}
