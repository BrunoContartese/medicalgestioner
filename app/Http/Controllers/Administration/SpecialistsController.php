<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Specialists\StoreSpecialistRequest;
use CloudMed\Http\Requests\Administration\Specialists\UpdateSpecialistRequest;
use Facades\CloudMed\Services\Administration\SpecialistsService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class SpecialistsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Specialists.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Specialists.create');
    }

    /**
     * @param StoreSpecialistRequest $request
     * @return mixed
     */
    public function store(StoreSpecialistRequest $request)
    {
        return SpecialistsService::store($request);
    }

    /**
     * @param $specialistId
     * @return mixed
     */
    public function edit($specialistId)
    {
        return SpecialistsService::edit($specialistId);
    }

    /**
     * @param $specialistId
     * @param UpdateSpecialistRequest $request
     * @return mixed
     */
    public function update($specialistId, UpdateSpecialistRequest $request)
    {
        return SpecialistsService::update($specialistId, $request);
    }

    /**
     * @param $specialistId
     * @return mixed
     */
    public function destroy($specialistId)
    {
        return SpecialistsService::destroy($specialistId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return SpecialistsService::dataTable();
    }
}
