<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Patients\StorePatientRequest;
use CloudMed\Http\Requests\Administration\Patients\UpdatePatientRequest;
use Facades\CloudMed\Services\Administration\PatientsService;

class PatientsController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Patients.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Patients.create');
    }

    /**
     * @param StoreDepositRequest $request
     * @return mixed
     */
    public function store(StorePatientRequest $request)
    {
        return PatientsService::store($request);
    }

    /**
     * @param $patientId
     * @return mixed
     */
    public function edit($patientId)
    {
        return PatientsService::edit($patientId);
    }

    /**
     * @param $patientId
     * @param UpdatePatientRequest $request
     * @return mixed
     */
    public function update($patientId, UpdatePatientRequest $request)
    {
        return PatientsService::update($patientId, $request);
    }

    public function show($patientId)
    {
        return PatientsService::show($patientId);
    }

    /**
     * @param $patientId
     * @return mixed
     */
    public function destroy($patientId)
    {
        return PatientsService::destroy($patientId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return PatientsService::dataTable();
    }

    public function findByDocumentNumber($documentType, $documentNumber)
    {
        return PatientsService::findByDocumentNumber($documentType, $documentNumber);
    }

    public function getPatientMedicalInsurances($documentType, $documentNumber)
    {
        return PatientsService::getPatientMedicalInsurances($documentType, $documentNumber);
    }
}