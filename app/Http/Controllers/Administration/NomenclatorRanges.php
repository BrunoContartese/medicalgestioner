<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Nomenclators\NomenclatorRanges\StoreNomenclatorRangeRequest;
use CloudMed\Http\Requests\Administration\Nomenclators\NomenclatorRanges\UpdateNomenclatorRangeRequest;
use Facades\CloudMed\Services\Administration\NomenclatorRangesService;

use CloudMed\Http\Controllers\Controller;

class NomenclatorRanges extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Administration.NomenclatorRanges.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Administration.NomenclatorRanges.create');
    }

    public function edit($nomenclatorRangeId)
    {
        return NomenclatorRangesService::edit($nomenclatorRangeId);
    }

    public function update($nomenclatorRangeId, UpdateNomenclatorRangeRequest $request)
    {
        return NomenclatorRangesService::update($nomenclatorRangeId, $request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNomenclatorRangeRequest $request)
    {
        return NomenclatorRangesService::store($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nomenclatorRangeId)
    {
        return NomenclatorRangesService::destroy($nomenclatorRangeId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return NomenclatorRangesService::dataTable();
    }
}
