<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\Nomenclators\RestoreNomenclatorRequest;
use CloudMed\Http\Requests\Administration\Nomenclators\StoreNomenclatorRequest;
use CloudMed\Http\Requests\Administration\Nomenclators\UpdateNomenclatorRequest;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use Facades\CloudMed\Services\Administration\NomenclatorsService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class Nomenclators extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Administration.Nomenclators.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Administration.Nomenclators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNomenclatorRequest $request)
    {
        return NomenclatorsService::store($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nomenclatorId)
    {
        return NomenclatorsService::edit($nomenclatorId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNomenclatorRequest $request, $nomenclatorId)
    {
        return NomenclatorsService::update($nomenclatorId, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nomenclatorId)
    {
        return NomenclatorsService::destroy($nomenclatorId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return NomenclatorsService::dataTable();
    }

    function restoreTrashed($nomenclatorId, RestoreNomenclatorRequest $request)
    {
        return NomenclatorsService::restoreTrashed($nomenclatorId);
    }
}
