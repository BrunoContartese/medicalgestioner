<?php

namespace CloudMed\Http\Controllers\Administration;


use CloudMed\Http\Requests\Administration\Deposits\UpdateDepositRequest;
use CloudMed\Http\Requests\Administration\Deposits\StoreDepositRequest;
use Facades\CloudMed\Services\Administration\DepositsService;

use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;


class DepositsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.Deposits.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.Deposits.create');
    }

    /**
     * @param StoreDepositRequest $request
     * @return mixed
     */
    public function store(StoreDepositRequest $request)
    {
        return DepositsService::store($request);
    }

    /**
     * @param $DepositId
     * @return mixed
     */
    public function edit($DepositId)
    {
        $DepositId = $DepositId;
        return DepositsService::edit($DepositId);
    }

    /**
     * @param $DepositId
     * @param UpdateDepositRequest $request
     * @return mixed
     */
    public function update($DepositId, UpdateDepositRequest $request)
    {
        return DepositsService::update($DepositId, $request);
    }

    /**
     * @param $DepositId
     * @return mixed
     */
    public function destroy($DepositId)
    {
        return DepositsService::destroy($DepositId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return DepositsService::dataTable();
    }
}
