<?php
/**
 * Created by PhpStorm.
 * User: bcontartese
 * Date: 23/07/19
 * Time: 21:20
 */

namespace CloudMed\Http\Controllers\Administration;


use Laracasts\Flash\Flash;
use CloudMed\Http\Controllers\Controller;
use CloudMed\Http\Requests\Administration\Nomenclators\Practices\ChangeHelpersQuantityRequest;
use CloudMed\Http\Requests\Administration\Nomenclators\Practices\StoreNomenclatorPracticeRequest;
use CloudMed\Http\Requests\Administration\Nomenclators\Practices\UpdateNomenclatorPracticeRequest;
use Facades\CloudMed\Services\Administration\NomenclatorPracticesService;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;

class NomenclatorPractices extends Controller
{

    public function practicesList($nomenclatorId)
    {
        return NomenclatorPracticesService::practicesList($nomenclatorId);
    }

    public function create($nomenclatorId)
    {
        if(null == $nomenclator = Nomenclator::find($nomenclatorId)) {
            Flash::error("Nomenclator no encontrado.");
            return redirect(route('nomenclators.index'));
        }

        return view('Administration.Nomenclators.Practices.create')->with('nomenclator', $nomenclator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($nomenclatorId, StoreNomenclatorPracticeRequest $request)
    {
        return NomenclatorPracticesService::store($nomenclatorId, $request);
    }

    /**
     * @return mixed
     */
    public function dataTable($nomenclatorId)
    {
        return NomenclatorPracticesService::dataTable($nomenclatorId);
    }

    public function edit($nomenclatorPracticeId)
    {
        if(null == $nomenclatorPractice = NomenclatorPractice::find($nomenclatorPracticeId)) {
            Flash::error("Práctica no encontrada.");
            return redirect(route('nomenclators.index'));
        }

        return view('Administration.Nomenclators.Practices.edit')->with('nomenclatorPractice', $nomenclatorPractice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($nomenclatorPracticeId, UpdateNomenclatorPracticeRequest $request)
    {
        return NomenclatorPracticesService::update($nomenclatorPracticeId, $request);
    }

    public function getInfoById($nomenclatorPracticeId)
    {
        return NomenclatorPracticesService::getInfoById($nomenclatorPracticeId);
    }

    public function getInfoByCode($nomenclatorPracticeCode)
    {
        return NomenclatorPracticesService::getInfoByCode($nomenclatorPracticeCode);
    }

}