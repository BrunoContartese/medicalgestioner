<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\MedicalInsurancesManager\StoreMedicalInsuranceManagerRequest;
use CloudMed\Http\Requests\Administration\MedicalInsurancesManager\UpdateMedicalInsuranceManagerRequest;
use CloudMed\Models\Administration\MedicalInsuranceManager;
use Facades\CloudMed\Services\Administration\MedicalInsurancesManagersService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;
use CloudMed\Http\Requests\Administration\MedicalInsurancesManager\RestoreMedicalInsuranceManagerTrashedRequest;

class MedicalInsurancesManagers extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.MedicalInsurancesManagers.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.MedicalInsurancesManagers.create');
    }

    /**
     * @param StoreMedicalInsuranceManagerRequest $request
     * @return mixed
     */
    public function store(StoreMedicalInsuranceManagerRequest $request)
    {
        return MedicalInsurancesManagersService::store($request);
    }

    /**
     * @param $medicalInsuranceManagerId
     * @return mixed
     */
    public function edit($medicalInsuranceManagerId)
    {
        return MedicalInsurancesManagersService::edit($medicalInsuranceManagerId);
    }

    /**
     * @param $medicalInsuranceManagerId
     * @param UpdateMedicalInsuranceManagerRequest $request
     * @return mixed
     */
    public function update($medicalInsuranceManagerId, UpdateMedicalInsuranceManagerRequest $request)
    {
        return MedicalInsurancesManagersService::update($medicalInsuranceManagerId, $request);
    }

    /**
     * @param $medicalInsuranceId
     * @return mixed
     */
    public function destroy($medicalInsuranceManagerId)
    {
        return MedicalInsurancesManagersService::destroy($medicalInsuranceManagerId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return MedicalInsurancesManagersService::dataTable();
    }

    /**
     * @param $medicalInsuranceManagerId
     * @param RestoreMedicalInsuranceManagerTrashedRequest $request
     * @return mixed
     */
    function restoreTrashed($medicalInsuranceManagerId, RestoreMedicalInsuranceManagerTrashedRequest $request)
    {
        return MedicalInsurancesManagersService::restoreTrashed($medicalInsuranceManagerId);
    }
}
