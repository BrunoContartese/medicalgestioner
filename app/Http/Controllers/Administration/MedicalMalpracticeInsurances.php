<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\MedicalMalpracticeInsurances\StoreMedicalMalpracticeInsuranceRequest;
use CloudMed\Http\Requests\Administration\MedicalMalpracticeInsurances\UpdateMedicalMalpracticeInsuranceRequest;
use Facades\CloudMed\Services\Administration\MedicalMalpracticeInsurancesService;
use CloudMed\Http\Controllers\Controller;


class MedicalMalpracticeInsurances extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.MedicalMalpracticeInsurances.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.MedicalMalpracticeInsurances.create');
    }

    /**
     * @param StoreMedicalMalpracticeInsuranceRequest $request
     * @return mixed
     */
    public function store(StoreMedicalMalpracticeInsuranceRequest $request)
    {
        return MedicalMalpracticeInsurancesService::store($request);
    }

    /**
     * @param $MedicalMalpracticeInsuranceId
     * @return mixed
     */
    public function edit($MedicalMalpracticeInsuranceId)
    {
        return MedicalMalpracticeInsurancesService::edit($MedicalMalpracticeInsuranceId);
    }

    /**
     * @param $MedicalMalpracticeInsuranceId
     * @param UpdateMedicalMalpracticeInsuranceRequest $request
     * @return mixed
     */
    public function update($MedicalMalpracticeInsuranceId, UpdateMedicalMalpracticeInsuranceRequest $request)
    {
        return MedicalMalpracticeInsurancesService::update($MedicalMalpracticeInsuranceId, $request);
    }

    /**
     * @param $MedicalMalpracticeInsuranceId
     * @return mixed
     */
    public function destroy($MedicalMalpracticeInsuranceId)
    {
        return MedicalMalpracticeInsurancesService::destroy($MedicalMalpracticeInsuranceId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return MedicalMalpracticeInsurancesService::dataTable();
    }
}
