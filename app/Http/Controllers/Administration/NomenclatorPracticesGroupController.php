<?php

namespace CloudMed\Http\Controllers\Administration;

use CloudMed\Http\Requests\Administration\NomenclatorPracticesGroup\StoreNomenclatorPracticesGroupRequest;
use CloudMed\Http\Requests\Administration\NomenclatorPracticesGroup\UpdateNomenclatorPracticesGroupRequest;
use Facades\CloudMed\Services\Administration\NomenclatorPracticesGroupService;

use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class NomenclatorPracticesGroupController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Administration.NomenclatorPracticesGroup.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Administration.NomenclatorPracticesGroup.create');
    }

    /**
     * @param StoreNomenclatorPracticesGroupRequest $request
     * @return mixed
     */
    public function store(StoreNomenclatorPracticesGroupRequest $request)
    {
        return NomenclatorPracticesGroupService::store($request);
    }

    /**
     * @param $nomenclatorPracticeGroupId
     * @return mixed
     */
    public function edit($nomenclatorPracticeGroupId)
    {
        return NomenclatorPracticesGroupService::edit($nomenclatorPracticeGroupId);
    }

    /**
     * @param $nomenclatorPracticeGroupId
     * @param UpdateNomenclatorPracticesGroupRequest $request
     * @return mixed
     */
    public function update($nomenclatorPracticeGroupId, UpdateNomenclatorPracticesGroupRequest $request)
    {
        return NomenclatorPracticesGroupService::update($nomenclatorPracticeGroupId, $request);
    }

    /**
     * @param $nomenclatorPracticeGroupId
     * @return mixed
     */
    public function destroy($nomenclatorPracticeGroupId)
    {
        return NomenclatorPracticesGroupService::destroy($nomenclatorPracticeGroupId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return NomenclatorPracticesGroupService::dataTable();
    }
}
