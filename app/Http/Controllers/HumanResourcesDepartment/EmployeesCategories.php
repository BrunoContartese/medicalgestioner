<?php

namespace CloudMed\Http\Controllers\HumanResourcesDepartment;

use Carbon\Carbon;
use CloudMed\Http\Requests\HumanResourcesDepartment\EmployeesCategories\StoreEmployeeCategoryRequest;
use CloudMed\Http\Requests\HumanResourcesDepartment\EmployeesCategories\UpdateEmployeeCategoryRequest;
use Facades\CloudMed\Services\HumanResourcesDepartment\EmployeesCategoriesService;
use Yoeunes\Toastr\Facades\Toastr;

class EmployeesCategories
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('HumanResourcesDepartment.EmployeesCategories.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('HumanResourcesDepartment.EmployeesCategories.create');
    }

    /**
     * @param StoreEmployeeCategoryRequest $request
     * @return mixed
     */
    public function store(StoreEmployeeCategoryRequest $request)
    {
        return EmployeesCategoriesService::store($request);
    }

    /**
     * @param $employeeCategoryId
     * @return mixed
     */
    public function edit($employeeCategoryId)
    {
        return EmployeesCategoriesService::edit($employeeCategoryId);
    }

    /**
     * @param $employeeCategoryId
     * @param UpdateEmployeeCategoryRequest $request
     * @return mixed
     */
    public function update($employeeCategoryId, UpdateEmployeeCategoryRequest $request)
    {
        return EmployeesCategoriesService::update($employeeCategoryId, $request);
    }

    /**
     * @param $employeeCategoryId
     * @return mixed
     */
    public function destroy($employeeCategoryId)
    {
        return EmployeesCategoriesService::destroy($employeeCategoryId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return EmployeesCategoriesService::dataTable();
    }
}