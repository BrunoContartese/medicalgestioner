<?php

namespace CloudMed\Http\Controllers;

use CloudMed\Http\Requests\Administration\Users\UpdateProfileRequest;
use Facades\CloudMed\Services\Administration\UsersService;
use Illuminate\Http\Request;
use Yoeunes\Toastr\Facades\Toastr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        Toastr::info("Bienvenido a CloudMed");
        return view('home');
    }
}
