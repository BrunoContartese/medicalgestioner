<?php

namespace CloudMed\Http\Controllers\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelThree;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class PathologicalAnatomyLevelThreeCategories extends Controller
{
    public function childCategories($categoryId)
    {
        if(null == $category = PathologicalAnatomyCategoryLevelThree::find($categoryId))
        {
            return "";
        }

        return view('MedicalDepartments.PathologicalAnatomy.Categories.PathologicalAnatomyCategoriesLevelThree.childCategories')->with(['categories' => $category->childCategories]);
    }
}
