<?php

namespace CloudMed\Http\Controllers\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelOne;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class PathologicalAnatomyLevelOneCategories extends Controller
{
    public function childCategories($categoryId)
    {
        if(null == $category = PathologicalAnatomyCategoryLevelOne::find($categoryId))
        {
            return "";
        }

        return view('MedicalDepartments.PathologicalAnatomy.Categories.PathologicalAnatomyCategoriesLevelOne.childCategories')->with(['categories' => $category->childCategories]);
    }
}
