<?php

namespace CloudMed\Http\Controllers\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Http\Requests\MedicalDepartments\PathologicalAnatomy\createPathologicalAnatomyStudyRequest;
use CloudMed\Http\Requests\MedicalDepartments\PathologicalAnatomy\SyncBillingCodesRequest;
use CloudMed\Http\Requests\MedicalDepartments\PathologicalAnatomy\SyncCategoriesRequest;
use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\MedicalReportTemplate;
use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\MedicalReportTemplateCategory;
use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\MedicalReportTitle;
use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyStudy;
use Facades\CloudMed\Services\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class PathologicalAnatomyController extends Controller
{
    public function index()
    {
        return view('MedicalDepartments.PathologicalAnatomy.index');
    }

    public function create()
    {
        return view('MedicalDepartments.PathologicalAnatomy.create');
    }

    public function dataTable()
    {
        return PathologicalAnatomyService::dataTable();
    }

    public function store(createPathologicalAnatomyStudyRequest $request)
    {
        return PathologicalAnatomyService::store($request);
    }

    public function destroy($studyId)
    {
        return PathologicalAnatomyService::destroy($studyId);
    }

    public function printSticker($studyCode)
    {
        return PathologicalAnatomyService::printSticker($studyCode);
    }

    public function medicalReport()
    {
        return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.index');
    }

    public function dataTableForMedicalReport()
    {
        return PathologicalAnatomyService::dataTableForMedicalReport();
    }

    public function billingCodesAssignment($sampleId)
    {
        if(null == $sample = PathologicalAnatomyStudy::find($sampleId)) {
            \Toastr::error("La muestra seleccionada no existe en la base de datos.");
            return redirect(url('/medicalDepartments/pathologicalAnatomy'));
        }
        return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.billingCodesAssigment')->with(['sample' => $sample]);
    }

    public function syncBillingCodesWithSample($sampleId, SyncBillingCodesRequest $request)
    {
        return PathologicalAnatomyService::syncBillingCodesWithSample($sampleId, $request);
    }

    public function categorizeSample($sampleId)
    {
        if(null == $sample = PathologicalAnatomyStudy::find($sampleId)) {
            \Toastr::error("La muestra seleccionada no existe en la base de datos.");
            return redirect(url('/medicalDepartments/pathologicalAnatomy'));
        }
        return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.categorizeSample')->with(['sample' => $sample]);
    }

    public function syncCategoriesWithSample($sampleId, SyncCategoriesRequest $request)
    {
        return PathologicalAnatomyService::syncCategoriesWithSample($sampleId, $request);
    }

    public function writeMedicalReport($sampleId)
    {
        if(null == $sample = PathologicalAnatomyStudy::find($sampleId)) {
            \Toastr::error("La muestra seleccionada no existe en la base de datos.");
            return redirect(url('/medicalDepartments/pathologicalAnatomy'));
        }

        return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.WriteMedicalReport')->with('sample', $sample);
    }

    public function getMedicalReportTitle($titleId)
    {
        if(null == $title = MedicalReportTitle::find($titleId)) {
            return \Response::json(["errors" => 'El título seleccionado no existe en la base de datos. '], 422);
        }

        return \Response::json($title->name, 200);
    }

    public function getTemplatesByCategory($templateCategoryId)
    {
        if(null == $templateCategory = MedicalReportTemplateCategory::find($templateCategoryId)) {
            return \Response::json(["errors" => 'La categoría seleccionada no existe en la base de datos. '], 422);
        }

        return view('MedicalDepartments.PathologicalAnatomy.Templates.TemplateSelect')->with(['templates' => $templateCategory->childTemplates]);
    }

    public function getMedicalReportTemplate($templateId)
    {
        if(null == $template = MedicalReportTemplate::find($templateId)) {
            return \Response::json(["errors" => 'La plantilla seleccionada no existe en la base de datos. '], 422);
        }

        return \Response::json($template->description, 200);
    }

    public function papanicolaouTemplateDialog()
    {
        return view('MedicalDepartments.PathologicalAnatomy.Templates.papanicolaouTemplate');
    }

    public function getPapanicolaouTemplateDescription($templateId)
    {
        return PathologicalAnatomyService::getPapanicolaouTemplateDescription($templateId);
    }
}
