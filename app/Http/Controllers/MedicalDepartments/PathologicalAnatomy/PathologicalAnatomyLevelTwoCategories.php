<?php

namespace CloudMed\Http\Controllers\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyCategoryLevelTwo;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class PathologicalAnatomyLevelTwoCategories extends Controller
{
    public function childCategories($categoryId)
    {
        if(null == $category = PathologicalAnatomyCategoryLevelTwo::find($categoryId))
        {
            return "";
        }

        return view('MedicalDepartments.PathologicalAnatomy.Categories.PathologicalAnatomyCategoriesLevelTwo.childCategories')->with(['categories' => $category->childCategories]);
    }
}
