<?php

namespace CloudMed\Http\Controllers\Pharmacy;

use CloudMed\Http\Requests\Pharmacy\UploadAlphaBetaZipFileRequest;
use CloudMed\Jobs\Pharmacy\UpdateMedicamentsJob;
use Facades\CloudMed\Services\Pharmacy\MedicamentsUpdateService;
use Facades\CloudMed\Services\Pharmacy\MedicamentsService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;
use Yoeunes\Toastr\Facades\Toastr;

class MedicamentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Pharmacy.Medicaments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       return MedicamentsService::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return MedicamentsService::dataTable();
    }

    public function medicamentsUpdate()
    {
        return view('Pharmacy.Medicaments.medicamentsUpdate.update');
    }

    public function uploadAlphaBetaZipFile(UploadAlphaBetaZipFileRequest $request)
    {
        MedicamentsUpdateService::uploadAlphaBetaZipFile($request->file);

        Toastr::warning("Actualización en proceso.");

        return \Response::json("ok", 200);
    }

    public function medicamentUpdatesDataTable()
    {
        return MedicamentsService::medicamentUpdatesDataTable();
    }
}
