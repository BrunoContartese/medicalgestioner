<?php

namespace CloudMed\Http\Controllers\BillingDepartment;

use Carbon\Carbon;
use CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements\DeleteSinglePracticeAgreementRequest;
use CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements\StoreMedicalInsuranceAgreementRequest;
use CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements\storePracticeAgreementRequest;
use CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements\UpdateMedicalInsuranceAgreementRequest;
use CloudMed\Http\Requests\BillingDepartment\MedicalInsuranceAgreements\UploadPracticesAgreementsFileRequest;
use CloudMed\Imports\BillingDepartment\MedicalInsuranceAgreements\PracticesAgreementImport;
use CloudMed\Models\BillingDepartment\MedicalInsuranceAgreement;
use Facades\CloudMed\Services\BillingDepartment\MedicalInsuranceAgreementsService;
use Maatwebsite\Excel\Facades\Excel;
use Yoeunes\Toastr\Facades\Toastr;

class MedicalInsuranceAgreements
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('BillingDepartment.MedicalInsuranceAgreements.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('BillingDepartment.MedicalInsuranceAgreements.create');
    }

    /**
     * @param StoreMedicalInsuranceAgreementRequest $request
     * @return mixed
     */
    public function store(StoreMedicalInsuranceAgreementRequest $request)
    {
        return MedicalInsuranceAgreementsService::store($request);
    }

    /**
     * @param $medicalInsuranceAgreementId
     * @return mixed
     */
    public function edit($medicalInsuranceAgreementId)
    {
        return MedicalInsuranceAgreementsService::edit($medicalInsuranceAgreementId);
    }

    /**
     * @param $medicalInsuranceAgreementId
     * @param UpdateMedicalInsuranceAgreementRequest $request
     * @return mixed
     */
    public function update($medicalInsuranceAgreementId, UpdateMedicalInsuranceAgreementRequest $request)
    {
        return MedicalInsuranceAgreementsService::update($medicalInsuranceAgreementId, $request);
    }

    /**
     * @param $medicalInsuranceAgreementId
     * @return mixed
     */
    public function destroy($medicalInsuranceAgreementId)
    {
        return MedicalInsuranceAgreementsService::destroy($medicalInsuranceAgreementId);
    }

    /**
     * @return mixed
     */
    public function dataTable()
    {
        return MedicalInsuranceAgreementsService::dataTable();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function practicesAgreementModal($medicalInsuranceAgreementId)
    {
        return view('BillingDepartment.MedicalInsuranceAgreements.practicesAgreementModal')->with(['medicalInsuranceAgreementId' => $medicalInsuranceAgreementId]);
    }

    public function uploadPracticesAgreement($medicalInsuranceAgreementId, UploadPracticesAgreementsFileRequest $request)
    {
        return MedicalInsuranceAgreementsService::uploadPracticesAgreement($medicalInsuranceAgreementId, $request);
    }

    public function practicesAgreementDataTable($medicalInsuranceAgreementId)
    {
        return MedicalInsuranceAgreementsService::practicesAgreementDataTable($medicalInsuranceAgreementId);
    }

    public function practicesAgreement($medicalInsuranceAgreementId)
    {
        if(null == $medicalInsuranceAgreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId)) {
            Toastr::error("Convenio no encontrado.");
            return redirect('/billingDepartment/medicalInsuranceAgreements');
        }
        return view("BillingDepartment.MedicalInsuranceAgreements.practicesAgreement.practicesAgreement")->with(['medicalInsuranceAgreement' => $medicalInsuranceAgreement]);
    }

    public function deleteSinglePracticeAgreement($medicalInsuranceAgreementId, DeleteSinglePracticeAgreementRequest $request)
    {
        return MedicalInsuranceAgreementsService::deleteSinglePracticeAgreement($medicalInsuranceAgreementId, $request);
    }

    public function addPracticeAgreementModal()
    {
        $modal =  view('BillingDepartment.MedicalInsuranceAgreements.practicesAgreement.addNewPracticeAgreement')->render();
        return \Response::json($modal, 200);
    }

    public function storePracticeAgreement($medicalInsuranceAgreementId, storePracticeAgreementRequest $request)
    {
        return MedicalInsuranceAgreementsService::storePracticeAgreement($medicalInsuranceAgreementId, $request);
    }
}