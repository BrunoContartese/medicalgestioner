<?php

namespace CloudMed\Http\Controllers\PatientCheckingAccounts;

use Facades\CloudMed\Services\PatientCheckingAccounts\PatientCheckingAccountsService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class PatientCheckingAccounts extends Controller
{
    public function index()
    {
        return PatientCheckingAccountsService::index();
    }

    public function dataTable()
    {
        return PatientCheckingAccountsService::dataTable();
    }
}
