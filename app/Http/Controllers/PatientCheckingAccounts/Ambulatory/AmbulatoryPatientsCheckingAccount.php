<?php

namespace CloudMed\Http\Controllers\PatientCheckingAccounts\Ambulatory;

use CloudMed\Http\Requests\PatientCheckingAccounts\Ambulatory\StoreNewCheckingAccountRequest;
use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use Facades\CloudMed\Services\PatientCheckingAccounts\Ambulatory\AmbulatoryPatientsCheckingAccountService;
use Illuminate\Http\Request;
use CloudMed\Http\Controllers\Controller;

class AmbulatoryPatientsCheckingAccount extends Controller
{

    public function checkIn()
    {
        return view('PatientCheckingAccounts.Ambulatory.OpenNewAccount');
    }

    public function getAmbulatoryCheckingAccountFinalStepInformation($documentTypeId, $documentNumber, $medicalInsuranceId)
    {
        return AmbulatoryPatientsCheckingAccountService::getAmbulatoryCheckingAccountFinalStepInformation($documentTypeId, $documentNumber, $medicalInsuranceId);
    }

    public function storeNewCheckingAccount(StoreNewCheckingAccountRequest $request)
    {
        return AmbulatoryPatientsCheckingAccountService::storeNewCheckingAccount($request);
    }
}
