<?php

namespace CloudMed\Services\BillingDepartment;

use Carbon\Carbon;
use CloudMed\Imports\BillingDepartment\MedicalInsuranceAgreements\PracticesAgreementImport;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use CloudMed\Models\BillingDepartment\MedicalInsuranceAgreement;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Facades\Excel;
use Yoeunes\Toastr\Facades\Toastr;
use Yajra\DataTables\Facades\DataTables;

class MedicalInsuranceAgreementsService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(MedicalInsuranceAgreement::orderBy('created_at', 'ASC')->orderBy('medical_insurance_id', 'ASC'))
            ->addColumn('actions', function(MedicalInsuranceAgreement $row) {
                return view('BillingDepartment.MedicalInsuranceAgreements.dataTableActions')->with('id', $row->id);
            })
            ->editColumn('nomenclator_id', function(MedicalInsuranceAgreement $row) {
                return $row->nomenclator->name;
            })
            ->editColumn('medical_insurance_id', function(MedicalInsuranceAgreement $row) {
                return $row->medicalInsurance->name;
            })
            ->editColumn('valid_from', function(MedicalInsuranceAgreement $row) {
                return Carbon::parse($row->valid_from)->format('d/m/Y');
            })
            ->editColumn('valid_until', function(MedicalInsuranceAgreement $row) {
                return is_null($row->valid_until) ? '' : Carbon::parse($row->valid_until)->format('d/m/Y');
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new agreement method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $medicalInsuranceAgreement = MedicalInsuranceAgreement::create([
            'description' => $request->description,
            'nomenclator_id' => $request->nomenclator_id,
            'medical_insurance_id' => $request->medical_insurance_id,
            'without_end_date' => $request->without_end_date,
            'valid_from' => $request->valid_from,
            'valid_until' => $request->valid_until
        ]);

        $rangesValues = $request->rangesValues;
        $cleanedRangesValues = [];
        foreach($rangesValues as $key => $range) {
            $cleanedRangesValues[$range['code']] = [
                'expense_unit_value' => $range['expense_unit_value'],
                'honorary_unit_value' => $range['honorary_unit_value']
            ];
        }
        $medicalInsuranceAgreement->rangesValuesAgreement()->sync($cleanedRangesValues);

        Toastr::success('Éxito al registrar el convenio.');

        return \Response::json("OK", 200);
    }

    /**
     * Edit an agreement method
     * @param $medicalInsuranceAgreementId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($medicalInsuranceAgreementId)
    {
        if(null == $agreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId)) {
            Toastr::error("Convenio no encontrado.");
            return redirect(route('medicalInsuranceAgreements.index'));
        }

        return view('BillingDepartment.MedicalInsuranceAgreements.edit')->with('agreement', $agreement);
    }

    /**
     * Update an agreement method
     * @param $medicalInsuranceAgreementId
     * @param $request
     * @return JsonResponse
     */
    public function update ($medicalInsuranceAgreementId , $request)
    {

        if(null == $agreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId)) {
            Toastr::error("Convenio no encontrado.");
            return redirect(route('medicalInsuranceAgreements.index'));
        }

        $agreement->update([
            'description' => $request->description,
            'nomenclator_id' => $request->nomenclator_id,
            'medical_insurance_id' => $request->medical_insurance_id,
            'without_end_date' => $request->without_end_date,
            'valid_from' => $request->valid_from,
            'valid_until' => $request->valid_until
        ]);

        $rangesValues = $request->rangesValues;

        $cleanedRangesValues = [];

        foreach($rangesValues as $key => $range) {
            $cleanedRangesValues[$range['code']] = [
                'expense_unit_value' => $range['expense_unit_value'],
                'honorary_unit_value' => $range['honorary_unit_value']
            ];
        }

        $agreement->rangesValuesAgreement()->sync($cleanedRangesValues);

        Toastr::success('Convenio actualizado con éxito.');

        return \Response::json("OK", 200);

    }

    /**
     * Delete agreement method
     * @param $medicalInsuranceAgreementId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($medicalInsuranceAgreementId)
    {
        if(null == $agreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId)) {
            Toastr::error("Convenio no encontrado.");
            return redirect(route('medicalInsuranceAgreements.index'));
        }

        $agreement->delete();

        Toastr::success('Convenio  eliminado con éxito.');

        return redirect(route('medicalInsuranceAgreements.index'));

    }

    public function uploadPracticesAgreement($medicalInsuranceAgreementId, $request)
    {
        $file = $request->file('practicesAgreementFile');

        $path = 'AppData/BillingDepartment/MedicalInsuranceAgreements/' . $medicalInsuranceAgreementId;

        $fileName = Carbon::now()->format('d-m-Y_H-i') . '.' . $file->getClientOriginalExtension();

        \Storage::putFileAs( $path, $file, $fileName);

        Excel::import(new PracticesAgreementImport($medicalInsuranceAgreementId), storage_path('app/' . $path . '/' . $fileName));

        return \Response::json("OK", 200);
    }

    public function practicesAgreementDataTable($medicalInsuranceAgreementId)
    {
        $agreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId);
        return DataTables::of($agreement->practicesValuesAgreement())
            ->addColumn('actions', function(NomenclatorPractice $row) {
                return "<button id='deleteRow' type='button' class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>";
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function deleteSinglePracticeAgreement($medicalInsuranceAgreementId, $request)
    {
        $agreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId);
        $practice = NomenclatorPractice::where('code', $request->code)->get();
        $practice = $practice[0];
        $agreement->practicesValuesAgreement()->detach($practice->id);

        return \Response::json("OK", 200);
    }

    public function storePracticeAgreement($medicalInsuranceAgreementId, $request)
    {
        $medicalInsuranceAgreement = MedicalInsuranceAgreement::find($medicalInsuranceAgreementId);

        $practice = NomenclatorPractice::find($request->nomenclator_practice_id);

        $medicalInsuranceAgreement->practicesValuesAgreement()->attach($practice->id, [
            'fixed_expense_value' => $request->fixed_expense_value,
            'fixed_honorary_value' => $request->fixed_honorary_value,
        ]);

        return \Response::json("OK", 200);
    }
}