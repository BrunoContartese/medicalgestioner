<?php

namespace CloudMed\Services\MedicalDepartments\PathologicalAnatomy;

use App\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyPapanicolaousTemplate;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyStudy;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class PathologicalAnatomyService
{
    public function dataTable()
    {

        $datatable = DataTables::eloquent(PathologicalAnatomyStudy::with('institution', 'medicalDepartment', 'pathologicalAnatomyType', 'patientCheckingAccount', 'patientCheckingAccount.patient'))
                            ->addColumn('actions', function(PathologicalAnatomyStudy $row) {
                                return view('MedicalDepartments.PathologicalAnatomy.IndexPartials.DataTableActions')->with(['id' => $row->id, 'row' => $row]);
                            })
                            ->editColumn('received_at', function(PathologicalAnatomyStudy $row) {
                                return view('MedicalDepartments.PathologicalAnatomy.IndexPartials.DataTableReceptionDateField')->with('row', $row);
                            })
                            ->rawColumns(['actions'])
                            ->make(true);

        return $datatable;
    }

    public function destroy($pathologicalAnatomyStudyId)
    {
        if(null == $pathologicalAnatomyStudy = PathologicalAnatomyStudy::find($pathologicalAnatomyStudyId)) {
            \Toastr::error("Estudio no encontrado.");
            return redirect('/medicalDepartments/pathologicalAnatomy');
        }

        $pathologicalAnatomyStudy->delete();

        \Toastr::success('Estudio  eliminado con éxito.');

        return redirect('/medicalDepartments/pathologicalAnatomy');
    }

    public function store($request)
    {
        $input = $request->all();
        $input['received_at'] = Carbon::now();

        $study = PathologicalAnatomyStudy::create($input);

        $studyCode = $study->medicalDepartment->prefix . $study->pathologicalAnatomyType->prefix . $study->id;

        $study->update([
            'code' => $studyCode
        ]);

        return \Response::json("/medicalDepartments/pathologicalAnatomy/printSticker/". $studyCode, 200);
    }

    public function printSticker($studyCode)
    {
        $pdf = SnappyPdf::loadView('MedicalDepartments.PathologicalAnatomy.sticker', [
            'code' => $studyCode
        ]);

        $pdf->setOptions([
            'page-width' => '60mm',
            'page-height' => '30mm',
            'margin-top'    => 5,
            'margin-right'  => 0,
            'margin-bottom' => 0,
            'margin-left'   => 0,
        ]);

        return $pdf->inline('EtiquetaPatologica.pdf');
    }


    public function dataTableForMedicalReport()
    {
        $datatable = DataTables::eloquent(PathologicalAnatomyStudy::whereNotNull('received_at')->with('institution', 'medicalDepartment', 'pathologicalAnatomyType', 'patientCheckingAccount', 'patientCheckingAccount.patient'))
            ->addColumn('actions', function(PathologicalAnatomyStudy $row) {
                return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.DataTablePartials.DataTableActions')->with(['row' => $row]);
            })
            ->addColumn('billingCodes', function(PathologicalAnatomyStudy $row) {
                return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.DataTablePartials.DataTableBillingCodesButton')->with(['row' => $row]);
            })
            ->addColumn('categorizeSample', function(PathologicalAnatomyStudy $row) {
                return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.DataTablePartials.CategorizeSampleButton')->with(['row' => $row]);
            })
            ->addColumn('writeMedicalReport', function(PathologicalAnatomyStudy $row) {
                return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.DataTablePartials.WriteMedicalReportButton')->with(['row' => $row]);
            })
            ->editColumn('received_at', function(PathologicalAnatomyStudy $row) {
                return view('MedicalDepartments.PathologicalAnatomy.MedicalReport.DataTablePartials.DataTableReceptionDateField')->with('row', $row);
            })
            ->editColumn('created_at', function(PathologicalAnatomyStudy $row) {
                return Carbon::parse($row->created_at)->format('d/m/Y');
            })
            ->rawColumns(['actions'])
            ->make(true);

        return $datatable;
    }

    public function syncBillingCodesWithSample($sampleId, $request)
    {
        if(null == $sample = PathologicalAnatomyStudy::find($sampleId)) {
            return \Response::json(["errors" => ["La muestra seleccionada no existe en la base de datos."]], 422);
        }

        $sample->billingCodes()->detach();

        foreach($request->practices as $practice) {
            $sample->billingCodes()->attach($practice['id']);
        }

        return \Response::json("OK", 200);
    }

    public function syncCategoriesWithSample($sampleId, $request)
    {
        if(null == $sample = PathologicalAnatomyStudy::find($sampleId))
        {
            return \Response::json(["errors" => ["Muestra no encontrada."]], 422);
        }

        $sample->update([
            'pathological_anatomy_category_level_one_id' => $request->levelOneCategoryId,
            'pathological_anatomy_category_level_two_id' => $request->levelTwoCategoryId,
            'pathological_anatomy_category_level_three_id' => $request->levelThreeCategoryId,
            'pathological_anatomy_category_level_four_id' => $request->levelFourCategoryId,
        ]);

        return \Response::json("OK", 200);

    }

    public function getPapanicolaouTemplateDescription($templateId)
    {
        if(null == $template = PathologicalAnatomyPapanicolaousTemplate::find($templateId))
        {
            return \Response::json(["errors" => ["Plantilla de papanicolaou no encontrada."]], 422);
        }

        return $template->description;
    }
}
