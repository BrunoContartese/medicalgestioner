<?php

namespace CloudMed\Services\Pharmacy;
use Carbon\Carbon;
use CloudMed\Jobs\Pharmacy\UpdateMedicamentsJob;
use CloudMed\Models\Pharmacy\Medicament;
use CloudMed\Models\Pharmacy\MedicamentAdministrationLine;
use CloudMed\Models\Pharmacy\MedicamentDrug;
use CloudMed\Models\Pharmacy\MedicamentPharmacologicalAction;
use CloudMed\Models\Pharmacy\MedicamentUpdateLog;
use CloudMed\Models\Pharmacy\MedicamentUpdateQueue;
use CloudMed\Models\Pharmacy\MedicamentUpdateStatus;
use CloudMed\Models\Pharmacy\PharmaceuticalLaboratory;
use Illuminate\Support\Facades\Log;
use Storage;
use Yoeunes\Toastr\Facades\Toastr;
use ZipArchive;

class MedicamentsUpdateService
{
    public $updateQueue;

    public function uploadAlphaBetaZipFile($file)
    {
        $updateQueueId = null;
        /**
         * First, we extract files
         */
        if($this->unzipFile($file) == 1) {

            $updateQueueId = $this->addUpdateToQueue();

            $updateMedicamentJob = new UpdateMedicamentsJob($updateQueueId);
            dispatch($updateMedicamentJob);

        } else {
            $this->finishQueueWithFail($updateQueueId);
            return \Response::json(['errors' => ['Hubo un problema de sistema. Por favor contáctese con el departamento de sistemas.']], 422);
        }
    }

    /**
     * @param $file
     * @return int
     */
    private function unzipFile($file)
    {
        $zip = new ZipArchive();
        $res = $zip->open($file);
        if ($res === TRUE) {
            $zip->extractTo(base_path("storage/app/public/alphabetha/updates/" . Carbon::now()->format('dmY')));
            $zip->close();
            return 1;
        }
        return 0;
    }

    public function processTxtFiles($updateQueueId)
    {
        set_time_limit(0);

        $path = "public/alphabetha/updates/" . Carbon::now()->format('dmY');

        try {
            $this->createOrUpdateDrugs($path);

            $this->createOrUpdateAdministrationLines($path);

            $this->createOrUpdatePharmacologicalActions($path);

            $this->createOrUpdateMedicaments($path);

            $this->addExtraInformationToMedicament($path);

            $this->finishQueue($updateQueueId);


        } catch( \Exception $exception) {
            Log::error("Hubo un error al actualizar la base de datos de medicamentos. Queue ID: " . $updateQueueId );
            Log::error($exception);
            $this->finishQueueWithFail($updateQueueId);

        }

    }

    private function addUpdateToQueue()
    {
         $updateQueue = MedicamentUpdateQueue::create([
            'user_id' => \Auth::user()->id,
            'medicament_update_status_id' => 2
        ]);

         return $updateQueue->id;

    }

    private function finishQueue($queueId)
    {
        $updateQueue = MedicamentUpdateQueue::find($queueId);

        $updateQueue->update([
            'medicament_update_status_id' => 1
        ]);
    }

    private function finishQueueWithFail($queueId)
    {
        $updateQueue = MedicamentUpdateQueue::find($queueId);

        $updateQueue->update([
            'medicament_update_status_id' => 3
        ]);
    }

    private function createOrUpdateDrugs($path)
    {
        /*Obtengo el archivo*/
        $file = Storage::get($path . '/monodro.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13).chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $drugs = $this->splitDrugCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $drugs = $this->trimDrugsWhiteSpaces($drugs);

        /*Guardo en la base de datos*/
        $this->storeDrugs($drugs);
    }

    private function splitDrugCodesAndDescriptions($lines)
    {
        $drugs = [];
        foreach($lines as $key => $val)
        {
            array_push($drugs, array(
                'code' => substr($val,0,5),
                'description' => substr($val,5,32)
            ));
        }
        return $drugs;
    }

    private function trimDrugsWhiteSpaces($drugs)
    {
        foreach($drugs as $drug) {
            /*Saco los espacios en blanco*/
            $drug['description'] = trim($drug['description'], " \t\n\r\0\x0B");
        }

        return $drugs;
    }

    private function storeDrugs($drugs)
    {
        foreach($drugs as $drug)
        {
            /*Guardo o actualizo la base de datos según corresponda*/
            if($drug['code'] != '') {
                if(!$this->checkDrugExistence($drug['code'])) {
                    MedicamentDrug::create(array(
                        'code' => $drug['code'],
                        'description' => mb_convert_encoding($drug['description'], 'UTF-8')
                    ));
                } else {
                    MedicamentDrug::where('code', $drug['code'])->update([
                        'description' => mb_convert_encoding($drug['description'], 'UTF-8')
                    ]);
                }

            }
        }
    }

    private function checkDrugExistence($drugCode)
    {
        $drug = MedicamentDrug::where('code', $drugCode)->get();
        return count($drug) > 0 ? 1 : 0;
    }

    private function createOrUpdateAdministrationLines($path)
    {
        /*Obtengo el archivo*/
        $file = Storage::get($path . '/vias.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13) . chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $administrationLines = $this->splitAdministrationLinesCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $administrationLines = $this->trimAdministrationLinesWhiteSpaces($administrationLines);

        /*Guardo en la base de datos*/
        $this->storeAdministrationLines($administrationLines);
    }

    private function splitAdministrationLinesCodesAndDescriptions($lines)
    {
        $administrationLines = [];

        foreach ($lines as $key => $val) {
            array_push($administrationLines, array(
                'code' => (int) substr($val, 0, 5),
                'description' => substr($val, 5, 50)
            ));
        }

        return $administrationLines;
    }

    private function trimAdministrationLinesWhiteSpaces($administrationLines)
    {
        foreach($administrationLines as $administrationLine)
        {
            /*Saco los espacios en blanco*/
            $administrationLine['description'] = trim($administrationLine['description'], " \t\n\r\0\x0B" );
        }

        return $administrationLines;
    }

    private function storeAdministrationLines($administrationLines)
    {
        foreach ($administrationLines as $administrationLine) {
            /*Guardo o actualizo en la base de datos segun corresponda*/
            if ($administrationLine['code'] != '') {
                if(!$this->checkAdministrationLineExistence($administrationLine['code'])) {
                    MedicamentAdministrationLine::create([
                        'code' => $administrationLine['code'],
                        'description' => mb_convert_encoding($administrationLine['description'], 'UTF-8'),
                    ]);
                } else {
                    MedicamentAdministrationLine::where('code', $administrationLine['code'])->update([
                        'description' => mb_convert_encoding($administrationLine['description'], 'UTF-8'),
                    ]);
                }
            }
        }
    }

    private function checkAdministrationLineExistence($code)
    {
        $administrationLine = MedicamentAdministrationLine::where('code', $code)->get();

        return count($administrationLine) > 0 ? 1 : 0;
    }

    private function createOrUpdatePharmacologicalActions($path)
    {
        /*Obtengo el archivo*/
        $file = Storage::get($path . '/acciofar.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13) . chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $pharmacologicalAction = $this->splitPharmacologicalActionsCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $pharmacologicalAction = $this->trimPharmacologicalActionsWhiteSpaces($pharmacologicalAction);

        /*Guardo en la base de datos*/
        $this->storePharmacologicalActions($pharmacologicalAction);
    }

    private function splitPharmacologicalActionsCodesAndDescriptions($lines)
    {
        $pharmacologicalActions = [];

        foreach ($lines as $key => $val) {
            array_push($pharmacologicalActions, array(
                'code' => (int) substr($val, 0, 5),
                'description' => substr($val, 5, 50)
            ));
        }
        return $pharmacologicalActions;
    }

    private function trimPharmacologicalActionsWhiteSpaces($pharmacologicalActions)
    {
        foreach($pharmacologicalActions as $pharmacologicalAction)
        {
            /*Saco los espacios en blanco*/
            $pharmacologicalAction['description'] = trim($pharmacologicalAction['description'], " \t\n\r\0\x0B" );
        }

        return $pharmacologicalActions;
    }

    private function storePharmacologicalActions($pharmacologicalActions)
    {
        foreach ($pharmacologicalActions as $pharmacologicalAction) {
            /*Guardo o actualizo en la base de datos segun corresponda*/
            if ($pharmacologicalAction['code'] != '') {
                if(!$this->checkPharmacologicalActionExistence($pharmacologicalAction['code'])) {
                    MedicamentPharmacologicalAction::create([
                        'code' => $pharmacologicalAction['code'],
                        'description' => mb_convert_encoding($pharmacologicalAction['description'], 'UTF-8'),
                    ]);
                } else {
                    MedicamentPharmacologicalAction::where('code', $pharmacologicalAction['code'])->update([
                        'description' => mb_convert_encoding($pharmacologicalAction['description'], 'UTF-8'),
                    ]);
                }
            }
        }
    }

    private function checkPharmacologicalActionExistence($code)
    {
        $pharmacologicalAction = MedicamentPharmacologicalAction::where('code', $code)->get();
        return count($pharmacologicalAction) > 0 ? 1: 0;
    }

    private function createOrUpdateMedicaments($path)
    {
        /*Obtengo el archivo*/
        $file = Storage::get($path . '/manual.dat');

        /*Separo cada linea*/
        $lines = explode(chr(13).chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $medicaments = $this->splitMedicamentsCodesAndDescriptions($lines);

        /*Quito los espacios en blanco*/
        $medicaments = $this->trimMedicamentsWhiteSpaces($medicaments);

        /*Guardo en la base de datos*/
        $this->storeMedicaments($medicaments);
    }

    private function splitMedicamentsCodesAndDescriptions($lines)
    {
        $medicaments = [];
        foreach($lines as $key => $val)
        {
            array_push($medicaments, array(
                'label' => substr($val,0,7),
                'name' => substr($val,7,44),
                'presentation' => substr($val,51,24),
                'laboratory_name' => substr($val,85,16),
                'sell_price' => ((float) substr($val,101,9) / 100),
                'laboratory_code' => $this->checkLaboratoryExistence(intval(substr($val,123,3)), substr($val,85,16)),
                'alphabeta_medicament_id' => (int) substr($val,126,5),
                'is_active' => (int) substr($val,131,1),
                'barcode' => substr($val,132,13),
                'units' => (int) substr($val,145,4),
            ));
        }
        return $medicaments;
    }

    private function checkLaboratoryExistence($laboratoryCode, $laboratoryName)
    {
        $laboratory = PharmaceuticalLaboratory::where('code', $laboratoryCode)->get();

        if(count($laboratory) < 1) {
            $this->createLaboratory($laboratoryCode, $laboratoryName);
        }

        return $laboratoryCode;
    }

    private function createLaboratory($laboratoryCode, $laboratoryName)
    {
        $laboratoryName = trim($laboratoryName);
        $laboratoryName = mb_convert_encoding($laboratoryName, 'UTF-8');

        PharmaceuticalLaboratory::create([
            'name' => $laboratoryName,
            'code' => $laboratoryCode
        ]);
    }

    private function trimMedicamentsWhiteSpaces($medicaments)
    {
        foreach($medicaments as $medicament) {
            /*Saco los espacios en blanco*/
            $medicament['label'] = trim($medicament['label']);
            $medicament['name'] = trim($medicament['name']);
            $medicament['presentation'] = trim($medicament['presentation']);
            $medicament['laboratory_name'] = trim($medicament['laboratory_name']);
            $medicament['barcode'] = trim($medicament['barcode']);
        }

        return $medicaments;
    }

    private function storeMedicaments($medicaments)
    {
        foreach($medicaments as $medicament)
        {
            /*Guardo en la base de datos*/
            if($medicament['label'] != '') {
                if(!$this->checkMedicamentExistence($medicament['alphabeta_medicament_id'])) {
                    $this->createMedicament($medicament);
                } else {
                    $this->updateMedicament($medicament);
                }
            }
        }
    }

    private function checkMedicamentExistence($alphaBethaMedicamentId)
    {
        $medicament = Medicament::where('alphabeta_medicament_id', $alphaBethaMedicamentId)->get();

        return count($medicament) > 0 ? 1 : 0;
    }

    private function createMedicament($medicament)
    {
        Medicament::create(array(
            'label' => mb_convert_encoding($medicament['label'], 'UTF-8'),
            'name' => mb_convert_encoding($medicament['name'], 'UTF-8'),
            'presentation' => mb_convert_encoding($medicament['presentation'], 'UTF-8'),
            'laboratory_name' => mb_convert_encoding($medicament['laboratory_name'], 'UTF-8'),
            'sell_price' => $medicament['sell_price'],
            'laboratory_code' => $medicament['laboratory_code'],
            'alphabeta_medicament_id' => $medicament['alphabeta_medicament_id'],
            'is_active' => $medicament['is_active'],
            'barcode' => mb_convert_encoding($medicament['barcode'], 'UTF-8'),
            'units' => $medicament['units'],
        ));
    }

    private function updateMedicament($medicament)
    {
        Medicament::where('alphabeta_medicament_id', $medicament['alphabeta_medicament_id'])->update(array(
            'label' => mb_convert_encoding($medicament['label'], 'UTF-8'),
            'name' => mb_convert_encoding($medicament['name'], 'UTF-8'),
            'presentation' => mb_convert_encoding($medicament['presentation'], 'UTF-8'),
            'laboratory_name' => mb_convert_encoding($medicament['laboratory_name'], 'UTF-8'),
            'sell_price' => $medicament['sell_price'],
            'laboratory_code' => $medicament['laboratory_code'],
            'is_active' => $medicament['is_active'],
            'barcode' => mb_convert_encoding($medicament['barcode'], 'UTF-8'),
            'units' => $medicament['units'],
        ));
    }

    private function addExtraInformationToMedicament($path)
    {
        /*Obtengo el archivo*/
        $file = Storage::get($path . '/manextra.txt');

        /*Separo cada linea*/
        $lines = explode(chr(13).chr(10), $file);

        /*Separo los codigos de la descripcion*/
        $medicaments = $this->splitMedicamentExtraInformationCodes($lines);

        /*Guardo en la base de datos*/
        $this->storeMedicamentExtraInformation($medicaments);
    }

    private function splitMedicamentExtraInformationCodes($lines)
    {
        $medicaments = [];
        foreach($lines as $key => $val)
        {
            array_push($medicaments, array(
                'alphabeta_medicament_id' => (int) substr($val,0,5),
                'medicament_drug_code' => (int) substr($val,12,5),
                'medicament_pharmacological_action_code' => (int) substr($val,7,5),
                'medicament_administration_line_code' => (int) substr($val,48,5),

            ));
        }
        return $medicaments;
    }

    private function storeMedicamentExtraInformation($medicaments)
    {
        foreach($medicaments as $medicament)
        {
            /*Guardo en la base de datos*/
            if($medicament['alphabeta_medicament_id'] != '') {
                if(count(Medicament::where('alphabeta_medicament_id', $medicament['alphabeta_medicament_id'])->get()) > 0) {
                        Medicament::where('alphabeta_medicament_id', $medicament['alphabeta_medicament_id'])->update([
                                            'medicament_pharmacological_action_code' => $medicament['medicament_pharmacological_action_code'],
                                            'medicament_drug_code' => $medicament['medicament_drug_code'],
                                            'medicament_administration_line_code' => $medicament['medicament_administration_line_code'],
                                        ]);
                }

            }
        }
    }
}