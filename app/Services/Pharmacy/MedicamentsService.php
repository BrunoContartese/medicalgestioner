<?php

namespace CloudMed\Services\Pharmacy;

use Carbon\Carbon;
use CloudMed\Models\Pharmacy\Medicament;
use CloudMed\Models\Pharmacy\MedicamentUpdateQueue;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class MedicamentsService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Medicament::orderBy('name', 'ASC')->orderBy('laboratory_code', 'ASC'))
            ->editColumn('laboratory_code', function(Medicament $row) {
                return isset($row->laboratory->name) ? $row->laboratory->name : "N / D";
            })
            ->editColumn('medicament_drug_code', function(Medicament $row) {
                return isset($row->drug->description) ? $row->drug->description : "N / D";
            })
            ->editColumn('on_stock', function(Medicament $row) {
                return view('Pharmacy.Medicaments.TableButtons.OnStock')->with('row', $row);
            })
            ->addColumn('actions', function(Medicament $row) {
                return view('Pharmacy.Medicaments.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function medicamentUpdatesDataTable()
    {
        return DataTables::of(MedicamentUpdateQueue::orderBy('created_at', 'DESC'))
            ->editColumn('user_id', function(MedicamentUpdateQueue $row) {
                return $row->user->name;
            })
            ->editColumn('medicament_update_status_id', function(MedicamentUpdateQueue $row) {
                return view('Pharmacy.Medicaments.medicamentsUpdate.updateStatusLabel')->with('id', $row->status->id);
            })
            ->editColumn('created_at', function(MedicamentUpdateQueue $row) {
                return Carbon::parse($row->created_at)->format('d/m/Y');
            })
            ->addColumn('time', function(MedicamentUpdateQueue $row) {
                return Carbon::parse($row->created_at)->diffInMinutes($row->updated_at);
            })
            ->rawColumns(['medicament_update_status_id'])
            ->make(true);
    }

    public function edit($medicamentId)
    {
        if(null == $medicament = Medicament::find($medicamentId)) {
            Toastr::error("El medicamento a editar no es válido.");
            return redirect(route('medicaments.index'));
        }

        return view('Pharmacy.Medicaments.edit')->with(['medicament' => $medicament]);
    }
}