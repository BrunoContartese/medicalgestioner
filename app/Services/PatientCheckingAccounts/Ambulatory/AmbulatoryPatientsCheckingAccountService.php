<?php

namespace CloudMed\Services\PatientCheckingAccounts\Ambulatory;
use Carbon\Carbon;
use CloudMed\Models\Administration\Patient;
use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use Yajra\DataTables\Facades\DataTables;

class AmbulatoryPatientsCheckingAccountService
{
    public function getAmbulatoryCheckingAccountFinalStepInformation($documentType, $documentNumber, $medicalInsuranceId)
    {
        $patient = Patient::where('document_type_id', $documentType)
            ->where('document_number', $documentNumber)
            ->get();

        $patient = $patient[0];

        return view('PatientCheckingAccounts.Ambulatory.partials.WizardHelpers.AmbulatoryCheckingAccountFinalStepInformation')->with(['patient' => $patient, 'medicalInsuranceId' => $medicalInsuranceId]);
    }

    public function storeNewCheckingAccount($request)
    {
        $patient = Patient::where('document_type_id', $request->document_type_id)->where('document_number', $request->document_number)->get();

        $patient = $patient[0];

        /*Pensar en posible race condition por el número de rendición*/
        $code = PatientCheckingAccount::wherePatientCheckingAccountTypeId(1)->max('checking_account_number') + 1;

        $checkingAccount = PatientCheckingAccount::firstOrCreate(['checking_account_number' => $code], [
                            'patient_id' => $patient->id,
                            'patient_checking_account_status_id' => 1,
                            'patient_checking_account_type_id' => 1,
                        ]);

        while(!$checkingAccount->wasRecentlyCreated) {
            $code++;

            $checkingAccount = PatientCheckingAccount::firstOrCreate(['checking_account_number' => $code],[
                'patient_id' => $patient->id,
                'patient_checking_account_status_id' => 1,
                'patient_checking_account_type_id' => 1,
                'checking_account_number' => $code
            ]);

        }

        $patientMedicalInsurance = $patient->medicalInsurances()->where('medical_insurance_id', $request->medical_insurance_id)->get();

        $patientMedicalInsurance = $patientMedicalInsurance[0];

        $checkingAccount->checkingAccountBillingInformation()->attach($request->medical_insurance_id);

        return \Response::json("OK", 200);
    }

}
