<?php

namespace CloudMed\Services\PatientCheckingAccounts;

use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;

class PatientCheckingAccountsService
{
    public function index()
    {


        return view('PatientCheckingAccounts.index')->with([
            'openedCheckingAccounts' => $this->getOpenedCheckingAccounts(),
            'openedAmbulatoryCheckingAccounts' =>$this->getOpenedAmbulatoryCheckingAccounts(),
            'openedInternationCheckingAccounts' =>$this->getOpenedInternationCheckingAccounts(),
        ]);
    }

    private function getOpenedCheckingAccounts()
    {
        return PatientCheckingAccount::where('patient_checking_account_status_id', 1)->count();
    }

    private function getOpenedAmbulatoryCheckingAccounts()
    {
        return PatientCheckingAccount::where('patient_checking_account_status_id', 1)->where('patient_checking_account_type_id', 1)->count();
    }

    private function getOpenedInternationCheckingAccounts()
    {
        return PatientCheckingAccount::where('patient_checking_account_status_id', 1)->where('patient_checking_account_type_id', 2)->count();
    }

    public function dataTable()
    {
        return DataTables::eloquent(PatientCheckingAccount::with('checkingAccountStatus', 'patient')->orderBy('created_at', 'DESC'))
            ->editColumn('created_at', function(PatientCheckingAccount $row) {
                return Carbon::parse($row->created_at)->format('d/m/Y H:i');
            })
            ->editColumn('patient_checking_account_type_id', function(PatientCheckingAccount $row) {
                return $row->checkingAccountType->name;
            })
            ->editColumn('patient_checking_account_status_id', function(PatientCheckingAccount $row) {
                return $row->checkingAccountStatus->name;
            })
            ->addColumn('medical_insurance_id', function(PatientCheckingAccount $row) {
                return $row->checkingAccountBillingInformation[0]->name;
            })
            ->addColumn('patient_document_type', function(PatientCheckingAccount $row) {
                return $row->patient->documentType->name;
            })
            ->addColumn('patient_document_number', function(PatientCheckingAccount $row) {
                return $row->patient->document_number;
            })
            ->addColumn('patient_name', function(PatientCheckingAccount $row) {
                return $row->patient->name;
            })
            ->make(true);
    }
}
