<?php
namespace CloudMed\Services\HumanResourcesDepartment;

use CloudMed\Models\HumanResourcesDepartment\EmployeeCategory;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class EmployeesCategoriesService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(EmployeeCategory::orderBy('name', 'ASC'))
            ->addColumn('actions', function(EmployeeCategory $row) {
                return view('HumanResourcesDepartment.EmployeesCategories.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new agreement method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $employeeCategory = EmployeeCategory::create($input);

        Toastr::success('Éxito al registrar la categoría de empleado.');

        return redirect(route('employeesCategories.index'));
    }

    /**
     * Edit an agreement method
     * @param $medicalInsuranceAgreementId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($employeeCategoryId)
    {
        if(null == $category = EmployeeCategory::find($employeeCategoryId)) {
            Toastr::error("Categoría de trabajo no encontrada.");
            return redirect(route('employeesCategories.index'));
        }

        return view('HumanResourcesDepartment.EmployeesCategories.edit')->with('category', $category);
    }

    /**
     * Update an agreement method
     * @param $medicalInsuranceAgreementId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ($employeeCategoryId , $request)
    {

        if(null == $category = EmployeeCategory::find($employeeCategoryId)) {
            Toastr::error("Categoría de trabajo no encontrada.");
            return redirect(route('employeesCategories.index'));
        }

        $input = $request->all();

        $category->update($input);

        Toastr::success('Categoría de trabajo actualizada con éxito.');

        return redirect(route('employeesCategories.index'));

    }

    /**
     * Delete agreement method
     * @param $medicalInsuranceAgreementId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($employeeCategoryId)
    {
        if(null == $category = EmployeeCategory::find($employeeCategoryId)) {
            Toastr::error("Categoría de trabajo no encontrada.");
            return redirect(route('employeesCategories.index'));
        }

        $category->delete();

        Toastr::success('Categoría de trabajo eliminada con éxito.');

        return redirect(route('employeesCategories.index'));

    }
}