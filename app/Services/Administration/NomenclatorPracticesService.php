<?php
/**
 * Created by PhpStorm.
 * User: bcontartese
 * Date: 23/07/19
 * Time: 21:28
 */

namespace CloudMed\Services\Administration;



use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use Yoeunes\Toastr\Facades\Toastr;
use Yajra\DataTables\Facades\DataTables;

class NomenclatorPracticesService
{

    public function practicesList($nomenclatorId)
    {

        if(null == $nomenclator = Nomenclator::find($nomenclatorId)) {
            Toastr::error("Nomenclator no encontrado.");
            return redirect(route('nomenclators.index'));
        }

        return view('Administration.Nomenclators.Practices.index')->with(['nomenclator' => $nomenclator]);
    }

    public function dataTable($nomenclatorId)
    {

        $datatable = DataTables::of(NomenclatorPractice::where('nomenclator_id', $nomenclatorId))
                ->editColumn('is_module', function(NomenclatorPractice $row) {
                   return $row->is_module == true ? '<b>SI</b>' : '<b>NO</b>';
                })
                ->addColumn('actions', function(NomenclatorPractice $row) {
                    return view('Administration.Nomenclators.Practices.dataTableActions')->with('row', $row);
                })
                ->rawColumns(['is_module', 'actions'])
                ->make(true);

        return $datatable;
    }

    public function store($nomenclatorId, $request)
    {
        $input = $request->all();

        $input['nomenclator_id'] = $nomenclatorId;

        $nomenclatorPractice = NomenclatorPractice::create($input);

        if($nomenclatorPractice) {
            Toastr::success('Práctica creada con éxito');
        } else {
            Toastr::error("Ocurrió un error al crear la práctica");
        }

        return redirect(url('administration/nomenclatorPractices', $nomenclatorId));

    }

    public function update($nomenclatorPracticeId, $request)
    {
        $input = $request->all();

        if(null == $nomenclatorPractice = NomenclatorPractice::find($nomenclatorPracticeId)) {
            Toastr::error("Práctica no encontrada.");
            return redirect(route('nomenclators.index'));
        }

        $nomenclatorPractice->update($input);

        Toastr::success("Práctica " . $nomenclatorPractice->code . " modificada con éxito");

        return redirect(url('administration/nomenclatorPractices', $nomenclatorPractice->nomenclator->id));
    }

    public function getInfoById($nomenclatorPracticeId)
    {
        $practice = NomenclatorPractice::find($nomenclatorPracticeId);

        return \Response::json($practice, 200);
    }

    public function getInfoByCode($nomenclatorPracticeCode)
    {
        $practice = NomenclatorPractice::whereCodeAndNomenclatorId($nomenclatorPracticeCode, 1)->get();
        if(count($practice) < 1) {
            return \Response::json(["errors" => ["El código ingresado no existe en la base de datos."]], 422);
        }

        return \Response::json($practice[0], 200);
    }

}