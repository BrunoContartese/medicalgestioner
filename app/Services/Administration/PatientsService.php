<?php

namespace CloudMed\Services\Administration;

use Carbon\Carbon;
use CloudMed\Models\Administration\Patient;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class PatientsService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Patient::orderBy('name', 'ASC'))
            ->editColumn('document_type', function(Patient $row) {
                return $row->documentType->name;
            })
            ->addColumn('more_info', function(Patient $row) {
                return "<button class='btn btn-warning' onclick='showPatientInfo(" . $row->id .");' type='button'> <i class='fa fa-info-circle'></i></button>";
            })
            ->addColumn('actions', function(Patient $row) {
                return view('Administration.Patients.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions','more_info'])
            ->make(true);
    }

    /**
     * Store new patient method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $input['medicalInsurancesTable'] = '';

        $patient = Patient::create($input);

        $this->syncMedicalInsurances($request->medicalInsurancesTable, $patient);

        Toastr::success('Éxito al registrar el paciente.');

        return Response::json("OK", 200);
    }

    private function syncMedicalInsurances($medicalInsurancesTable, $patient)
    {
        $medicalInsurances = [];

        foreach($medicalInsurancesTable as $medicalInsurance)
        {
            $medicalInsurances[$medicalInsurance["medical_insurance_id"]] = array(
                'afilliate_number' => $medicalInsurance["afilliate_number"],
                'expiration_date' => Carbon::createFromFormat('d/m/Y',$medicalInsurance["expiration_date"])->format('Y-m-d'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        }

        $patient->medicalInsurances()->sync($medicalInsurances);
    }

    /**
     * Edit patient method
     * @param $patientId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($patientId)
    {
        if(null == $patient = Patient::find($patientId)) {
            Toastr::error("Paciente no encontrado.");
            return redirect(route('patients.index'));
        }

        return view('Administration.Patients.edit')->with('patient', $patient);
    }

    /**
     * Update a patient method
     * @param $patientId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ($patientId , $request)
    {
        $input = $request->all();

        $input['medicalInsurancesTable'] = '';

        if(null == $patient = Patient::find($patientId)) {
            Toastr::error("Paciente no encontrado.");
            return redirect(route('patients.index'));
        }

        $patient->update($input);

        $this->syncMedicalInsurances($request->medicalInsurancesTable, $patient);

        Toastr::success('Datos del paciente actualizados con éxito.');

        return Response::json("OK", 200);
    }

    public function show($patientId)
    {
        if(null == $patient = Patient::find($patientId)) {
            Toastr::error("Paciente no encontrado.");
            return redirect(route('patients.index'));
        }

        return view('Administration.Patients.show')->with(['patient' => $patient]);
    }

    public function findByDocumentNumber($documentType, $documentNumber)
    {
        $patient = Patient::where('document_type_id', $documentType)
                            ->where('document_number', $documentNumber)
                            ->get();

        if(count($patient) < 1) {
            return Response::json(["Error" => ['Paciente no encontrado']], 404);
        }

        return view('Administration.Patients.show')->with("patient", $patient[0]);
    }

    public function getPatientMedicalInsurances($documentType, $documentNumber)
    {
        $patient = Patient::where('document_type_id', $documentType)
            ->where('document_number', $documentNumber)
            ->get();

        if(count($patient[0]->medicalInsurances) < 1) {
            return Response::json(["Error" => ['Obra social no asignada']], 404);
        }

        return view('Administration.Patients.Helpers.PatientMedicalInsurancesSelect')->with("patient", $patient[0]);
    }

}
