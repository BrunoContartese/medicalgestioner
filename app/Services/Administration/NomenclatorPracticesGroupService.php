<?php

namespace CloudMed\Services\Administration;

use Carbon\Carbon;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPracticeGroup;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class NomenclatorPracticesGroupService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(NomenclatorPracticeGroup::orderBy('name', 'ASC'))
            ->addColumn('actions', function(NomenclatorPracticeGroup $row) {
                return view('Administration.NomenclatorPracticesGroup.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new nomenclatorPracticeGroup method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();


        $nomenclatorPracticeGroup = NomenclatorPracticeGroup::create($input);
        $nomenclatorPracticeGroup->nomenclatorPractices()->sync($request->nomenclator_practices_ids);

        Toastr::success('Éxito al registrar la practica.');

        return redirect(route('nomenclatorPracticesGroup.index'));
    }

    /**
     * Edit an nomenclatorPracticeGroup method
     * @param $nomenclatorPracticeGroupId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($nomenclatorPracticeGroupId)
    {
        if(null == $nomenclatorPracticeGroup = NomenclatorPracticeGroup::find($nomenclatorPracticeGroupId)) {
            Toastr::error("Grupo de practica no encontrado.");
            return redirect(route('nomenclatorPracticesGroup.index'));
        }

        return view('Administration.NomenclatorPracticesGroup.edit')->with('nomenclatorPracticeGroup', $nomenclatorPracticeGroup);
    }

    /**
     * Update an nomenclatorPracticeGroup method
     * @param $nomenclatorPracticeGroupId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ( $nomenclatorPracticeGroupId , $request)
    {
        $input = $request->all();

        if(null == $nomenclatorPracticeGroup = NomenclatorPracticeGroup::find($nomenclatorPracticeGroupId)) {
            Flash::error("Practica de grupo no encontrada.");
            return redirect(route('nomenclatorPracticesGroup.index'));
        }


        $nomenclatorPracticeGroup->update($input);

        $nomenclatorPracticeGroup->nomenclatorPractices()->sync($request->nomenclator_practices_ids);

        Toastr::success('Grupo de practicas actualizada con éxito.');

        return redirect(route('nomenclatorPracticesGroup.index'));

    }

    /**
     * Delete an nomenclatorPracticeGroup method
     * @param $nomenclatorPracticeGroupId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($nomenclatorPracticeGroupId)
    {
        if(null == $nomenclatorPracticeGroup = NomenclatorPracticeGroup::find($nomenclatorPracticeGroupId)) {
            Toastr::error("Grupo de practicas no encontrado.");
            return redirect(route('nomenclatorPracticesGroup.index'));
        }

        $nomenclatorPracticeGroup->delete();

        Toastr::success('Grupo de practicas eliminado con éxito.');
        return redirect(route('nomenclatorPracticesGroup.index'));

    }
}
