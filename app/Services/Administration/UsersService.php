<?php

namespace CloudMed\Services\Administration;

use CloudMed\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;

class UsersService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(User::orderBy('name', 'ASC'))
            ->addColumn('actions', function(User $row) {
                return view('Administration.Users.dataTableActions')->with('id', $row->id);
            })
            ->editColumn('last_login_at', function(User $row) {
                return Carbon::parse($row->last_login_at)->format('d/m/Y H:i');
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new user method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $input["password"] = bcrypt($input["password"]);

        $user = User::create($input);

        $user->syncRoles($request->role);

        Toastr::success('Éxito al registrar el usuario.');

        return redirect(route('users.index'));
    }

    /**
     * Edit an user method
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($userId)
    {
        if(null == $user = User::find($userId)) {
            Toastr::error("Usuario no encontrado.");
            return redirect(route('users.index'));
        }

        return view('Administration.Users.edit')->with('user', $user);
    }

    /**
     * Update an user method
     * @param $userId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($userId, $request)
    {
        $input = $request->all();

        if(null == $user = User::find($userId)) {
            Toastr::error("Usuario no encontrado.");
            return redirect(route('users.index'));
        }

        $input["password"] = bcrypt($input["password"]);

        $user->update($input);

        $user->syncRoles($request->role);

        \DB::table('sessions')->where('user_id', $user->id)->delete();

        Toastr::success('Usuario actualizado con éxito.');

        return redirect(route('users.index'));

    }

    /**
     * Delete an user method
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($userId)
    {
        if(null == $user = User::find($userId)) {
            Toastr::error("Usuario no encontrado.");
            return redirect(route('users.index'));
        }

        $user->delete();

        Toastr::success('Usuario eliminado con éxito.');
        return redirect(route('users.index'));

    }

    public function updateProfile($request)
    {

        if(!isset($request->password)) {
            \Auth::user()->update([
               'email' => $request->email
            ]);
        } else {
            \Auth::user()->update([
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            \DB::table('sessions')->where('user_id', \Auth::user()->id)->delete();
        }


        Toastr::success("Perfil actualizado con éxito.");
        return \Response::json("OK", 200);
    }

    public function updateProfileImage($request)
    {
        $file = $request->file('profileImageFile');

        $path = 'public/images/users/profileImages/';

        $fileName =  Hash::make(\Auth::user()->id) . '.' . $file->getClientOriginalExtension();

        User::whereId(\Auth::user()->id)->update([
            'profile_image_url' => '/storage/images/users/profileImages/' . $fileName
        ]);

        \Storage::putFileAs($path, $file, $fileName);
    }

    public function updateSignatureImage($request)
    {
        $file = $request->file('signatureImageFile');

        $path = 'public/images/users/profileSignatures/';

        $fileName = Hash::make(\Auth::user()->id) . '.' . $file->getClientOriginalExtension();

        User::whereId(\Auth::user()->id)->update([
            'signature_image_url' => '/storage/images/users/profileSignatures/' . $fileName
        ]);

        \Storage::putFileAs($path, $file, $fileName);
    }

    public function getSignature()
    {
        return \Auth::user()->signature_image_url;
    }
}
