<?php

namespace CloudMed\Services\Administration;

use Carbon\Carbon;
use CloudMed\Models\Administration\Deposit;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class DepositsService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Deposit::orderBy('name', 'ASC'))
            ->addColumn('actions', function(Deposit $row) {
                return view('Administration.Deposits.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new specialty method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $deposit = Deposit::create($input);

        Toastr::success('Éxito al registrar el deposito.');

        return redirect(route('deposits.index'));
    }

    /**
     * Edit an deposit method
     * @param $DepositId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($DepositId)
    {
        if(null == $Deposit = Deposit::find($DepositId)) {
            Toastr::error("Deposito no encontrado.");
            return redirect(route('deposits.index'));
        }

        return view('Administration.Deposits.edit')->with('Deposit', $Deposit);
    }

    /**
     * Update an specialty method
     * @param $DepositId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ( $DepositId , $request)
    {
        $input = $request->all();

        if(null == $Deposit = Deposit::find($DepositId)) {
            Flash::error("Deposito no encontrado.");
            return redirect(route('deposits.index'));
        }

        $Deposit->update($input);

        Toastr::success('Deposito actualizado con éxito.');

        return redirect(route('deposits.index'));

    }

    /**
     * Delete deposit method
     * @param $DepositId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($DepositId)
    {
        if(null == $deposit = Deposit::find($DepositId)) {
            Toastr::error("Deposito no encontrado.");
            return redirect(route('deposits.index'));
        }

        $deposit->delete();

        Toastr::success('Deposito  eliminado con éxito.');
        return redirect(route('deposits.index'));

    }
}
