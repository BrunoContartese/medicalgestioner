<?php

namespace CloudMed\Services\Administration;

use Carbon\Carbon;
use Yoeunes\Toastr\Facades\Toastr;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RolesService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Role::orderBy('name', 'ASC'))
            ->addColumn('actions', function(Role $row) {
                return view('Administration.Roles.dataTableActions')->with('id', $row->id);
            })
            ->addColumn('permissions', function(Role $row) {
                return view('Administration.Roles.permissionsDataTableButton')->with('id', $row->id);
            })
            ->rawColumns(['actions', 'permissions'])
            ->make(true);
    }

    /**
     * Store new role method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        Role::create($input);

        Toastr::success('Éxito al registrar el perfil.');

        return redirect(route('roles.index'));
    }

    /**
     * Edit role method
     * @param $roleId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($roleId)
    {
        if(null == $role = Role::find($roleId)) {
            Toastr::error("Perfil no encontrado.");
            return redirect(route('roles.index'));
        }

        return view('Administration.Roles.edit')->with('role', $role);
    }

    /**
     * Update role method
     * @param $roleId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($roleId, $request)
    {
        $input = $request->all();

        if(null == $role = Role::find($roleId)) {
            Toastr::error("Perfil no encontrado.");
            return redirect(route('roles.index'));
        }

        $role->update($input);

        Toastr::success('Perfil actualizado con éxito.');

        return redirect(route('roles.index'));

    }

    /**
     * Delete role method
     * @param $roleId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($roleId)
    {
        if(null == $role = Role::find($roleId)) {
            Toastr::error("Perfil no encontrado.");
            return redirect(route('roles.index'));
        }

        $role->delete();

        Toastr::success('Perfil eliminado con éxito.');
        return redirect(route('roles.index'));

    }

    public function updatePermissions($roleId, $request)
    {
        if(null == $role = Role::find($roleId)) {
            Toastr::error("Perfil no encontrado.");
            return redirect(route('roles.index'));
        }

        $role->syncPermissions($request->permissions);

        Toastr::success("Permisos actualizados con éxito.");
        return redirect(route('roles.index'));
    }
}