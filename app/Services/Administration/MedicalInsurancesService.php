<?php

namespace CloudMed\Services\Administration;

use CloudMed\Http\Requests\Administration\MedicalInsurances\StoreMedicalInsurancesNewsBoardRequest;
use CloudMed\Models\Administration\MedicalInsurance;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Yoeunes\Toastr\Facades\Toastr;
use Yajra\DataTables\Facades\DataTables;

class MedicalInsurancesService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(MedicalInsurance::orderBy('name', 'ASC')->withTrashed())
            ->addColumn('actions', function(MedicalInsurance $row) {
                return view('Administration.MedicalInsurances.dataTableActions')->with('id', $row->id);
            })
            ->editColumn('deleted_at', function(MedicalInsurance $row) {
                if(!is_null($row->deleted_at)) {
                    return "<b>Suspendida el <br>" . Carbon::parse($row->deleted_at)->format('d/m/Y H:i') . "<br> por " . $row->destroyer->name . "</b>";
                }

                return "Funcional";

            })
            ->editColumn('updated_by', function(MedicalInsurance $row) {
                if(!is_null($row->updated_by)) {
                    return $row->editor->name . "<br>" . Carbon::parse($row->updated_at)->format('d/m/Y H:i');
                }

                return "";

            })
            ->addColumn('news_board', function(MedicalInsurance $row) {
                return view('Administration.MedicalInsurances.newsButton')->with('id', $row->id);
            })
            ->addColumn('actions', function(MedicalInsurance $row) {
                return view('Administration.MedicalInsurances.dataTableActions')->with(['id' => $row->id, 'row' => $row]);
            })
            ->rawColumns(['actions', 'updated_by', 'deleted_at', 'news_board'])
            ->make(true);
    }

    /**
     * Store new medical insurance method
     * @param $request
     * @return JsonResponse
     */
    public function store($request)
    {
        $input = $request->all();

        $medicalInsurance = MedicalInsurance::create($input);

        Toastr::success('Éxito al registrar la obra social.');

        return \Response::json("Éxito al registrar la obra social.", 200);
    }

    /**
     * Edit medical insurance method
     * @param $medicalInsurance
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($medicalInsurance)
    {
        if(null == $medicalInsurance = MedicalInsurance::find($medicalInsurance)) {
            Toastr::error("Obra social no encontrada.");
            return redirect(route('medicalInsurances.index'));
        }

        return view('Administration.MedicalInsurances.edit')->with('medicalInsurance', $medicalInsurance);
    }

    /**
     * Update medical insurance method
     * @param $medicalInsurance
     * @param $request
     * @return JsonResponse
     */
    public function update($medicalInsurance, $request)
    {
        $input = $request->all();

        if(null == $medicalInsurance = MedicalInsurance::find($medicalInsurance)) {
            Toastr::error("Obra social no encontrada.");
            return \Response::json(["Error" => 'Obra social no encontrada.', 404]);
        }

        $medicalInsurance->update($input);

        Toastr::success('Obra social actualizada con éxito.');

        return \Response::json("Obra social actualizada con éxito.", 200);

    }

    /**
     * Delete medicalInsurance method
     * @param $medicalInsurance
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($medicalInsurance)
    {
        if(null == $medicalInsurance = MedicalInsurance::find($medicalInsurance)) {
            Toastr::error("Obra social no encontrada.");
            return redirect(route('medicalInsurances.index'));
        }

        $medicalInsurance->delete();

        Toastr::success('Obra social suspendida con éxito.');

        return redirect(route('medicalInsurances.index'));

    }

    /**
     * @param $medicalInsuranceId
     * @return JsonResponse
     */
    public function restoreTrashed($medicalInsuranceId)
    {
        if(null == $medicalInsurance = MedicalInsurance::withTrashed()->find($medicalInsuranceId)) {
            Toastr::error("Obra social no encontrada.");
            return \Response::json(["Error" => 'Obra social no encontrada.', 404]);
        }

        $medicalInsurance->restore();

        Toastr::success('Obra social habilitada con éxito.');

        return redirect(route('medicalInsurances.index'));

    }

    public function writeNews($medicalInsuranceId)
    {
        if(null == $medicalInsurance = MedicalInsurance::withTrashed()->find($medicalInsuranceId)) {
            Toastr::error("Obra social no encontrada.");
            return \Response::json(["Error" => 'Obra social no encontrada.', 404]);
        }

        return view('Administration.MedicalInsurances.newsBoard')->with('medicalInsurance', $medicalInsurance);
    }

    public function storeNews($medicalInsuranceId, $request)
    {
        if(null == $medicalInsurance = MedicalInsurance::withTrashed()->find($medicalInsuranceId)) {
            Toastr::error("Obra social no encontrada.");
            return \Response::json(["Error" => 'Obra social no encontrada.', 404]);
        }

        $medicalInsurance->update([
           'news' => $request->news
        ]);

        Toastr::success('Noticias almacenadas con éxito.');
        return \Response::json("Noticias almacenadas con éxito.", 200);
    }
}