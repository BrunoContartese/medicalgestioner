<?php
/**
 * Created by PhpStorm.
 * User: bcontartese
 * Date: 21/07/19
 * Time: 00:59
 */

namespace CloudMed\Services\Administration;


use CloudMed\Http\Requests\Administration\Nomenclators\UpdateNomenclatorRequest;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use Illuminate\Http\JsonResponse;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;

class NomenclatorsService
{
    public function dataTable()
    {
        return DataTables::of(Nomenclator::orderBy('name', 'ASC')->withTrashed())
            ->addColumn('actions', function(Nomenclator $row) {
                return view('Administration.Nomenclators.dataTableActions')->with('row', $row);
            })
            ->addColumn("status", function(Nomenclator $row) {
                return is_null($row->deleted_at) ? '<b>ACTIVO</b>' : '<b style="color:red;">INACTIVO</b>';
            })
            ->addColumn("practicesListButton", function(Nomenclator $row) {
                return view('Administration.Nomenclators.practicesListDataTableButton')->with(['row' => $row]);
            })
            ->rawColumns(['actions', 'practicesListButton', 'status'])
            ->make(true);
    }

    /**
     * Store new nomenclator method
     * @param $request
     * @return JsonResponse
     */
    public function store($request)
    {
        $input = $request->all();

        $nomenclator = Nomenclator::create($input);

        if(null != $mirrorNomenclator = Nomenclator::find($request->nomenclatorMirror)) {

            foreach($mirrorNomenclator->practices as $practice) {

                NomenclatorPractice::create([
                    'is_module' => $practice->is_module,
                    'code' => $practice->code,
                    'description' => $practice->description,
                    'expense_units' => $practice->expense_units,
                    'honorary_units' => $practice->honorary_units,
                    'helper_honorary_units' => $practice->helper_honorary_units,
                    'anesthesist_honorary_units' => $practice->anesthesist_honorary_units,
                    'nomenclator_id' => $nomenclator->id
                ]);
            }

        }

        Flash::success('Éxito al registrar el nomenclador.');

        return redirect(route('nomenclators.index'));
    }

    /**
     * Edit nomenclator method
     * @param $nomenclator
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($nomenclator)
    {
        if(null == $nomenclator = Nomenclator::find($nomenclator)) {
            Flash::error("Nomenclador no encontrado o inactivo.");
            return redirect(route('nomenclators.index'));
        }

        return view('Administration.Nomenclators.edit')->with('nomenclator', $nomenclator);
    }

    /**
     * Update nomenclator method
     * @param $nomenclator
     * @param $request
     * @return JsonResponse
     */
    public function update($nomenclator, $request)
    {
        $input = $request->all();

        if(null == $nomenclator = Nomenclator::find($nomenclator)) {
            Flash::error("Nomenclador no encontrado.");
            return redirect(route('nomenclators.index'));
        }

        $nomenclator->update($input);

        Flash::success('Nomenclador actualizado con éxito.');

        return redirect(route('nomenclators.index'));

    }

    /**
     * Delete nomenclator method
     * @param $nomenclator
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($nomenclator)
    {
        if(null == $nomenclator = Nomenclator::find($nomenclator)) {
            Flash::error("Nomenclador no encontrado.");
            return redirect(route('nomenclators.index'));
        }

        $nomenclator->delete();

        Flash::success('Nomenclador suspendido con éxito.');

        return redirect(route('nomenclators.index'));

    }

    /**
     * @param $nomenclator
     * @return JsonResponse
     */
    public function restoreTrashed($nomenclator)
    {
        if(null == $nomenclator = Nomenclator::withTrashed()->find($nomenclator)) {
            Flash::error("Nomenclador no encontrado.");
            return redirect(route('nomenclators.index'));
        }

        $nomenclator->restore();

        Flash::success('Nomenclador habilitado con éxito.');

        return redirect(route('nomenclators.index'));

    }
}