<?php

namespace CloudMed\Services\Administration;

use CloudMed\User;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;
use CloudMed\Models\Administration\Specialist;

class SpecialistsService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Specialist::orderBy('name', 'ASC'))
            ->addColumn('actions', function(Specialist $row) {
                return view('Administration.Specialists.dataTableActions')->with('id', $row->id);
            })
            ->addColumn('specialties', function(Specialist $row) {

                return view('Administration.Specialists.specialtiesInDataTable')->with(['specialties' => $row->specialties]);
            })
            ->rawColumns(['actions', 'specialties'])
            ->make(true);
    }

    /**
     * Store new specialist method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $specialist = Specialist::create($input);

        $specialist->specialties()->sync($request->specialties_ids);

        Toastr::success('Éxito al registrar el Especialista.');

        return redirect(route('specialists.index'));
    }

    /**
     * Edit an specialist method
     * @param $specialistId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($specialistId)
    {
        if(null == $specialist = Specialist::find($specialistId)) {
            Toastr::error("Especialista no encontrado.");
            return redirect(route('specialist.index'));
        }

        return view('Administration.Specialists.edit')->with('specialist', $specialist);
    }

    /**
     * Update an specialist method
     * @param $specialistId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($specialistId, $request)
    {
        $input = $request->all();

        if(null == $specialist = Specialist::find($specialistId)) {
            Toastr::error("Especialista no encontrado.");
            return redirect(route('specialists.index'));
        }

        $specialist->update($input);

        $specialist->specialties()->sync($request->specialties_ids);

        Toastr::success('Especialista actualizado con éxito.');

        return redirect(route('specialists.index'));

    }

    /**
     * Delete an specialist method
     * @param $specialistId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($specialistId)
    {
        if(null == $specialist = Specialist::find($specialistId)) {
            Toastr::error("Especialista no encontrado.");
            return redirect(route('specialists.index'));
        }

        $specialist->delete();

        Toastr::success('Especialista eliminado con éxito.');
        return redirect(route('specialists.index'));

    }
}
