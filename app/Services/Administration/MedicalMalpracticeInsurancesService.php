<?php

namespace CloudMed\Services\Administration;

use CloudMed\Models\Administration\MedicalMalpracticeInsurance;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class MedicalMalpracticeInsurancesService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(MedicalMalpracticeInsurance::orderBy('name', 'ASC'))
            ->addColumn('actions', function(MedicalMalpracticeInsurance $row) {
                return view('Administration.MedicalMalpracticeInsurances.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new MedicalMalpracticeInsurance method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $MedicalMalpracticeInsurance = MedicalMalpracticeInsurance::create($input);

        Toastr::success('Éxito al registrar el seguro.');

        return redirect(route('medicalMalpracticeInsurances.index'));
    }

    /**
     * Edit an MedicalMalpracticeInsurance method
     * @param $MedicalMalpracticeInsuranceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($MedicalMalpracticeInsuranceId)
    {
        if(null == $MedicalMalpracticeInsurance = MedicalMalpracticeInsurance::find($MedicalMalpracticeInsuranceId)) {
            Toastr::error("Seguro no encontrado.");
            return redirect(route('medicalMalpracticeInsurances.index'));
        }

        return view('Administration.MedicalMalpracticeInsurances.edit')->with('medicalMalpracticeInsurance', $MedicalMalpracticeInsurance);
    }

    /**
     * Update an MedicalMalpracticeInsurance method
     * @param $MedicalMalpracticeInsuranceId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ($MedicalMalpracticeInsuranceId , $request)
    {
        $input = $request->all();

        if(null == $MedicalMalpracticeInsurance = MedicalMalpracticeInsurance::find($MedicalMalpracticeInsuranceId)) {
            Toastr::error("Seguro no encontrado.");
            return redirect(route('medicalMalpracticeInsurances.index'));
        }

        $MedicalMalpracticeInsurance->update($input);

        Toastr::success('Seguro actualizado con éxito.');

        return redirect(route('medicalMalpracticeInsurances.index'));

    }

    /**
     * Delete MedicalMalpracticeInsurance method
     * @param $MedicalMalpracticeInsuranceId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($MedicalMalpracticeInsuranceId)
    {
        if(null == $MedicalMalpracticeInsurance = MedicalMalpracticeInsurance::find($MedicalMalpracticeInsuranceId)) {
            Toastr::error("Seguro no encontrado.");
            return redirect(route('medicalMalpracticeInsurances.index'));
        }

        $MedicalMalpracticeInsurance->delete();

        Toastr::success('Seguro  eliminado con éxito.');
        return redirect(route('medicalMalpracticeInsurances.index'));

    }
}