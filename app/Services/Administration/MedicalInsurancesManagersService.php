<?php

namespace CloudMed\Services\Administration;

use CloudMed\Models\Administration\MedicalInsurance;
use CloudMed\Models\Administration\MedicalInsuranceManager;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Yoeunes\Toastr\Facades\Toastr;
use Yajra\DataTables\Facades\DataTables;

class MedicalInsurancesManagersService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(MedicalInsuranceManager::withTrashed()->orderBy('name', 'ASC'))
            ->addColumn('actions', function(MedicalInsuranceManager $row) {
                return view('Administration.MedicalInsurancesManagers.dataTableActions')->with('id', $row->id);
            })
            ->editColumn('updated_by', function(MedicalInsuranceManager $row) {
                if(!is_null($row->updated_by)) {
                    return $row->editor->name . "<br>" . Carbon::parse($row->updated_at)->format('d/m/Y H:i');
                }

                return "";

            })
            ->editColumn('medical_insurance_id', function(MedicalInsuranceManager $row) {
                return is_null($row->medicalInsurance->deleted_at) ? $row->medicalInsurance->name : $row->medicalInsurance->name . "<br>" . "<b style='color: red;'>(SUSPENDIDA)</b>";
            })
            ->addColumn('actions', function(MedicalInsuranceManager $row) {
                return view('Administration.MedicalInsurancesManagers.dataTableActions')->with(['id' => $row->id, 'row' => $row]);
            })
            ->rawColumns(['actions', 'updated_by', 'medical_insurance_id'])
            ->make(true);
    }

    /**
     * Store new medical insurance manager method
     * @param $request
     * @return JsonResponse
     */
    public function store($request)
    {
        $input = $request->all();

        $medicalInsuranceManager = MedicalInsuranceManager::create($input);

        Toastr::success('Éxito al registrar la gerenciadora.');

        return \Response::json("Éxito al registrar la gerenciadora.", 200);
    }

    /**
     * Edit medical insurance manager method
     * @param $medicalInsuranceManagerId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($medicalInsuranceManagerId)
    {
        if(null == $medicalInsuranceManager = MedicalInsuranceManager::find($medicalInsuranceManagerId)) {
            Toastr::error("Gerenciadora no encontrada.");
            return redirect(route('medicalInsurancesManagers.index'));
        }

        return view('Administration.MedicalInsurancesManagers.edit')->with('medicalInsuranceManager', $medicalInsuranceManager);
    }

    /**
     * Update medical insurance method
     * @param $medicalInsuranceManagerId
     * @param $request
     * @return JsonResponse
     */
    public function update($medicalInsuranceManagerId, $request)
    {
        $input = $request->all();

        if(null == $medicalInsuranceManager = MedicalInsuranceManager::find($medicalInsuranceManagerId)) {
            Toastr::error("Gerenciadora no encontrada.");
            return \Response::json(["Error" => 'Gerenciadora no encontrada.', 404]);
        }

        $medicalInsuranceManager->update($input);

        Toastr::success('Gerenciadora actualizada con éxito.');

        return \Response::json("Gerenciadora actualizada con éxito.", 200);

    }

    /**
     * Delete medicalInsurance method
     * @param $medicalInsuranceManagerId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($medicalInsuranceManagerId)
    {
        if(null == $medicalInsuranceManager = MedicalInsuranceManager::find($medicalInsuranceManagerId)) {
            Toastr::error("Gerenciadora no encontrada.");
            return redirect(route('medicalInsurancesManagers.index'));
        }

        $medicalInsuranceManager->delete();

        Toastr::success('Gerenciadora suspendida con éxito.');

        return redirect(route('medicalInsurancesManagers.index'));

    }

    public function restoreTrashed($medicalInsuranceManagerId)
    {
        if(null == $medicalInsuranceManager = MedicalInsuranceManager::withTrashed()->find($medicalInsuranceManagerId)) {

            Toastr::error("Gerenciadora no encontrada.");

            return \Response::json(["Error" => 'Gerenciadora no encontrada.', 404]);
        }

        $medicalInsuranceManager->restore();

        Toastr::success('Gerenciadora habilitada con éxito.');

        return redirect(route('medicalInsurancesManagers.index'));

    }
}