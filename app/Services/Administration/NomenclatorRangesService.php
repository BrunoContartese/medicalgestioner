<?php

namespace CloudMed\Services\Administration;

use CloudMed\Http\Requests\Administration\Nomenclators\NomenclatorRanges\UpdateNomenclatorRangeRequest;
use CloudMed\Models\Administration\Nomenclators\NomenclatorRange;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class NomenclatorRangesService
{
    public function dataTable()
    {
        return DataTables::of(NomenclatorRange::orderBy('from_code', 'ASC'))
            ->addColumn('actions', function(NomenclatorRange $row) {
                return view('Administration.NomenclatorRanges.dataTableActions')->with('row', $row);
            })
            ->editColumn('nomenclator_id', function(NomenclatorRange $row) {
                return $row->nomenclator->name;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new nomenclator method
     * @param $request
     * @return JsonResponse
     */
    public function store($request)
    {
        $input = $request->all();

        $nomenclator = NomenclatorRange::create($input);

        toastr()->success('Éxito al registrar el rango.');

        return redirect(route('nomenclatorRanges.index'));
    }

    /**
     * Edit nomenclatorRange method
     * @param $nomenclatorRangeId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($nomenclatorRangeId)
    {
        if(null == $nomenclatorRange = NomenclatorRange::find($nomenclatorRangeId)) {
            Toastr::error("Rango no encontrado o inactivo.");
            return redirect(route('nomenclatorRanges.index'));
        }

        return view('Administration.NomenclatorRanges.edit')->with('nomenclatorRange', $nomenclatorRange);
    }

    /**
     * Update nomenclator method
     * @param $nomenclator
     * @param $request
     * @return JsonResponse
     */
    public function update($nomenclatorRangeId, $request)
    {
        $input = $request->all();

        if(null == $nomenclatorRange = NomenclatorRange::find($nomenclatorRangeId)) {
            Toastr::error("Rango no encontrado.");
            return redirect(route('nomenclatorRanges.index'));
        }

        $nomenclatorRange->update($input);

        Toastr::success('Nomenclador actualizado con éxito.');

        return redirect(route('nomenclatorRanges.index'));

    }

    /**
     * Delete nomenclatorRange method
     * @param $nomenclatorRange
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($nomenclatorRangeId)
    {
        if(null == $nomenclatorRange = NomenclatorRange::find($nomenclatorRangeId)) {
            toastr()->error("Nomenclador no encontrado.");
            return redirect(route('nomenclatorRanges.index'));
        }

        $nomenclatorRange->delete();

        toastr()->success('Rango eliminado con éxito.');

        return redirect(route('nomenclatorRanges.index'));

    }
}