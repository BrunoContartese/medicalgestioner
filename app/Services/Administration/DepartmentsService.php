<?php

namespace CloudMed\Services\Administration;

use Carbon\Carbon;
use CloudMed\Models\Administration\Department;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class DepartmentsService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Department::orderBy('name', 'ASC'))
            ->addColumn('actions', function(Department $row) {
                return view('Administration.Departments.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new specialty method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $user = Department::create($input);

        Toastr::success('Éxito al registrar el departamento.');

        return redirect(route('departments.index'));
    }

    /**
     * Edit an specialty method
     * @param $SpecialtyId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($DepartmentId)
    {
        if(null == $Department = Department::find($DepartmentId)) {
            Toastr::error("Departamento no encontrado.");
            return redirect(route('departments.index'));
        }

        return view('Administration.Departments.edit')->with('Department', $Department);
    }

    /**
     * Update an specialty method
     * @param $DepartmentId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ( $DepartmentId , $request)
    {
        $input = $request->all();

        if(null == $Department = Department::find($DepartmentId)) {
            Flash::error("Departamento no encontrado.");
            return redirect(route('departments.index'));
        }


        $Department->update($input);



        Toastr::success('Departamento actualizado con éxito.');

        return redirect(route('departments.index'));

    }

    /**
     * Delete department method
     * @param $DepartmentId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($DepartmentId)
    {
        if(null == $department = Department::find($DepartmentId)) {
            Toastr::error("Departamento no encontrado.");
            return redirect(route('departments.index'));
        }

        $department->delete();

        Toastr::success('Departamento  eliminado con éxito.');
        return redirect(route('departments.index'));

    }
}
