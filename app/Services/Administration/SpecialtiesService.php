<?php

namespace CloudMed\Services\Administration;

use Carbon\Carbon;
use CloudMed\Models\Administration\Specialty;
use Laracasts\Flash\Flash;
use Yajra\DataTables\Facades\DataTables;
use Yoeunes\Toastr\Facades\Toastr;

class SpecialtiesService
{
    /**
     * Server side data table
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable()
    {
        return DataTables::of(Specialty::orderBy('name', 'ASC'))
            ->addColumn('actions', function(Specialty $row) {
                return view('Administration.Specialties.dataTableActions')->with('id', $row->id);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store new specialty method
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($request)
    {
        $input = $request->all();

        $specialty = Specialty::create($input);

        Toastr::success('Éxito al registrar la especialidad.');

        return redirect(route('specialties.index'));
    }

    /**
     * Edit an specialty method
     * @param $SpecialtyId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($SpecialtyId)
    {
        if(null == $Specialty = Specialty::find($SpecialtyId)) {
            Toastr::error("Especialidad no encontrado.");
            return redirect(route('specialties.index'));
        }

        return view('Administration.Specialties.edit')->with('Specialty', $Specialty);
    }

    /**
     * Update an specialty method
     * @param $SpecialtyId
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update ( $SpecialtyId , $request)
    {
        $input = $request->all();

        if(null == $Specialty = Specialty::find($SpecialtyId)) {
            Flash::error("Especialidad no encontrada.");
            return redirect(route('specialties.index'));
        }


        $Specialty->update($input);



        Toastr::success('Especialidad actualizada con éxito.');

        return redirect(route('specialties.index'));

    }

    /**
     * Delete an speciality method
     * @param $SpecialtyId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($SpecialtyId)
    {
        if(null == $specialty = Specialty::find($SpecialtyId)) {
            Toastr::error("Especialidad no encontrada.");
            return redirect(route('specialties.index'));
        }

        $specialty->delete();

        Toastr::success('Especialidad eliminada con éxito.');
        return redirect(route('specialties.index'));

    }
}
