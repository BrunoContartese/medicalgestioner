<?php

namespace CloudMed\Services;

use CloudMed\Models\Administration\Patient;

class HelpersService
{
    public function getOpenedCheckingAccounts($documentTypeId, $documentNumber)
    {
        $patient = Patient::whereDocumentTypeIdAndDocumentNumber($documentTypeId, $documentNumber)->get();

        /*Realizo una segunda verificación de la existencia del paciente*/
        if(count($patient) < 1) {
            return \Response::json(["Error" => ["Paciente no encontrado"]], 404);
        }

        $patient = $patient[0];

        /*Verifico que tenga al menos una rendición abierta*/
        if(count($patient->checkingAccounts()->wherePatientCheckingAccountStatusId(1)->get()) < 1) {
            return \Response::json(["Error" => ["El paciente no posee rendiciones abiertas"]], 404);
        }

        return view('Helpers.CheckingAccounts.OpenedCheckingAccounts')->with('checkingAccounts', $patient->checkingAccounts()->wherePatientCheckingAccountStatusId(1)->get());
    }
}
