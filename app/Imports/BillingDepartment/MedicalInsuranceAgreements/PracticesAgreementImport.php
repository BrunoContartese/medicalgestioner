<?php

namespace CloudMed\Imports\BillingDepartment\MedicalInsuranceAgreements;

use Carbon\Carbon;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use CloudMed\Models\BillingDepartment\MedicalInsuranceAgreement;
use Illuminate\Support\Collection;
use Flash;
use Maatwebsite\Excel\Concerns\ToCollection;

class PracticesAgreementImport implements ToCollection
{
    private $medicalInsuranceAgreementId;
    private $errors = [];

    public function __construct($medicalInsuranceAgreementId)
    {
        $this->medicalInsuranceAgreementId = $medicalInsuranceAgreementId;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $practices = $this->makePracticesArray($rows);

        $medicalInsuranceAgreement = MedicalInsuranceAgreement::find($this->medicalInsuranceAgreementId);

        $medicalInsuranceAgreement->practicesValuesAgreement()->sync($practices);

        $this->checkErrors();

    }

    private function checkErrors()
    {
        if(count($this->errors) > 0) {

            $message = view('BillingDepartment.MedicalInsuranceAgreements.practicesAgreementErrors')->with(['errors' => $this->errors])->render();

            Flash::error($message);

            \Toastr::warning("La valorización de prácticas se ha realizado con algunos errores.");

        } else {

            \Toastr::success("Valorización de prácticas realizada con éxito.");
        }
    }

    private function makePracticesArray($rows)
    {

        $practices = [];

        foreach($rows as $key => $row) {
            /**
            La primera fila es el encabezado. El orden sería:
            $row[0] = Código de practica,
            $row[1] = Valor del gasto,
            $row[2] = Valor del honorario,
            */
            if($key != 0) {
                if(count(NomenclatorPractice::where('code', $row[0])->get()) < 1) {

                    array_push($this->errors, 'El código ' . $row[0] . ' es inválido o no existe en las prácticas del nomenclador.');

                } else {

                    $practice = NomenclatorPractice::where('code', $row[0])->get();

                    $practice = $practice[0];

                    $fixed_expense_value = 0;
                    $fixed_honorary_value = 0;

                    if(is_float($row[1]) || is_int($row[1])) {
                        $fixed_expense_value = $row[1];
                    } else {
                        $fixed_expense_value = 0;
                        array_push($this->errors, 'El valor del gasto de la práctica ' . $row[0] . ' no es válido. Se ha colocado automáticamente con valor 0.');
                    }

                    if(is_float($row[2]) || is_int($row[2])) {
                        $fixed_honorary_value = $row[2];
                    } else {
                        $fixed_honorary_value = 0;
                        array_push($this->errors, 'El valor del honorario de la práctica ' . $row[0] . ' no es válido. Se ha colocado automáticamente con valor 0.');

                    }

                    if(floatval($row[1]) != false)
                        $practices[$practice->id] = [
                            'fixed_expense_value' => $fixed_expense_value,
                            'fixed_honorary_value' => $fixed_honorary_value,
                        ];

                }
            }
        }

        return $practices;
    }
}
