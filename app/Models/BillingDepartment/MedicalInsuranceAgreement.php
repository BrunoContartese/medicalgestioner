<?php

namespace CloudMed\Models\BillingDepartment;

use Arcanedev\Support\Database\Model;
use CloudMed\Http\Controllers\Administration\NomenclatorPractices;
use CloudMed\Models\Administration\MedicalInsurance;
use CloudMed\Models\Administration\Nomenclators\Nomenclator;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use CloudMed\Models\Administration\Nomenclators\NomenclatorRange;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicalInsuranceAgreement extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medical_insurance_agreement';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'nomenclator_id',
        'medical_insurance_id',
        'without_end_date',
        'valid_from',
        'valid_until'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'description' => 'string',
        'nomenclator_id' => 'int',
        'medical_insurance_id' => 'int',
        'without_end_date' => 'boolean',
        'valid_from' => 'date',
        'valid_until' => 'date',
    ];


    public static $rules = [
        'description' => 'required|unique:medical_insurance_agreement,description',
        'nomenclator_id' => 'required|exists:nomenclators,id',
        'medical_insurance_id' => 'required|exists:medical_insurances,id',
        'valid_from' => 'required',
        'rangesValues' => 'array',
    ];

    public static $messages = [
        'description.required' => 'El campo Nombre del convenio es obligatorio.',
        'description.unique' => 'El nombre del convenio ya existe en la base de datos.',
        'nomenclator_id.required' => 'Debe seleccionar el nomenclador con el cuál se realizará el convenio.',
        'nomenclator_id.exists' => 'El nomenclador seleccionado no es válido.',
        'medical_insurance_id.required' => 'Debe seleccionar la obra social con la cuál se realizará el convenio.',
        'nomenclator_id.exists' => 'La obra social seleccionada no es válida.',
        'valid_from.required' => 'Debe ingresar la fecha desde la cuál tendrá vigencia el convenio.'
    ];

    public function nomenclator()
    {
        return $this->belongsTo(Nomenclator::class, 'nomenclator_id');
    }
    public function medicalInsurance()
    {
        return $this->belongsTo(MedicalInsurance::class, 'medical_insurance_id');
    }

    public function rangesValuesAgreement()
    {
        return $this->belongsToMany(NomenclatorRange::class, 'medical_insurance_agreement_ranges_values', 'medical_insurance_agreement_id', 'nomenclator_range_id')->withPivot(['expense_unit_value', 'honorary_unit_value', 'fixed_honorary_amount', 'fixed_expense_amount']);
    }

    public function practicesValuesAgreement()
    {
        return $this->belongsToMany(NomenclatorPractice::class, 'medical_insurance_agreement_practices_values', 'medical_insurance_agreement_id', 'nomenclator_practice_id')->withPivot(['expense_unit_value', 'honorary_unit_value', 'fixed_honorary_value', 'fixed_expense_value']);

    }
}