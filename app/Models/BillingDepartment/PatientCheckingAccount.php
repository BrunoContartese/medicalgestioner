<?php

namespace CloudMed\Models\BillingDepartment;

use CloudMed\Models\Administration\MedicalInsurance;
use CloudMed\Models\Administration\Patient;
use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyStudy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class PatientCheckingAccount extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = 'patient_checking_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patient_checking_account_type_id',
        'patient_id',
        'checking_account_number',
        'patient_checking_account_status_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'patient_checking_account_type_id' => 'integer',
        'patient_id' => 'integer',
        'checking_account_number' => 'integer',
        'patient_checking_account_status_id' => 'integer',
    ];

    public static $rules = [
        'document_type_id' => 'required',
        'document_number' => 'required',
        'medical_insurance_id' => 'required|exists:medical_insurances,id'
    ];

    public static $messages = [
        'document_type_id.required' => 'Debe seleccionar el tipo de documento del paciente.',
        'document_number.required' => 'Debe ingresar el número de documento del paciente.',
        'medical_insurance_id.required' => 'Debe seleccionar la obra social del paciente.',
        'medical_insurance_id.exists' => 'La obra social seleccionada no es válida.',
        'patient_id.required' => 'El paciente seleccionado no es válido.',
    ];

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function checkingAccountType()
    {
        return $this->belongsTo(PatientCheckingAccountType::class, 'patient_checking_account_type_id');
    }

    public function checkingAccountStatus()
    {
        return $this->belongsTo(PatientCheckingAccountStatus::class, 'patient_checking_account_status_id');
    }

    public function checkingAccountBillingInformation()
    {
        return $this->belongsToMany(MedicalInsurance::class, 'patient_checking_accounts_billing_information', 'patient_checking_account_id', 'medical_insurance_id');
    }

    public function pathologicalAnatomyStudies()
    {
        return $this->hasMany(PathologicalAnatomyStudy::class, 'patient_checking_account_id');
    }
}
