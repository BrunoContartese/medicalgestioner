<?php

namespace CloudMed\Models\BillingDepartment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientCheckingAccountType extends Model
{
    use SoftDeletes;

    public $table = 'patient_checking_accounts_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
    ];

    public function patientCheckingAccounts()
    {
        return $this->hasMany(PatientCheckingAccount::class, 'patient_checking_account_type_id');
    }
}
