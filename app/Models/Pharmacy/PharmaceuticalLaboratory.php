<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;

class PharmaceuticalLaboratory extends Model
{
    public $table = 'pharmaceutical_laboratories';

    protected $fillable = ['code', 'name'];

    protected $casts = [
        'id' => 'integer',
        'code' => 'integer',
        'name' => 'string',
    ];
}
