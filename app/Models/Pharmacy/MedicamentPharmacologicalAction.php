<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;

class MedicamentPharmacologicalAction extends Model
{
    public $table = 'medicament_pharmacological_actions';

    protected $fillable = ['code', 'description'];

    protected $casts = [
        'id' => 'integer',
        'code' => 'integer',
        'description' => 'string',
    ];
}
