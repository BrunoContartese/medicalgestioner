<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;

class MedicamentAdministrationLine extends Model
{
    public $table = 'medicament_administration_lines';

    protected $fillable = ['code', 'description'];

    protected $casts = [
      'id' => 'integer',
      'code' => 'integer',
      'description' => 'string',
    ];
}
