<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;

class MedicamentUpdateStatus extends Model
{
    public $table = 'medicament_update_statuses';

    protected $fillable = ['name'];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
    ];
}
