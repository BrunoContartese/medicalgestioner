<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;

class Medicament extends Model
{
    public $table = 'medicaments';

    protected $fillable = [
        'label',
        'name',
        'presentation',
        'laboratory_name',
        'cost_price',
        'sell_price',
        'laboratory_code',
        'alphabeta_medicament_id',
        'is_active',
        'barcode',
        'units',
        'medicament_drug_code',
        'medicament_pharmacological_action_code',
        'medicament_administration_line_code',
        'medicament_category_id',
        'on_stock'
    ];

    protected $casts = [
        'label' => 'string',
        'name' => 'string',
        'presentation' => 'string',
        'laboratory_name' => 'string',
        'cost_price' => 'double',
        'sell_price' => 'double',
        'laboratory_code' => 'integer',
        'alphabeta_medicament_id' => 'integer',
        'is_active' => 'boolean',
        'barcode' => 'string',
        'units' => 'integer',
        'medicament_drug_code' => 'integer',
        'medicament_pharmacological_action_code' => 'integer',
        'medicament_administration_line_code' => 'integer',
        'medicament_category_id' => 'integer'
    ];

    public function pharmacologicalAction()
    {
        return $this->belongsTo(MedicamentPharmacologicalAction::class, 'medicament_pharmacological_action_code', 'code');
    }

    public function drug()
    {
        return $this->belongsTo(MedicamentDrug::class, 'medicament_drug_code', 'code');
    }

    public function administrationLine()
    {
        return $this->belongsTo(MedicamentAdministrationLine::class, 'medicament_administration_line_code', 'code');
    }

    public function laboratory()
    {
        return $this->belongsTo(PharmaceuticalLaboratory::class, 'laboratory_code', 'code');
    }

    public function category()
    {
        return $this->belongsTo(MedicamentCategory::class, 'medicament_category_id');
    }
}
