<?php

namespace CloudMed\Models\Pharmacy;

use CloudMed\User;
use Illuminate\Database\Eloquent\Model;

class MedicamentUpdateQueue extends Model
{
    public $table = 'medicament_updates_queue';

    protected $fillable = ['user_id', 'medicament_update_status_id'];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'medicament_update_status_id' => 'integer',
    ];

    public function status()
    {
        return $this->belongsTo(MedicamentUpdateStatus::class, 'medicament_update_status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
