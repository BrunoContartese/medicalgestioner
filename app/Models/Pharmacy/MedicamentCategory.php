<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicamentCategory extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = 'medicament_categories';

    protected $fillable = ['description'];

    protected $casts = [
        'id' => 'integer',
        'description' => 'string'
    ];
}
