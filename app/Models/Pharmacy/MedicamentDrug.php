<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;

class MedicamentDrug extends Model
{
    public $table = 'medicament_drugs';

    protected $fillable = ['code', 'description'];

    protected $casts = [
        'id' => 'integer',
        'code' => 'integer',
        'description' => 'string',
    ];
}
