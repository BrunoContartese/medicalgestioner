<?php

namespace CloudMed\Models\HumanResourcesDepartment;

use Arcanedev\Support\Database\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class EmployeeCategory extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'employees_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'tango_code',
        'employee_agreement_id',
        'employee_settlement_method_id',
        'suggested_salary_amount',
        'minimum_salary_amount',
        'maximum_salary_amount',
        'extra_hour_amount',
        'salary_matrix_amount',
        'days_per_month',
        'days_per_week',
        'hours_per_day',
        'hours_per_week',
        'hours_per_month'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'tango_code' => 'string',
        'employee_agreement_id' => 'int',
        'employee_settlement_method_id' => 'int',
        'suggested_salary_amount' => 'float',
        'minimum_salary_amount' => 'float',
        'maximum_salary_amount' => 'float',
        'extra_hour_amount' => 'float',
        'salary_matrix_amount' => 'float',
        'days_per_month' => 'int',
        'days_per_week' => 'int',
        'hours_per_day' => 'float',
        'hours_per_week' => 'float',
        'hours_per_month' => 'float'
    ];


    public static $rules = [
        'name' => 'required|unique:employees_categories,name',
        'tango_code' => 'required|unique:employees_categories,tango_code',
        'employee_agreement_id' => 'required|exists:employees_agreements,id',
        'employee_settlement_method_id' => 'required|exists:employees_settlement_methods,id'
    ];

    public static $messages = [
        'name.required' => 'Debe ingresar el nombre de la categoría.',
        'name.unique' => 'El nombre de la categoría ya existe en la base de datos.',
        'tango_code.required' => 'Debe ingresar el código correspondiente al sistema de tango.',
        'tango_code.unique' => 'El código correspondiente al sistema de tango ya está ingresado en la base de datos.',
        'employee_agreement_id.required' => 'Debe seleccionar el convenio de trabajo.',
        'employee_agreement_id.exists' => 'El convenio de trabajo seleccionado es inválido.',
        'employee_settlement_method_id.required' => 'Debe seleccionar el método de liquidación.',
        'employee_settlement_method_id.exists' => 'El método de liquidación seleccionado es inválido.'
    ];


}