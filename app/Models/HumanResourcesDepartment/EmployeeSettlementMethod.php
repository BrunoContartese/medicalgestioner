<?php

namespace CloudMed\Models\HumanResourcesDepartment;

use Arcanedev\Support\Database\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class EmployeeSettlementMethod extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'employees_settlement_methods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];


    public static $rules = [
        'name' => 'required|unique:employees_agreements,name'
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre del método de liquidación es obligatorio.',
        'name.unique' => 'El nombre del método de liquidación ya existe en la base de datos.',
    ];

    public function employeeCategories()
    {
        return $this->hasMany(EmployeeCategory::class);
    }


}