<?php

namespace CloudMed\Models\Administration\Nomenclators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class NomenclatorRange extends Model
{
    use Userstamps;

    public $table = "nomenclator_ranges";

    public $fillable = [
        'from_code',
        'to_code',
        'nomenclator_id',
    ];

    public $casts = [
        'from_code' => 'integer',
        'to_code' => 'integer',
        'nomenclator_id' => 'integer',
    ];

    public static $rules = [
        'from_code' => 'required',
        'to_code' => 'required',
        'nomenclator_id' => 'required',
    ];

    public static $messages = [
        'from_code.required' => 'Debe indicar el código inicial del rango.',
        'to_code.required' => 'Debe indicar el código final del rango.',
        'nomenclator_id.required' => 'Debe indicar el nomenclador al que pertenece el rango.',
    ];

    public function nomenclator()
    {
        return $this->belongsTo(Nomenclator::class, 'nomenclator_id');
    }

    public function getDescriptionAttribute()
    {
        return $this->from_code . "-" . $this->to_code;
    }
}
