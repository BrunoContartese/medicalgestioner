<?php

namespace CloudMed\Models\Administration\Nomenclators;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class NomenclatorPracticeGroup extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'nomenclator_practices_group';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];


    public static $rules = [
        'name' => 'required|unique:nomenclator_practices_group,name',
    ];

    public static $messages = [
        'name.required' => 'El campo Titulo es obligatorio.',
        'name.unique' => 'El Titulo ya existe en la base de datos. Verifique que no esté suspendida.',
    ];

    public function nomenclatorPractices()
    {

        return $this->belongsToMany(NomenclatorPractice::class,'nomenclator_practices_group_pivot', 'nomenclator_practice_group_id', 'nomenclator_practice_id');

    }
}
