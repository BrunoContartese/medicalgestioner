<?php
/**
 * Created by PhpStorm.
 * User: bcontartese
 * Date: 23/07/19
 * Time: 19:16
 */

namespace CloudMed\Models\Administration\Nomenclators;


use Arcanedev\Support\Database\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class NomenclatorPractice extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = "nomenclator_practices";

    public $fillable = [
        'is_module',
        'code',
        'description',
        'expense_units',
        'honorary_units',
        'helper_honorary_units',
        'anesthesist_honorary_units',
        'nomenclator_id',
        'helpers_quantity'
    ];

    public $casts = [
        'is_module' => 'boolean',
        'code' => 'integer',
        'description' => 'string',
        'expense_units' => 'float',
        'honorary_units' => 'float',
        'helper_honorary_units' => 'float',
        'anesthesist_honorary_units' => 'float',
        'nomenclator_id' => 'integer',
        'helpers_quantity' => 'integer'
    ];

    public static $rules = [
        'is_module' => 'required',
        'code' => 'required',
        'description' => 'required',
        'expense_units' => 'required',
        'honorary_units' => 'required',
        'helper_honorary_units' => 'required',
        'anesthesist_honorary_units' => 'required',
        'helpers_quantity' => 'required'
    ];

    public static $messages = [
        'is_module.required' => 'Debe indicar si la práctica es un módulo.',
        'code.required' => 'Debe ingresar el código de la práctica.',
        'description.required' => 'Debe ingresar la descripción de la práctica.',
        'expense_units.required' => 'Debe ingresar las unidades de gasto.',
        'honorary_units.required' => 'Debe ingresar las unidades de honorario del médico.',
        'helper_honorary_units.required' => 'Debe ingresar las unidades de honorario del médico ayudante.',
        'anesthesist_honorary_units.required' => 'Debe ingresar las unidades de honorario del anestesista.',
        'helpers_quantity.required' => 'Debe indicar si la práctica posee 2 ayudantes.',
        'nomenclator.exists' => 'Nomenclador inválido.',
        'code.unique' => 'El código ingresado ya está asignado a una práctica para el nomenclador deseado.',
        'expense_units.numeric' => 'Las unidades de gasto debe pertenecer a los números reales.',
        'honorary_units.numeric' => 'Las unidades del honorario médico debe pertenecer a los números reales.',
        'helper_honorary_units.numeric' => 'Las unidades del honorario del médico ayudante debe pertenecer a los números reales.',
        'anesthesist_honorary_units.numeric' => 'Las unidades del honorario de anestesista debe pertenecer a los números reales.',
    ];

    public function getFullNameAttribute()
    {
        return $this->code . ' - ' . substr($this->description, 0, 50) . "...";
    }

}
