<?php

namespace CloudMed\Models\Administration\Nomenclators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Nomenclator extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = "nomenclators";

    public $fillable = [
        'name'
    ];

    public static $rules = [
        'name' => 'required|unique:nomenclators,name'
    ];

    public static $messages = [
        'name.required' => 'Debe ingresar el nombre del nomenclador.',
        'name.unique' => 'Ya existe un nomenclador con el nombre ingresado.'
    ];

    public function practices()
    {
        return $this->hasMany(NomenclatorPractice::class, 'nomenclator_id')->orderBy('code', 'ASC');
    }
}
