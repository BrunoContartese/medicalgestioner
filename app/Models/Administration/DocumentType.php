<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    public $table = 'document_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'postal_code' => 'integer',
    ];
}
