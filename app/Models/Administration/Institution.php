<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    public $table = 'institutions';
}
