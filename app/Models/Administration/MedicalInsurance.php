<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicalInsurance extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medical_insurances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'billing_address',
        'contact',
        'perceives_gross_income',
        'apply_discount_on_medications',
        'medication_discount_percentage',
        'cuit',
        'news'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */


    public static $rules = [
        'name' => 'required|unique:medical_insurances,name',
        'cuit' => 'required|unique:medical_insurances,cuit|min:11|max:11',
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre es obligatorio.',
        'cuit.required' => 'El campo CUIT es obligatorio.',
        'name.unique' => 'El nombre para la obra social a registrar ya existe en la base de datos. Verifique que no esté suspendida.',
        'cuit.unique' => 'El cuit de la obra social a registrar ya existe en la base de datos.'
    ];
}
