<?php

namespace CloudMed\Models\Administration;

use CloudMed\Models\MedicalDepartments\PathologicalAnatomy\PathologicalAnatomyDepartmentPrefix;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Department extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medical_departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'prefix'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];


    public static $rules = [
        'name' => 'required|unique:medical_departments,name',
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre es obligatorio.',
        'name.unique' => 'El nombre del Departamento ya existe en la base de datos. Verifique que no esté suspendida.',
    ];

    public function pathologicalAnatomyPrefix()
    {
        return $this->belongsTo(PathologicalAnatomyDepartmentPrefix::class, 'medical_department_id');
    }
}
