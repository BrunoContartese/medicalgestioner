<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicalInsuranceManager extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medical_insurances_managers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'medical_insurance_id',
        'name',
        'billing_address',
        'contact',
        'perceives_gross_income',
        'cuit'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'medical_insurance_id' => 'integer',
        'name' => 'string',
        'billing_address' => 'string',
        'contact' => 'string',
        'perceives_gross_income' => 'boolean',
        'cuit' => 'string'
    ];

    public static $rules = [
        'medical_insurance_id' => 'required|exists:medical_insurances,id',
        'name' => 'required|unique:medical_insurances_managers,name',
        'cuit' => 'required|unique:medical_insurances_managers,cuit'
    ];

    public static $messages = [
        'medical_insurance_id.required' => 'Debe seleccionar la obra social',
        'medical_insurance_id.exists' => 'La obra social seleccionada no es válida',
        'name.required' => 'El campo Nombre es obligatorio.',
        'cuit.required' => 'El campo CUIT es obligatorio.',
        'name.unique' => 'El nombre para la gerenciadora a registrar ya existe en la base de datos.',
        'cuit.unique' => 'El cuit de la gerenciadora a registrar ya existe en la base de datos.'
    ];

    public function medicalInsurance()
    {
        return $this->belongsTo(MedicalInsurance::class, 'medical_insurance_id')->withTrashed();
    }
}
