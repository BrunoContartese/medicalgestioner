<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Specialist extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medical_specialists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'cuit',
        'licence',
        'email',
        'area_code',
        'phone_number',
        'medical_malpractice_insurance_id',

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'cuit' => 'string',
        'licence' => 'string',
        'mail' => 'string',
        'phone_number' => 'string',
        'medical_malpractice_insurance_id' => 'int',
        'area_code' => 'string',

    ];


    public static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'area_code' => 'required',
        'phone_number' => 'required'
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre es obligatorio.',
        'name.unique' => 'El nombre del especialista ya existe en la base de datos. Verifique que no esté suspendida.',
        'cuit.required' => 'El campo CUIT es obligatorio.',
        'cuit.unique' => 'El CUIT del especialista ya existe en la base de datos. Verifique que no esté suspendida.',
        'email.required' => 'El campo E-Mail es obligatorio.',
        'email.email' => 'El E-Mail ingresado es inválido.',
        'area_code.required' => 'El campo código de área es obligatorio.',
        'phone_number.required' => 'El campo teléfono es obligatorio.',
    ];


    public function specialties()
    {
        return $this->belongsToMany(Specialty::class, 'specialist_specialties', 'specialist_id', 'specialty_id');
    }

    public function medicalMalpracticeInsurance()
    {
        return $this->belongsTo(MedicalMalpracticeInsurance::class, 'medical_malpractice_insurance_id');
    }
}
