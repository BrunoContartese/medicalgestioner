<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_name',
        'postal_code',
        'province_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'city_name' => 'string',
        'postal_code' => 'string',
        'province_id' => 'integer',
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
}
