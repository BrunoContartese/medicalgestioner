<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Deposit extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medication_deposits';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];


    public static $rules = [
        'name' => 'required|unique:medication_deposits,name',
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre es obligatorio.',
        'name.unique' => 'El nombre del deposito ya existe en la base de datos. Verifique que no esté suspendida.',
    ];
}
