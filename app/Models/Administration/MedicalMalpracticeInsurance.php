<?php

namespace CloudMed\Models\Administration;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;


class MedicalMalpracticeInsurance extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'medical_malpractice_insurances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];


    public static $rules = [
        'name' => 'required|unique:medical_malpractice_insurances,name',
    ];

    public static $messages = [
        'name.required' => 'El campo Nombre es obligatorio.',
        'name.unique' => 'El nombre del seguro ya existe en la base de datos.',
    ];
}
