<?php

namespace CloudMed\Models\Administration;

use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Patient extends Model
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'patients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'document_type_id',
        'document_number',
        'gender_id',
        'city_id',
        'phone_number',
        'mobile_phone_area_code',
        'mobile_phone_number',
        'address',
        'birthday'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'document_type_id' => 'integer',
        'document_number' => 'string',
        'gender_id' => 'integer',
        'city_id' => 'integer',
        'phone_number' => 'string',
        'address' => 'string',
        'mobile_phone_area_code' => 'string',
        'mobile_phone_number' => 'string',
        'birthday' => 'date'
    ];


    public static $rules = [
        'name' => 'required',
        'document_type_id' => 'required|exists:document_types,id',
        'document_number' => 'required',
        'gender_id' => 'required|exists:genders,id',
        'city_id' => 'required|exists:cities,id',
        'address' => 'required',
        'birthday' => 'required',
    ];

    public static $messages = [
        'name.required' => 'Debe ingresar el nombre del paciente.',
        'document_type_id.required' => 'Debe ingresar el tipo de documento del paciente.',
        'document_type_id.exists' => 'El tipo de documento ingresado es inválido.',
        'document_number.required' => 'Debe ingresar el número de documento del paciente.',
        'document_number.unique' => 'El número de documento ingresado ya existe en la base de datos.',
        'gender_id.required' => 'Debe seleccionar el sexo del paciente',
        'gender_id.exists' => 'El sexo seleccionado es inválido',
        'city_id.required' => 'Debe seleccionar la ciudad de residencia del paciente.',
        'city_id.exists' => 'La ciudad seleccionada es inválida.',
        'address.required' => 'Debe ingresar la dirección del paciente.',
        'birthday.required' => 'Debe ingresar la fecha de nacimiento del paciente.'
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id');
    }

    public function medicalInsurances()
    {
        return $this->belongsToMany(MedicalInsurance::class, 'patient_medical_insurance')->withPivot(['afilliate_number', 'expiration_date', 'created_at', 'updated_at']);
    }

    public function checkingAccounts()
    {
        return $this->hasMany(PatientCheckingAccount::class, 'patient_id');
    }
}
