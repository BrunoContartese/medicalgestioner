<?php

namespace CloudMed\Models\Pharmacy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicamentUpdateLog extends Model
{
    public $table = 'medicament_updates';

    protected $fillable = [
        'user_id',
        'succeeded'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'succeeded' => 'integer',

    ];
}
