<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicalReportTitle extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = "pathological_anatomy_medical_report_titles";

    protected $fillable = [
        'name',
    ];
}
