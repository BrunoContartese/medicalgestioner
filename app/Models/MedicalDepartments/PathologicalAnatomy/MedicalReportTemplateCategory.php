<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicalReportTemplateCategory extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = "pathological_anatomy_templates_categories";

    protected $fillable = [
        'name',
    ];

    public function childTemplates()
    {
        return $this->hasMany(MedicalReportTemplate::class, 'template_category_id');
    }
}
