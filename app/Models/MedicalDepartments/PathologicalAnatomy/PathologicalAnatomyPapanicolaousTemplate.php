<?php

namespace App\Models\MedicalDepartments\PathologicalAnatomy;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class PathologicalAnatomyPapanicolaousTemplate extends BaseModel
{
    use Userstamps;
    use SoftDeletes;

    public $table = 'pathological_anatomy_papanicolaous_templates';

    public $fillable = [
        'category',
        'description'
    ];

    protected $casts = [
        'id' => 'int',
        'code' => 'int',
        'description' => 'string'
    ];

    public static $rules = [

    ];

    public static $messages = [
    ];
}
