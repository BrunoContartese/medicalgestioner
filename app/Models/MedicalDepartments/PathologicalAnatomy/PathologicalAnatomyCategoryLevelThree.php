<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;

class PathologicalAnatomyCategoryLevelThree extends Model
{
    public $table = 'pathological_anatomy_categories_level_three';

    public function childCategories()
    {
        return $this->hasMany(PathologicalAnatomyCategoryLevelFour::class, 'pathological_category_level_three_id');
    }
}
