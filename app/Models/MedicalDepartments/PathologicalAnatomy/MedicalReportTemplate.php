<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MedicalReportTemplate extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = "pathological_anatomy_templates";

    protected $fillable = [
        'code',
        'description',
    ];

    public function getFullDescriptionAttribute()
    {
        return $this->code . ': ' . $this->description;
    }
}
