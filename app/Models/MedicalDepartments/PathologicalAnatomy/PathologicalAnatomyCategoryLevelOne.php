<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;

class PathologicalAnatomyCategoryLevelOne extends Model
{
    public $table = 'pathological_anatomy_categories_level_one';

    public function childCategories()
    {
        return $this->hasMany(PathologicalAnatomyCategoryLevelTwo::class, 'pathological_category_level_one_id');
    }
}
