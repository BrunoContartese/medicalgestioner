<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;

class PathologicalAnatomyCategoryLevelTwo extends Model
{
    public $table = 'pathological_anatomy_categories_level_two';

    public function childCategories()
    {
        return $this->hasMany(PathologicalAnatomyCategoryLevelThree::class, 'pathological_category_level_two_id');
    }
}
