<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;

class PathologicalAnatomyCategoryLevelFour extends Model
{
    public $table = 'pathological_anatomy_categories_level_four';
}
