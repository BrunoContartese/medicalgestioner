<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class PathologicalAnatomyType extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = "pathological_anatomy_types";

    protected $fillable = [
        'name',
        'prefix'
    ];

    public function pathologicalAnatomies()
    {
        return $this->hasMany(PathologicalAnatomyStudy::class, 'pathological_anatomy_type_id');
    }
}
