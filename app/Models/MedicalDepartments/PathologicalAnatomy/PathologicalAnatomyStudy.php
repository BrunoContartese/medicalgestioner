<?php

namespace CloudMed\Models\MedicalDepartments\PathologicalAnatomy;

use CloudMed\Models\Administration\Department;
use CloudMed\Models\Administration\Institution;
use CloudMed\Models\Administration\Nomenclators\NomenclatorPractice;
use CloudMed\Models\Administration\Specialist;
use CloudMed\Models\BillingDepartment\PatientCheckingAccount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class PathologicalAnatomyStudy extends Model
{
    use SoftDeletes;
    use Userstamps;

    public $table = 'pathological_anatomy_studies';

    protected $fillable = [
        'code',
        'pathological_anatomy_type_id',
        'medical_specialist_id',
        'patient_checking_account_id',
        'institution_id',
        'medical_department_id',
        'received_at',
        'pathological_anatomy_category_level_one_id',
        'pathological_anatomy_category_level_two_id',
        'pathological_anatomy_category_level_three_id',
        'pathological_anatomy_category_level_four_id',
        'comments'
    ];

    public static $rules = [
        'pathological_anatomy_type_id' => 'required|exists:pathological_anatomy_types,id',
        'medical_specialist_id' => 'required|exists:medical_specialists,id',
        'patient_checking_account_id' => 'required|exists:patient_checking_accounts,id',
        'institution_id' => 'required|exists:institutions,id',
        'medical_department_id' => 'required|exists:medical_departments,id',
    ];

    public static $messages = [
        'pathological_anatomy_type_id.required' => 'Debe seleccionar el tipo de estudio.',
        'pathological_anatomy_type_id.exists' => 'El tipo de estudio seleccionado no existe en la base de datos.',
        'medical_specialist_id.required' => 'Debe seleccionar el médico.',
        'medical_specialist_id.exists' => 'El médico seleccionado no existe en la base de datos.',
        'patient_checking_account_id.required' => 'Debe seleccionar la rendición a la que se adjuntará el estudio.',
        'patient_checking_account_id.exists' => 'La rendición seleccionada no existe en la base de datos.',
        'institution_id.required' => 'Debe seleccionar la institución derivante.',
        'institution_id.exists' => 'La institución derivante seleccionada no existe en la base de datos.',
        'medical_department_id.required' => 'Debe seleccionar el departamento solicitante.',
        'medical_department_id.exists' => 'El departamento solicitante seleccionado no existe en la base de datos.',
    ];

    public function institution()
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }

    public function medicalDepartment()
    {
        return $this->belongsTo(Department::class, 'medical_department_id');
    }

    public function pathologicalAnatomyType()
    {
        return $this->belongsTo(PathologicalAnatomyType::class, 'pathological_anatomy_type_id');
    }

    public function patientCheckingAccount()
    {
        return $this->belongsTo(PatientCheckingAccount::class, 'patient_checking_account_id');
    }

    public function billingCodes()
    {
        return $this->belongsToMany(NomenclatorPractice::class, 'pathological_anatomy_billing_codes', 'pathological_anatomy_study_id', 'nomenclator_practice_id');
    }

    public function specialist()
    {
        return $this->belongsTo(Specialist::class, 'medical_specialist_id');
    }

}
